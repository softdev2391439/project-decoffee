/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package softdecoffee.pos;

import java.awt.Font;
import java.awt.Frame;
import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import static java.lang.Thread.sleep;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;
import static org.joda.time.format.StrictISODateTimeFormat.date;
import softdecoffee.dao.CustomerDao;
import softdecoffee.dao.EmployeeDao;
import softdecoffee.model.Customer;
import softdecoffee.model.Employee;
import softdecoffee.model.Product;
import softdecoffee.model.Promotion;
import softdecoffee.model.Receipt;
import softdecoffee.model.ReceiptDetail;

import softdecoffee.service.CustomerService;
import softdecoffee.service.EmployeeService;
import softdecoffee.service.ProductService;
import softdecoffee.service.ReceiptService;
import softdecoffee.ui.customer.CustomerDialog;
import softdecoffee.ui.promotion.PromotionPanel;

/**
 *
 * @author Admin
 */
public class POS extends javax.swing.JPanel implements BuyProductable {

    ArrayList<Product> products;
    ProductService productService = new ProductService();
    ReceiptService receiptService = new ReceiptService();
    Receipt receipt;
    private final ProductlistPanel productListPanel;
    private Customer editedCustomer;
    Customer customer;
    private Object customerService;
    private Object tblCustomer;
    private Object employee;
    private POS pos;
    public DefaultTableModel model = new DefaultTableModel();
    private Frame frame;
    private Object customPanel;
    private Customer customerId;
    private Promotion showPromotion;
    private Promotion Promotion;
    private float cashAmount;
    private java.sql.Date receiptDate;
    private java.sql.Date date;
    private String currentUser;
        private Employee EmpUser;
	private String rank;

    /**
     * Creates new form PosPanel
     */
    public POS() {

        initComponents();
        enableform(false);
        enableCash(false);
        receipt = new Receipt();
        

        tblReceiptDetail.setModel(new AbstractTableModel() {
            String[] headers = {"Name", "QTY", "Price", "Total"};

            @Override
            public String getColumnName(int column) {
                return headers[column];
            }

            @Override
            public int getRowCount() {
                return receipt.getReceiptDetails().size();
            }

            @Override
            public int getColumnCount() {
                return 4;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                ArrayList<ReceiptDetail> receiptDetails = receipt.getReceiptDetails();
                ReceiptDetail receiptDetail = receiptDetails.get(rowIndex);

                switch (columnIndex) {
                    case 0:
                        return receiptDetail.getProductName();
                    case 1:
                        return receiptDetail.getReceiptDetailQty();
                    case 2:
                        return receiptDetail.getReceiptDetailUnitPrice();
                    case 3:
                        return receiptDetail.getReceiptDetailTotal();
                    default:
                        return "";
                }
            }

            @Override
            public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
                ArrayList<ReceiptDetail> receiptDetails = receipt.getReceiptDetails();
                ReceiptDetail receiptDetail = receiptDetails.get(rowIndex);
                if (columnIndex == 2) {
                    int qty = Integer.parseInt((String) aValue);
                    if (qty < 1) {
                        return;
                    }
                    receiptDetail.setReceiptDetailQty(qty);
                    //receipt.calculateTotal();

                    refreshReceipt();
                }
            }

            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                switch (columnIndex) {
                    case 2:
                        return true;
                    default:
                        return false;
                }
            }

        });
        productListPanel = new ProductlistPanel();
        productListPanel.addOnBuyProduct(this);
        scrProduceList.setViewportView(productListPanel);
    }

    private void refreshReceipt() {
        tblReceiptDetail.revalidate();
        tblReceiptDetail.repaint();
        lblTotal.setText("" + receipt.getReceiptTotal());
        lblTatalNet.setText("" + receipt.getReceiptNetTotal());
        lblChang.setText(String.format("%.2f", receipt.getReceiptChange()));
    }

    public void refreshTable() {
        receipt = new Receipt();
        model.setRowCount(0);
        lblTotal.setText("0.00");
        lblTatalNet.setText("0.00");
        lblChang.setText("0.00");
        lblCusName.setText("-");
        lblCusPoint.setText("-");
        lblDiscount.setText("0.00");
        txtCash.setText("");
        txtTel.setText("");

        enableform(false);
        enableCash(false);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel4 = new javax.swing.JPanel();
        scrReceipDetail = new javax.swing.JScrollPane();
        tblReceiptDetail = new javax.swing.JTable();
        jPanel5 = new javax.swing.JPanel();
        btnMember = new javax.swing.JButton();
        tbnNewMember = new javax.swing.JButton();
        btnPromotion = new javax.swing.JButton();
        btnCradit = new javax.swing.JButton();
        btnPrompay = new javax.swing.JButton();
        btnCash = new javax.swing.JButton();
        btnEditPro = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        scrProduceList = new javax.swing.JScrollPane();
        jPanel2 = new javax.swing.JPanel();
        btnConfirm = new javax.swing.JButton();
        jLabel7 = new javax.swing.JLabel();
        jLabel68 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        txtCash = new javax.swing.JTextField();
        lblTatalNet = new javax.swing.JLabel();
        lblTotal = new javax.swing.JLabel();
        lblChang = new javax.swing.JLabel();
        lblDiscount = new javax.swing.JLabel();
        jPanel6 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        lblCusName = new javax.swing.JLabel();
        lblCusPoint = new javax.swing.JLabel();
        txtTel = new javax.swing.JTextField();
        tbnConfirm = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        btnFood = new javax.swing.JButton();
        btnDressert = new javax.swing.JButton();
        btnDrink = new javax.swing.JButton();

        setBackground(new java.awt.Color(221, 206, 183));

        jPanel4.setBackground(new java.awt.Color(221, 206, 183));

        tblReceiptDetail.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        scrReceipDetail.setViewportView(tblReceiptDetail);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(scrReceipDetail, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(scrReceipDetail, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 230, Short.MAX_VALUE)
        );

        jPanel5.setBackground(new java.awt.Color(131, 100, 82));
        jPanel5.setPreferredSize(new java.awt.Dimension(644, 44));

        btnMember.setBackground(new java.awt.Color(242, 199, 99));
        btnMember.setFont(new java.awt.Font("Poppins Medium", 0, 14)); // NOI18N
        btnMember.setText("Member");
        btnMember.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMemberActionPerformed(evt);
            }
        });

        tbnNewMember.setBackground(new java.awt.Color(242, 199, 99));
        tbnNewMember.setFont(new java.awt.Font("Poppins Medium", 0, 14)); // NOI18N
        tbnNewMember.setText("New Member");
        tbnNewMember.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tbnNewMemberActionPerformed(evt);
            }
        });

        btnPromotion.setBackground(new java.awt.Color(242, 199, 99));
        btnPromotion.setFont(new java.awt.Font("Poppins Medium", 0, 14)); // NOI18N
        btnPromotion.setText("Promotion");
        btnPromotion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPromotionActionPerformed(evt);
            }
        });

        btnCradit.setBackground(new java.awt.Color(242, 199, 99));
        btnCradit.setFont(new java.awt.Font("Poppins Medium", 0, 14)); // NOI18N
        btnCradit.setText("CRADIT CARD");
        btnCradit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCraditActionPerformed(evt);
            }
        });

        btnPrompay.setBackground(new java.awt.Color(242, 199, 99));
        btnPrompay.setFont(new java.awt.Font("Poppins Medium", 0, 14)); // NOI18N
        btnPrompay.setText("PROMPAY");
        btnPrompay.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPrompayActionPerformed(evt);
            }
        });

        btnCash.setBackground(new java.awt.Color(242, 199, 99));
        btnCash.setFont(new java.awt.Font("Poppins Medium", 0, 14)); // NOI18N
        btnCash.setText("CASH");
        btnCash.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCashActionPerformed(evt);
            }
        });

        btnEditPro.setBackground(new java.awt.Color(242, 199, 99));

        btnEditPro.setFont(new java.awt.Font("Poppins Medium", 0, 14)); // NOI18N

        btnEditPro.setText("Edit Promotion");
        btnEditPro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditProActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(tbnNewMember)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnMember)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnEditPro, javax.swing.GroupLayout.PREFERRED_SIZE, 133, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnPromotion)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnCradit)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnPrompay)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnCash)
                .addContainerGap())
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addContainerGap(9, Short.MAX_VALUE)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnMember)
                    .addComponent(tbnNewMember)
                    .addComponent(btnPromotion)
                    .addComponent(btnCradit)
                    .addComponent(btnPrompay)
                    .addComponent(btnCash)
                    .addComponent(btnEditPro))
                .addContainerGap())
        );

        jPanel3.setBackground(new java.awt.Color(221, 206, 183));

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(scrProduceList)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(scrProduceList, javax.swing.GroupLayout.DEFAULT_SIZE, 499, Short.MAX_VALUE)
        );

        jPanel2.setBackground(new java.awt.Color(171, 146, 131));

        btnConfirm.setBackground(new java.awt.Color(242, 199, 99));
        btnConfirm.setFont(new java.awt.Font("Poppins Medium", 0, 14)); // NOI18N
        btnConfirm.setText("CONFRIM");
        btnConfirm.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnConfirmActionPerformed(evt);
            }
        });

        jLabel7.setFont(new java.awt.Font("Poppins Medium", 0, 12)); // NOI18N
        jLabel7.setText("Total :");

        jLabel68.setFont(new java.awt.Font("Poppins Medium", 0, 12)); // NOI18N
        jLabel68.setText("Discount :");

        jLabel9.setFont(new java.awt.Font("Poppins Medium", 0, 12)); // NOI18N
        jLabel9.setText("Total Net :");

        jLabel10.setFont(new java.awt.Font("Poppins Medium", 0, 12)); // NOI18N
        jLabel10.setText("Cash :");

        jLabel11.setFont(new java.awt.Font("Poppins Medium", 0, 12)); // NOI18N
        jLabel11.setText("Change :");

        txtCash.setFont(new java.awt.Font("Poppins", 0, 12)); // NOI18N
        txtCash.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCashActionPerformed(evt);
            }
        });

        lblTatalNet.setFont(new java.awt.Font("Poppins Medium", 0, 12)); // NOI18N
        lblTatalNet.setText("-");

        lblTotal.setFont(new java.awt.Font("Poppins Medium", 0, 12)); // NOI18N
        lblTotal.setText("-");

        lblChang.setFont(new java.awt.Font("Poppins Medium", 0, 12)); // NOI18N
        lblChang.setText("-");

        lblDiscount.setFont(new java.awt.Font("Poppins Medium", 0, 12)); // NOI18N
        lblDiscount.setText("-");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jLabel11, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel9, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel10, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel68, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblChang, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(txtCash, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(btnConfirm))
                            .addComponent(lblTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblDiscount, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblTatalNet, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblTotal))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel68, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblDiscount))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(lblTatalNet))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(txtCash, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnConfirm))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel11)
                    .addComponent(lblChang))
                .addContainerGap())
        );

        jPanel6.setBackground(new java.awt.Color(171, 146, 131));

        jLabel1.setFont(new java.awt.Font("Poppins Medium", 0, 12)); // NOI18N
        jLabel1.setText("Member");

        jLabel2.setFont(new java.awt.Font("Poppins Medium", 0, 12)); // NOI18N
        jLabel2.setText("Name : ");

        jLabel3.setFont(new java.awt.Font("Poppins Medium", 0, 12)); // NOI18N
        jLabel3.setText("Point : ");

        jLabel4.setFont(new java.awt.Font("Poppins Medium", 0, 12)); // NOI18N
        jLabel4.setText("Tel. : ");

        lblCusName.setFont(new java.awt.Font("Poppins Medium", 0, 12)); // NOI18N
        lblCusName.setText("-");

        lblCusPoint.setFont(new java.awt.Font("Poppins Medium", 0, 12)); // NOI18N
        lblCusPoint.setText("-");

        txtTel.setFont(new java.awt.Font("Poppins", 0, 12)); // NOI18N
        txtTel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtTelActionPerformed(evt);
            }
        });

        tbnConfirm.setBackground(new java.awt.Color(242, 199, 99));
        tbnConfirm.setFont(new java.awt.Font("Poppins Medium", 0, 14)); // NOI18N
        tbnConfirm.setText("CONFRIM");
        tbnConfirm.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tbnConfirmActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGap(17, 17, 17)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addGap(18, 18, 18)
                        .addComponent(lblCusPoint, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(jLabel4))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblCusName, javax.swing.GroupLayout.PREFERRED_SIZE, 139, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel6Layout.createSequentialGroup()
                                .addComponent(txtTel, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(tbnConfirm)))))
                .addContainerGap(12, Short.MAX_VALUE))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(txtTel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tbnConfirm))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblCusName)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(lblCusPoint))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel1.setBackground(new java.awt.Color(221, 206, 183));
        jPanel1.setPreferredSize(new java.awt.Dimension(280, 44));

        btnFood.setBackground(new java.awt.Color(242, 199, 99));
        btnFood.setFont(new java.awt.Font("Poppins Medium", 0, 14)); // NOI18N
        btnFood.setText("Food");
        btnFood.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFoodActionPerformed(evt);
            }
        });

        btnDressert.setBackground(new java.awt.Color(242, 199, 99));
        btnDressert.setFont(new java.awt.Font("Poppins Medium", 0, 14)); // NOI18N
        btnDressert.setText("Dressert");
        btnDressert.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDressertActionPerformed(evt);
            }
        });

        btnDrink.setBackground(new java.awt.Color(242, 199, 99));
        btnDrink.setFont(new java.awt.Font("Poppins Medium", 0, 14)); // NOI18N
        btnDrink.setText("Drink");
        btnDrink.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDrinkActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(btnDrink)
                .addGap(18, 18, 18)
                .addComponent(btnDressert)
                .addGap(18, 18, 18)
                .addComponent(btnFood)
                .addGap(0, 93, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnDrink)
                    .addComponent(btnDressert)
                    .addComponent(btnFood))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 361, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(1, 1, 1)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(7, 7, 7))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(26, 26, 26))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnConfirmActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnConfirmActionPerformed
        calculateValues();
        //System.out.println("" + receipt);
        receiptService.addNew(receipt);
        openReceiptDialog();
        refreshReceipt();
    }//GEN-LAST:event_btnConfirmActionPerformed

    private void btnDrinkActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDrinkActionPerformed
        productListPanel.updateProductList("1");
        scrProduceList.setViewportView(productListPanel);
    }//GEN-LAST:event_btnDrinkActionPerformed

    private void btnDressertActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDressertActionPerformed
        productListPanel.updateProductList("2");
        scrProduceList.setViewportView(productListPanel);
    }//GEN-LAST:event_btnDressertActionPerformed

    private void btnFoodActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFoodActionPerformed
        productListPanel.updateProductList("3");
        scrProduceList.setViewportView(productListPanel);
    }//GEN-LAST:event_btnFoodActionPerformed

    private void txtCashActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCashActionPerformed
        float cashAmount = Float.parseFloat(txtCash.getText());
        receipt.setReceiptPayment(cashAmount);
    }//GEN-LAST:event_txtCashActionPerformed

    private void btnMemberActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMemberActionPerformed
        enableform(true);
    }//GEN-LAST:event_btnMemberActionPerformed

    private void tbnNewMemberActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tbnNewMemberActionPerformed
        editedCustomer = new Customer();
        openCustomerDialog();
    }//GEN-LAST:event_tbnNewMemberActionPerformed

    private void btnPromotionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPromotionActionPerformed
        openPromotionDialog();
    }//GEN-LAST:event_btnPromotionActionPerformed

    private void txtTelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtTelActionPerformed
        String tel = txtTel.getText().trim();
    }//GEN-LAST:event_txtTelActionPerformed

    private void tbnConfirmActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tbnConfirmActionPerformed
        String tel = txtTel.getText().trim();
        CustomerService customerService = new CustomerService();
        CustomerDao customerDao = new CustomerDao();
        customer = customerService.getByTel(tel);
        customerId = customerDao.getByTel(tel);
        receipt.setCusId(customerId.getId());
        if (customer != null) {
            receipt.calculateTotal();

            if (receipt.getReceiptTotal() >= 100.0) {
                customer.setCUST_P_VALUE(customer.getCUST_P_VALUE() + 1);
                customer = customerService.updateCustomerPoints(customer);
            }

            double value = customer.getCUST_P_VALUE();
            String formattedValue = String.format("%.2f", value);
            lblCusPoint.setText(formattedValue);
            lblCusName.setText(customer.getFname());

            System.out.println("Customer found: " + customer.getFname() + " " + customer.getLname());
        } else {
            System.out.println("Customer not found for tel: " + tel);
        }

    }//GEN-LAST:event_tbnConfirmActionPerformed

    private void btnCraditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCraditActionPerformed
        openCreditDialog();
        txtCash.setText("" + receipt.getReceiptNetTotal());
    }//GEN-LAST:event_btnCraditActionPerformed

    private void btnCashActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCashActionPerformed
        enableCash(true);
    }//GEN-LAST:event_btnCashActionPerformed

    private void btnPrompayActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPrompayActionPerformed
        openPormpayDialog();
        txtCash.setText("" + receipt.getReceiptNetTotal());
    }//GEN-LAST:event_btnPrompayActionPerformed

    private void btnEditProActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditProActionPerformed
        openPromotionPanel();
    }//GEN-LAST:event_btnEditProActionPerformed
    public void updateDiscountLabel(float discount) {
        lblDiscount.setText(String.format("%.2f", discount));
    }

    public void updateTotalNet(float totalNet) {
        lblTatalNet.setText(String.format("%.2f", totalNet));
    }

    private void calculateValues() {
        float totalNet = receipt.getReceiptNetTotal();
        float total = receipt.getReceiptTotal();
        float discount = receipt.getReceiptDiscount();
        float payment = receipt.getReceiptPayment();
        cashAmount = Float.parseFloat(txtCash.getText());

        float change = cashAmount - totalNet;

        if (txtCash.getText().isEmpty()) {
            JOptionPane.showMessageDialog(this, "Please Input Money.");
        } else if (Float.parseFloat(txtCash.getText()) < totalNet) {
            txtCash.setText("");
            JOptionPane.showMessageDialog(this, "Money Not Enough.");
        } else {
            discount = receipt.getReceiptDiscount();
            lblChang.setText(String.format("%.2f", change));
            lblDiscount.setText(String.format("%.2f", discount));
            receipt.setReceiptChange(change);
            receipt.setReceiptNetTotal(totalNet);
            receipt.setReceiptPayment(cashAmount);
            receipt.setReceiptTotal(total);
            receipt.setReceiptDate(date);
        }
    }

    private void openCustomerDialog() {
        JFrame frame = (JFrame) SwingUtilities.getRoot(this);
        CustomerDialog customerDialog = new CustomerDialog(frame, editedCustomer);
        customerDialog.setLocationRelativeTo(this);
        customerDialog.setVisible(true);
        customerDialog.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                // refreshTable();
            }

        });
    }

    private void openPormpayDialog() {
        JFrame frame = (JFrame) SwingUtilities.getRoot(this);
        PrompayDialog prompayDialog = new PrompayDialog(frame, true);
        prompayDialog.setLocationRelativeTo(this);
        prompayDialog.setVisible(true);
        prompayDialog.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                // refreshTable();
            }

        });
    }

    private void openCreditDialog() {
        JFrame frame = (JFrame) SwingUtilities.getRoot(this);
        CraditDialog craditDialog = new CraditDialog(frame, true);
        craditDialog.setLocationRelativeTo(this);
        craditDialog.setVisible(true);
        craditDialog.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                // refreshTable();
            }

        });
    }

    private void openPromotionDialog() {

        JFrame frame = (JFrame) SwingUtilities.getRoot(this);
        PromotionDialog promotionDialog = new PromotionDialog(frame, true, receipt, this, customer);
        promotionDialog.setLocationRelativeTo(this);
        promotionDialog.setVisible(true);
        promotionDialog.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                // refreshTable();
            }

        });
    }

    private void openReceiptDialog() {
        JFrame frame = (JFrame) SwingUtilities.getRoot(this);
        ReceiptDialog receiptDialog = new ReceiptDialog(frame, true, this, receipt, customer);
        receiptDialog.setLocationRelativeTo(this);
        receiptDialog.setVisible(true);

    }

    private void openPromotionPanel() {
        JFrame frame = (JFrame) SwingUtilities.getWindowAncestor(this);
        PromotionPanel promotionPanel = new PromotionPanel(frame, showPromotion);
        promotionPanel.setLocationRelativeTo(this.getParent());
        promotionPanel.setVisible(true);
        promotionPanel.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                // refreshTable();
            }

        });
    }

    public void enableform(boolean isEnable) {
        txtTel.setEditable(isEnable);
    }

    public void enableTel(boolean isEnable) {
        txtTel.setEditable(isEnable);
    }

    public void enableCash(boolean isEnable) {
        txtCash.setEditable(isEnable);
    }
 

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCash;
    private javax.swing.JButton btnConfirm;
    private javax.swing.JButton btnCradit;
    private javax.swing.JButton btnDressert;
    private javax.swing.JButton btnDrink;
    private javax.swing.JButton btnEditPro;
    private javax.swing.JButton btnFood;
    private javax.swing.JButton btnMember;
    private javax.swing.JButton btnPromotion;
    private javax.swing.JButton btnPrompay;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel68;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JLabel lblChang;
    private javax.swing.JLabel lblCusName;
    private javax.swing.JLabel lblCusPoint;
    private javax.swing.JLabel lblDiscount;
    private javax.swing.JLabel lblTatalNet;
    private javax.swing.JLabel lblTotal;
    private javax.swing.JScrollPane scrProduceList;
    private javax.swing.JScrollPane scrReceipDetail;
    private javax.swing.JTable tblReceiptDetail;
    private javax.swing.JButton tbnConfirm;
    private javax.swing.JButton tbnNewMember;
    private javax.swing.JTextField txtCash;
    private javax.swing.JTextField txtTel;
    // End of variables declaration//GEN-END:variables

    public void buy(Product product, int qty) {
        receipt.addReceiptDetail(product, qty);
        refreshReceipt();
    }

}
