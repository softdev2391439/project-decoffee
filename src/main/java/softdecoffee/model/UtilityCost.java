/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package softdecoffee.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Nobpharat
 */
public class UtilityCost {

	private int id;
	private double electric;
	private double water;
	private double rental;
	private double total;
	private String dateStraing;
	private Date date;

	public UtilityCost(int id, double electric, double water, double rental, double total,Date date) {
		this.id = id;
		this.electric = electric;
		this.water = water;
		this.rental = rental;
		this.total = total;
		this.date = date;
	}
	public UtilityCost(double electric, double water, double rental, double total,Date date) {
		this.id = -1;
		this.electric = electric;
		this.water = water;
		this.rental = rental;
		this.total = total;
		this.date = date;
	}
	public UtilityCost(double electric, double water, double rental, double total,String date) {
		this.id = -1;
		this.electric = electric;
		this.water = water;
		this.rental = rental;
		this.total = total;
		this.dateStraing = date;
	}
	
	public UtilityCost() {
		this.id =-1;
		this.electric = 0;
		this.water = 0;
		this.rental = 0;
		this.total = 0;
		this.date = null;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getElectric() {
		return electric;
	}

	public void setElectric(double electric) {
		this.electric = electric;
	}

	public double getWater() {
		return water;
	}

	public void setWater(double water) {
		this.water = water;
	}

	public double getRental() {
		return rental;
	}

	public void setRental(double rental) {
		this.rental = rental;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public String getDateString() {
		return dateStraing;
	}

	public void setDateString(String dateStraing) {
		this.dateStraing = dateStraing;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	@Override
	public String toString() {
		return "UtilityCost{" + "id=" + id + ", electric=" + electric + ", water=" + water + ", rental=" + rental + ", total=" + total + ", date=" + date + '}';
	}

	public static UtilityCost fromRS(ResultSet rs) {
		UtilityCost utilityCost = new UtilityCost();
		try {
			utilityCost.setId(rs.getInt("UC_CODE"));
			utilityCost.setElectric(rs.getDouble("UC_ELECTRIC"));
			utilityCost.setWater(rs.getDouble("UC_WATER"));
			utilityCost.setRental(rs.getDouble("UC_RENTAL"));
			utilityCost.setTotal(rs.getDouble("UC_TOTAL"));
			utilityCost.setDate(rs.getTimestamp("UC_DATE"));
		} catch (SQLException ex) {
			Logger.getLogger("" +utilityCost.getId()).log(Level.SEVERE, null, ex);
			return null;
		}
		return utilityCost;
	}
}
