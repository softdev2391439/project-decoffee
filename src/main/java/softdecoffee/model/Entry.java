/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package softdecoffee.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author sarit
 */
public class Entry {

    private int id;
    private int employeeId;
    private Date date;
    private Date timein;
    private Date timeout;
    private String status;
    private int totalHR;
    private String timeoutStr;

    public Entry(int id, int employeeId, Date date, Date timein, Date timeout, String status, int totalHR) {
        this.id = id;
        this.employeeId = employeeId;
        this.date = date;
        this.timein = timein;
        this.timeout = timeout;
        this.status = status;
        this.totalHR = totalHR;
    }

    public Entry(int employeeId, Date date, Date timein, Date timeout, String status, int totalHR) {
        this.id = -1;
        this.employeeId = employeeId;
        this.date = date;
        this.timein = timein;
        this.timeout = timeout;
        this.status = status;
        this.totalHR = totalHR;
    }
    

    public Entry(int employeeId,Date timein, Date timeout, String status, int totalHR) {
        this.id = -1;
        this.employeeId = employeeId;
        this.date = null;
        this.timein = timein;
        this.timeout = timeout;
        this.status = status;
        this.totalHR = totalHR;
    }

    public Entry() {
        this.id = -1;
        this.employeeId = 0;
        this.date = null;
        this.timein = null;
        this.timeout = null;
        this.status = "";
        this.totalHR = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getTimein() {
        return timein;
    }

    public void setTimein(Date timein) {
        this.timein = timein;
    }

    public Date getTimeout() {
        return timeout;
    }

    public void setTimeout(Date timeout) {
        this.timeout = timeout;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getTotalHR() {
        return totalHR;
    }

    public void setTotalHR(int totalHR) {
        this.totalHR = totalHR;
    }
        public String getTimeoutStr() {
		return timeoutStr;
	}

	public void setTimeoutStr(String timeoutStr) {
		this.timeoutStr = timeoutStr;
	}


    @Override
    public String toString() {
        return "Entry{" + "id=" + id + ", employeeId=" + employeeId + ", date=" + date + ", timein=" + timein + ", timeout=" + timeout + ", status=" + status + ", totalHR=" + totalHR + '}';
    }

    

    public static Entry fromRS(ResultSet rs) {
        Entry entry = new Entry();
        try {
            entry.setId(rs.getInt("ENT_CODE"));
            entry.setEmployeeId(rs.getInt("EMP_CODE"));
            entry.setDate(rs.getDate("ENT_DATE"));
            entry.setTimein(rs.getTime("ENT_TIME_IN"));
            entry.setTimeout(rs.getTime("ENT_TIME_OUT"));
            entry.setStatus(rs.getString("ENT_STATUS"));
            entry.setTotalHR(rs.getInt("ENT_TOTAL_HR"));

        } catch (SQLException ex) {
            Logger.getLogger(Entry.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return entry;
    }

    public void setTimeout(String formattedTime) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
}
