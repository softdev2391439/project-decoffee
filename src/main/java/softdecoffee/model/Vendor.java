/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package softdecoffee.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Puri
 */
public class Vendor {
    private int id;
    private String country;
    private String address;
    private String tel;
    private String name;
    private String contact;
    private String category;

    public Vendor(int id, String country, String address, String tel, String name, String contact, String category) {
        this.id = id;
        this.country = country;
        this.address = address;
        this.tel = tel;
        this.name = name;
        this.contact = contact;
        this.category = category;
    }
    public Vendor(String country, String address, String tel, String name, String contact, String category) {
        this.id = -1;
        this.country = country;
        this.address = address;
        this.tel = tel;
        this.name = name;
        this.contact = contact;
        this.category = category;
    }
    public Vendor() {
        this.id = -1;
        this.country = "";
        this.address = "";
        this.tel = "";
        this.name = "";
        this.contact = "";
        this.category = "";
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    @Override
    public String toString() {
        return "Vendor{" + "id=" + id + ", country=" + country + ", address=" + address + ", tel=" + tel + ", name=" + name + ", contact=" + contact + ", category=" + category + '}';
    }
    
    public static Vendor fromRS(ResultSet rs) {
        Vendor vendor = new Vendor();
        try {
            vendor.setId(rs.getInt("V_CODE"));
            vendor.setCountry(rs.getString("V_COUNTRY"));
            vendor.setAddress(rs.getString("V_ADDRESS"));
            vendor.setTel(rs.getString("V_PHONE"));
            vendor.setName(rs.getString("V_NAME"));
            vendor.setContact(rs.getString("V_CONTACT"));
            vendor.setCategory(rs.getString("V_CATEGORY"));
        } catch (SQLException ex) {
            Logger.getLogger(Vendor.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return vendor;
    }
}

