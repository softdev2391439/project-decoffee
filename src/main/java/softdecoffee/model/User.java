package softdecoffee.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
/**
 *
 * @author natta
 */
public class User {
    private int Id;
    private String Login;
    private String Name;
    private String Password;
    private String Gender;
    private int Role;
    private Date LastLogin;
    private Date RegisDate;

    public User(int Id,String Login,String Name, String Password, String Gender, int Role, Date LastLogin, Date RegisDate) {
        this.Id = Id;
        this.Login = Login;
        this.Name = Name;
        this.Password = Password;
        this.Gender = Gender;
        this.Role = Role;
        this.LastLogin = LastLogin;
        this.RegisDate = RegisDate;
    }

    public User(String Name,String Login ,String Password, String Gender, int Role) {
        this.Id = -1;
        this.Login = Login;
        this.Name = Name;
        this.Password = Password;
        this.Gender = Gender;
        this.Role = Role;
        this.LastLogin = null;
        this.RegisDate = null;
    }

    public User() {
        this.Id = -1;
        this.Login = "";
        this.Name = "";
        this.Password = "";
        this.Gender = "";
        this.Role = 1;
        this.LastLogin = null;
        this.RegisDate = null;
    }

    public int getId() {
        return Id;
    }

    public void setId(int Id) {
        this.Id = Id;
    }

    public String getLogin() {
        return Login;
    }

    public void setLogin(String Login) {
        this.Login = Login;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String Password) {
        this.Password = Password;
    }

    public String getGender() {
        return Gender;
    }

    public void setGender(String Gender) {
        this.Gender = Gender;
    }

    public int getRole() {
        return Role;
    }

    public void setRole(int Role) {
        this.Role = Role;
    }

    public Date getLastLogin() {
        return LastLogin;
    }

    public void setLastLogin(Date LastLogin) {
        this.LastLogin = LastLogin;
    }

    public Date getRegisDate() {
        return RegisDate;
    }

    public void setRegisDate(Date RegisDate) {
        this.RegisDate = RegisDate;
    }

    @Override
    public String toString() {
        return "User{" + "Id=" + Id + ", Login=" + Login + ", Name=" + Name + ", Password=" + Password + ", Gender=" + Gender + ", Role=" + Role + ", LastLogin=" + LastLogin + ", RegisDate=" + RegisDate + '}';
    }

    
    
    public static User fromRS(ResultSet rs) {
        User user = new User();
        try {
            user.setId(rs.getInt("U_ID"));
            user.setLogin(rs.getString("U_LOGIN"));
            user.setName(rs.getString("U_NAME"));
            user.setPassword(rs.getString("U_PASSWORD"));
            user.setGender(rs.getString("U_GENDER"));
            user.setRole(rs.getInt("U_ROLE"));
            user.setLastLogin(rs.getTimestamp("U_LASTLOGIN"));
            user.setRegisDate(rs.getTimestamp("U_REGISTATIONDATE"));
        } catch (SQLException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return user;
    }
}
