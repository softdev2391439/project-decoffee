/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package softdecoffee.model;

/**
 *
 * @author Lenovo
 */
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Date;
import softdecoffee.dao.CheckStockDao;
import softdecoffee.service.CheckStockDetailService;
import softdecoffee.service.CheckStockService;

/**
 *
 * @author Lenovo
 */
public class CheckStock {
    private int Id;
    private int employeeId;
    private Date checkDate;
    private ArrayList<CheckStockDetail> checkStockDetails = new ArrayList();
    private int nextId = 1;

    public CheckStock(int Id, int employeeId, Date checkDate) {
        this.Id = Id;
        this.employeeId = employeeId;
        this.checkDate = checkDate;
    }
    
    public CheckStock(int employeeId) {
        this.Id = nextId++;
        this.employeeId = employeeId;
        this.checkDate = new Date();
    }
    
    public CheckStock() {
        this.Id = nextId++;
        this.employeeId = -1;
        this.checkDate = null;
    }

    public int getId() {
        return Id;
    }

    public void setId(int Id) {
        this.Id = Id;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public Date getCheckDate() {
        return checkDate;
    }

    public void setCheckDate(Date checkDate) {
        this.checkDate = checkDate;
    }

    public ArrayList<CheckStockDetail> getCheckStockDetails() {
        return checkStockDetails;
    }

    public void setCheckStockDetails(ArrayList<CheckStockDetail> checkStockDetails) {
        this.checkStockDetails = checkStockDetails;
    }

    @Override
    public String toString() {
        return "CheckStock{" + "Id=" + Id + ", employeeId=" + employeeId + ", checkDate=" + checkDate + ", checkStockDetails=" + checkStockDetails + '}';
    }

   
    public void addCheckStockDetail(CheckStockDetail checkStockDetail) {
        CheckStockDetail csd = new CheckStockDetail(checkStockDetail.getId(),checkStockDetail.getMaterialId(),checkStockDetail.getCheckStockId(),checkStockDetail.getRemainingAmount(), checkStockDetail.getExpAmount(), checkStockDetail.getTotalLost());
        checkStockDetails.add(csd);
    }
    
    public void addCheckStockDetail(int checkStockId,Material material,double materialQoh,double qty){
       CheckStockDetail csd = new CheckStockDetail(material.getId(), checkStockId, materialQoh+qty, 00, 00);
       checkStockDetails.add(csd);
        //calculateTotal(); 
    }
    
    public void clearCheckStockDetails() {
        checkStockDetails.clear();
    }
  
    public void delCheckStockDetail(CheckStockDetail checkStockDetail){
        checkStockDetails.remove(checkStockDetail);
    }
    

    public static CheckStock fromRS(ResultSet rs) {
        CheckStock checkStock = new CheckStock();
        try {
            checkStock.setId(rs.getInt("CHECK_CODE"));
            checkStock.setEmployeeId(rs.getInt("EMP_CODE"));
            checkStock.setCheckDate(rs.getTimestamp("CHECK_DATETIME"));        
        } catch (SQLException ex) {
            Logger.getLogger(""+checkStock.getId()).log(Level.SEVERE, null, ex);
            return null;
        }
        return checkStock;
    }
    
    
}
