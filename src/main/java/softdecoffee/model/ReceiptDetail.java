/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package softdecoffee.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import softdecoffee.dao.ReceiptDao;

/**
 *
 * @author Admin
 */
public class ReceiptDetail {
    private int id;
    private int receiptId;
    private int productId;
    private String productName;
    private int receiptDetailQty;
    private float receiptDetailUnitPrice;
    private float receiptDetailTotal;
    private float receiptDetailDiscount;
    private int promotionDetailId;
    private Product product = new Product();
    private ResultSet rs;
    public Object selectedSize;

    public ReceiptDetail(int receiptId, int productId, String productName, int receiptDetailQty, float receiptDetailUnitPrice, float receiptDetailTotal, float receiptDetailDiscount, int promotionDetailId) {
        this.receiptId = receiptId;
        this.productId = productId;
        this.productName = productName;
        this.receiptDetailQty = receiptDetailQty;
        this.receiptDetailUnitPrice = receiptDetailUnitPrice;
        this.receiptDetailTotal = receiptDetailTotal;
        this.receiptDetailDiscount = receiptDetailDiscount;
        this.promotionDetailId = promotionDetailId;
    }
    
    
    

    public ReceiptDetail(int id, int receiptId, int productId, String productName, int receiptDetailQty, float receiptDetailUnitPrice) {
        this.id = id;
        this.receiptId = receiptId;
        this.productId = productId;
        this.productName = productName;
        this.receiptDetailQty = receiptDetailQty;
        this.receiptDetailUnitPrice = receiptDetailUnitPrice;
    }

    public ReceiptDetail(int receiptId, int productId, int receiptDetailQty, float receiptDetailUnitPrice, float receiptDetailTotal, float receiptDetailDiscount, int promotionDetailId) {
        this.id = -1;
        this.receiptId = receiptId;
        this.productId = productId;
        this.receiptDetailQty = receiptDetailQty;
        this.receiptDetailUnitPrice = receiptDetailUnitPrice;
        this.receiptDetailTotal = receiptDetailTotal;
        this.receiptDetailDiscount = receiptDetailDiscount;
        this.promotionDetailId = promotionDetailId;
    }

    public ReceiptDetail(int produceId, String produceName, float receiptDetailUnitPrice, int receiptDetailQty, float receiptDetailTotal, int receiptId) {
        this.id = -1;
        this.productId = produceId;
        this.productName = produceName;
        this.receiptDetailUnitPrice = receiptDetailUnitPrice;
        this.receiptDetailQty = receiptDetailQty;
        this.receiptDetailTotal = receiptDetailTotal;
        this.receiptId = receiptId;
    }
    

    public ReceiptDetail() {
        this.id = -1;
        this.receiptId = 0;
        this.productId = 0;
        this.productName = "";
        this.receiptDetailQty = 0;
        this.receiptDetailUnitPrice = 0;
        this.receiptDetailTotal = 0;
        this.receiptDetailDiscount = 0;
        this.promotionDetailId = 0;
        this.product = Product.fromRS(rs);
        
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getReceiptId() {
        return receiptId;
    }

    public void setReceiptId(int receiptId) {
        this.receiptId = receiptId;
    }

    public  int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getReceiptDetailQty() {
        return receiptDetailQty;
    }

    public void setReceiptDetailQty(int receiptDetailQty) {
        this.receiptDetailQty = receiptDetailQty;
    }

    public float getReceiptDetailUnitPrice() {
        return receiptDetailUnitPrice;
    }

    public void setReceiptDetailUnitPrice(float receiptDetailUnitPrice) {
        this.receiptDetailUnitPrice = receiptDetailUnitPrice;
    }

    public float getReceiptDetailTotal() {
        return receiptDetailTotal;
    }

    public void setReceiptDetailTotal(float receiptDetailTotal) {
        this.receiptDetailTotal = receiptDetailTotal;
    }

    public float getReceiptDetailDiscount() {
        return receiptDetailDiscount;
    }

    public void setReceiptDetailDiscount(float receiptDetailDiscount) {
        this.receiptDetailDiscount = receiptDetailDiscount;
    }

    public int getPromotionDetailId() {
        return promotionDetailId;
    }

    public void setPromotionDetailId(int promotionDetailId) {
        this.promotionDetailId = promotionDetailId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public  Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    @Override
    public String toString() {
        return "ReceiptDetail{" + "id=" + id + ", receiptId=" + receiptId + ", productId=" + productId + ", productName=" + productName + ", receiptDetailQty=" + receiptDetailQty + ", receiptDetailUnitPrice=" + receiptDetailUnitPrice + ", receiptDetailTotal=" + receiptDetailTotal + ", receiptDetailDiscount=" + receiptDetailDiscount + ", promotionDetailId=" + promotionDetailId + '}';
    }

   public static ReceiptDetail fromRs(ResultSet rs) {
        ReceiptDetail receiptDetail = new ReceiptDetail();
        try {
            receiptDetail.setId(rs.getInt("RCD_CODE"));
            receiptDetail.setReceiptId(rs.getInt("RE_CODE"));
            receiptDetail.setProductId(rs.getInt("PROD_CODE"));
            receiptDetail.setReceiptDetailQty(rs.getInt("RCD_QTY"));
            receiptDetail.setReceiptDetailUnitPrice(rs.getFloat("RCD_UNIT_PRICE"));
            receiptDetail.setReceiptDetailTotal(rs.getFloat("RCD_TOTAL"));
            receiptDetail.setReceiptDetailDiscount(rs.getFloat("RCD_DISCOUNT"));
            receiptDetail.setPromotionDetailId(rs.getInt("PRDE_CODE"));
            //receiptDetail.setProductSize(rs.getString("RCD_SIZE"));
            //receiptDetail.setProductSweet(rs.getString("RCD_SWEET"));
            //receiptDetail.setProductType(rs.getString("RCD_TYPE"));
        } catch (SQLException ex) {
            Logger.getLogger(ReceiptDetail.class.getName()).log(Level.SEVERE, null, ex);
        }
        return receiptDetail;
    }
}
