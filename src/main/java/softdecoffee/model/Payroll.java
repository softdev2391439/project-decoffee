/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package softdecoffee.model;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author werapan
 */
public class Payroll {

    private int id;
    private int employeeId;
    private String status;
    private Date date;
    private int entryId;
    private float totalPay;
    private float priceHour;

    public Payroll(int id, int employeeId, String status, Date date, int entryId, float totalPay, float priceHour) {
        this.id = id;
        this.employeeId = employeeId;
        this.status = status;
        this.date = date;
        this.entryId = entryId;
        this.totalPay = totalPay;
        this.priceHour = priceHour;
    }

    public Payroll(int employeeId, String status, Date date, int entryId, float totalPay, float priceHour) {
        this.id = -1;
        this.employeeId = employeeId;
        this.status = status;
        this.date = date;
        this.entryId = entryId;
        this.totalPay = totalPay;
        this.priceHour = priceHour;
    }

    public Payroll(int employeeId, String status, int entryId, float totalPay, float priceHour) {
        this.id = -1;
        this.employeeId = employeeId;
        this.status = status;
        this.date = null;
        this.entryId = entryId;
        this.totalPay = totalPay;
        this.priceHour = priceHour;
    }

    public Payroll() {
        this.id = -1;
        this.employeeId = 0;
        this.status = "";
        this.date = null;
        this.entryId = 0;
        this.totalPay = 0;
        this.priceHour = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getEntryId() {
        return entryId;
    }

    public void setEntryId(int entryId) {
        this.entryId = entryId;
    }

    public float getTotalPay() {
        return totalPay;
    }

    public void setTotalPay(float totalPay) {
        this.totalPay = totalPay;
    }

    public float getPriceHour() {
        return priceHour;
    }

    public void setPriceHour(float priceHour) {
        this.priceHour = priceHour;
    }

    @Override
    public String toString() {
        return "Payroll{" + "id=" + id + ", employeeId=" + employeeId + ", status=" + status + ", date=" + date + ", entryId=" + entryId + ", totalPay=" + totalPay + ", priceHour=" + priceHour + '}';
    }

    public static Payroll fromRS(ResultSet rs) {
        Payroll payroll = new Payroll();
        try {
            payroll.setId(rs.getInt("PRL_CODE"));
            payroll.setEmployeeId(rs.getInt("EMP_CODE"));
            payroll.setStatus(rs.getString("PRL_STATUS"));
            payroll.setDate(rs.getDate("PRL_DATE"));
            payroll.setEntryId(rs.getInt("ENT_CODE"));
            payroll.setTotalPay(rs.getFloat("PRL_TOTAL_PAY"));
            payroll.setPriceHour(rs.getFloat("PRL_PRICE_HR"));

        } catch (SQLException ex) {
            Logger.getLogger(Payroll.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return payroll;
    }
}