/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package softdecoffee.model;

import java.util.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author basba
 */
public class Promotion {

    private int id;
    private String name;
    private String start;
    private String end;
    private String detail;
    private String status;
    private Date startDate;
    private Date endDate;
    private ArrayList<PromotionDetail> promotionDetails = new ArrayList();

    public Promotion(int id, String name, String start, String end, String detail, String status) {
        this.id = id;
        this.name = name;
        this.start = start;
        this.end = end;
        this.detail = detail;
        this.status = status;
    }

    public Promotion(String name, String start, String end, String detail, String status) {
        this.id = -1;
        this.name = name;
        this.start = start;
        this.end = end;
        this.detail = detail;
        this.status = status;
    }

    public Promotion() {
        this.id = -1;
        this.name = "";
        this.start = "";
        this.end = "";
        this.detail = "";
        this.status = "";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public ArrayList<PromotionDetail> getPromotionDetails() {
        return promotionDetails;
    }

    public void setPromotionDetails(ArrayList<PromotionDetail> promotionDetails) {
        this.promotionDetails = promotionDetails;
    }
    
    @Override
    public String toString() {
        return "Promotion{" + "id=" + id + ", name=" + name + ", start=" + start + ", end=" + end + ", detail=" + detail + ", status=" + status + ", startDate=" + startDate + ", endDate=" + endDate + ", promotionDetails=" + promotionDetails + '}';
    }

    public void addPromotionDetail(PromotionDetail promotionDetail) {
        PromotionDetail pd = new PromotionDetail(promotionDetail.getId(), promotionDetail.getRcId(), promotionDetail.getProId(), promotionDetail.getDiscount());
        promotionDetails.add(pd);
    }
    
    public void addPromotionDetail(PromotionDetail promotionDetail, int qty) {
        PromotionDetail pd = new PromotionDetail(promotionDetail.getRcId(),promotionDetail.getProId() , promotionDetail.getDiscount());
        promotionDetails.add(pd);
    }
        public void clearPromotionDetails() {
        promotionDetails.clear();
    }
  
    public void delPromotionDetail(PromotionDetail promotionDetail){
        promotionDetails.remove(promotionDetail);
    }

    public static Promotion fromRS(ResultSet rs) {
        Promotion promotion = new Promotion();
        try {
            promotion.setId(rs.getInt("PRO_CODE"));
            promotion.setName(rs.getString("PRO_NAME"));
            String startDate = rs.getString("PRO_STARTDATE");
            String endDate = rs.getString("PRO_ENDDATE");
            promotion.setDetail(rs.getString("PRO_DETAIL"));
            promotion.setStatus(rs.getString("PRO_STATUS"));
            if (startDate != null) {
                try {
                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                    java.util.Date parsedDate = dateFormat.parse(startDate);
                    promotion.setStartDate(new java.sql.Date(parsedDate.getTime()));
                } catch (ParseException ex) {
                    Logger.getLogger(Employee.class.getName()).log(Level.SEVERE, null, ex);
                    promotion.setStartDate(null);
                }
            } else {
                promotion.setStartDate(null);
            }
            if (endDate != null) {
                try {
                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                    java.util.Date parsedDate = dateFormat.parse(endDate);
                    promotion.setEndDate(new java.sql.Date(parsedDate.getTime()));
                } catch (ParseException ex) {
                    Logger.getLogger(Employee.class.getName()).log(Level.SEVERE, null, ex);
                    promotion.setEndDate(null);
                }
            } else {
                promotion.setEndDate(null);
            }

        } catch (SQLException ex) {
            Logger.getLogger(Promotion.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return promotion;
    }

}
