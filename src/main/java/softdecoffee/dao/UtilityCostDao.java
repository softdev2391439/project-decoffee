/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package softdecoffee.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import softdecoffee.helper.DatabaseHelper;
import softdecoffee.model.UtilityCost;

/**
 *
 * @author Nobpharat
 */
public class UtilityCostDao implements Dao<UtilityCost> {

	@Override
	public UtilityCost get(int id) {
		UtilityCost utilityCost = null;
		String sql = "SELECT * FROM UTILITY_COST WHERE UC_CODE=?";
		Connection conn = DatabaseHelper.getConnect();
		try {
			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setInt(1, id);
			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {
				utilityCost = UtilityCost.fromRS(rs);
			}

		} catch (SQLException ex) {
			System.out.println(ex.getMessage());
		}
		return utilityCost;
	}


	public List<UtilityCost> getAll() {
		ArrayList<UtilityCost> list = new ArrayList();
		String sql = " SELECT * FROM UTILITY_COST ";
		Connection conn = DatabaseHelper.getConnect();
		try {
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				UtilityCost utilityCost = UtilityCost.fromRS(rs);
				list.add(utilityCost);

			}

		} catch (SQLException ex) {
			System.out.println(ex.getMessage());
		}
		return list;
	}

	@Override
	public List<UtilityCost> getAll(String where, String order) {
		ArrayList<UtilityCost> list = new ArrayList();
		String sql = " SELECT * FROM UTILITY_COST where " + where + " ORDER BY " + order;
		Connection conn = DatabaseHelper.getConnect();
		try {
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				UtilityCost utilityCost = UtilityCost.fromRS(rs);
				list.add(utilityCost);

			}

		} catch (SQLException ex) {
			System.out.println(ex.getMessage());
		}
		return list;
	}

	public List<UtilityCost> getAll(String order) {
		ArrayList<UtilityCost> list = new ArrayList();
		String sql = " SELECT * FROM UTILITY_COST  ORDER BY " + order;
		Connection conn = DatabaseHelper.getConnect();
		try {
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				UtilityCost utilityCost = UtilityCost.fromRS(rs);
				list.add(utilityCost);

			}

		} catch (SQLException ex) {
			System.out.println(ex.getMessage());
		}
		return list;
	}

	public UtilityCost save(UtilityCost obj) {

		String sql = " INSERT INTO UTILITY_COST(UC_ELECTRIC,UC_WATER,UC_RENTAL,UC_TOTAL,UC_DATE)"
			+ "VALUES(?, ?, ?, ?, ?)";
		Connection conn = DatabaseHelper.getConnect();
		try {
			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setDouble(1, obj.getElectric());
			stmt.setDouble(2, obj.getWater());
			stmt.setDouble(3, obj.getRental());
			stmt.setDouble(4, obj.getRental());
			stmt.setString(5, obj.getDateString());
			//            System.out.println(stmt);
			stmt.executeUpdate();
			int id = DatabaseHelper.getInsertedId(stmt);
			obj.setId(id);
		} catch (SQLException ex) {
			System.out.println(ex.getMessage());
			return null;
		}
		return obj;
	}

	@Override
	public UtilityCost update(UtilityCost obj) {
		String sql = " UPDATE UTILITY_COST"
			+ " SET UC_ELECTRIC = ?, UC_WATER = ?, UC_RENTAL = ?, UC_TOTAL = ?, UC_DATE = ?"
			+ " WHERE UC_CODE = ?";

		Connection conn = DatabaseHelper.getConnect();
		try {
			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setDouble(1, obj.getElectric());
			stmt.setDouble(2, obj.getWater());
			stmt.setDouble(3, obj.getRental());
			stmt.setDouble(4, obj.getRental());
			stmt.setString(5, obj.getDateString());
			stmt.setInt(6, obj.getId());
//            System.out.println(stmt);
			int ret = stmt.executeUpdate();
			System.out.println(ret);
			return obj;
		} catch (SQLException ex) {
			System.out.println(ex.getMessage());
			return null;
		}
	}

	@Override
	public int delete(UtilityCost obj) {
		String sql = "DELETE FROM UTILITY_COST WHERE UC_CODE=?";
		Connection conn = DatabaseHelper.getConnect();
		try {
			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setInt(1, obj.getId());
			int ret = stmt.executeUpdate();
			return ret;
		} catch (SQLException ex) {
			System.out.println(ex.getMessage());
		}
		return -1;
	}


}
