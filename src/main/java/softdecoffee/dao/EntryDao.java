/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package softdecoffee.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import softdecoffee.helper.DatabaseHelper;
import softdecoffee.model.Employee;
import softdecoffee.model.Entry;
import softdecoffee.model.Entry;

/**
 *
 * @author sarit
 */
public class EntryDao implements Dao<Entry> {

    @Override
    public Entry get(int id) {
        Entry entry = null;
        String sql = " SELECT * FROM ENTRY WHERE ENT_CODE=? ";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                entry = Entry.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return entry;
    }

    @Override
    public List<Entry> getAll() {
        ArrayList<Entry> list = new ArrayList();
        String sql = " SELECT * FROM ENTRY ";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Entry entry = Entry.fromRS(rs);
                list.add(entry);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<Entry> getAll(String order) {
        ArrayList<Entry> list = new ArrayList();
        String sql = " SELECT * FROM ENTRY  ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Entry entry = Entry.fromRS(rs);
                list.add(entry);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<Entry> getEntryByPayroll() {
        ArrayList<Entry> list = new ArrayList();
        String sql = " SELECT ENT_TOTAL_HR FROM ENTRY NATURAL JOIN PAYROLL WHERE PRL_STATUS = 'NOT PAY' ";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Entry entry = Entry.fromRS(rs);
                list.add(entry);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public Entry save(Entry obj) {
        String sql = " INSERT INTO ENTRY (EMP_CODE, ENT_STATUS)"
                + " VALUES(?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getEmployeeId());
            stmt.setString(2, obj.getStatus());
//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public Entry update(Entry obj) {
        String sql = " UPDATE ENTRY "
                + " SET EMP_CODE = ?, ENT_STATUS = ? "
                + " WHERE ENT_CODE = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getEmployeeId());
            stmt.setString(2, obj.getStatus());
            stmt.setInt(3, obj.getId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(Entry obj) {
        String sql = " DELETE FROM ENTRY WHERE ENT_CODE=? ";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    @Override
    public List<Entry> getAll(String where, String order) {
        ArrayList<Entry> list = new ArrayList();
        String sql = "SELECT * FROM ENTRY WHERE ENT_DATE =? ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, where);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                Entry entry = Entry.fromRS(rs);
                list.add(entry);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

}
