package softdecoffee.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import softdecoffee.helper.DatabaseHelper;
import softdecoffee.model.Employee;
import softdecoffee.model.Payroll;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
/**
 *
 * @author Nobpharat
 */
public class PayrollDao implements Dao<Payroll> {

    @Override
    public Payroll get(int id) {
        Payroll payroll = null;
        String sql = "SELECT * FROM PAYROLL WHERE PRL_CODE=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                payroll = Payroll.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return payroll;
    }

    public List<Payroll> getAll() {
        ArrayList<Payroll> list = new ArrayList();
        String sql = "SELECT * FROM PAYROLL";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Payroll payroll = Payroll.fromRS(rs);
                list.add(payroll);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<Payroll> getAll(String where, String order) {
        ArrayList<Payroll> list = new ArrayList();
        String sql = "SELECT * FROM PAYROLL where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Payroll payroll = Payroll.fromRS(rs);
                list.add(payroll);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<Payroll> getAll(String order) {
        ArrayList<Payroll> list = new ArrayList();
        String sql = "SELECT * FROM PAYROLL  ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Payroll payroll = Payroll.fromRS(rs);
                list.add(payroll);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<Payroll> getByStatus() {
        ArrayList<Payroll> list = new ArrayList();
        String sql = " SELECT * FROM (PAYROLL NATURAL JOIN ENTRY)NATURAL JOIN EMPLOYEE WHERE PRL_STATUS = 'NOT PAY' ";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Payroll payroll = Payroll.fromRS(rs);
                list.add(payroll);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    public Payroll updateOnlySTATUS(Payroll obj) {
        String sql = "UPDATE PAYROLL"
                + " SET PRL_STATUS = ?"
                + " WHERE PRL_CODE = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getStatus());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }


    /*public List<Payroll> totalWorkHourCal() throws ParseException {
        ArrayList<Payroll> list = new ArrayList();
        String sql = "SELECT * FROM EMP_CODE, ENT_TIME_IN, ENT_TIME_OUT FROM ENTRY";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            
            SimpleDateFormat dataFormat = new SimpleDateFormat("HH:mm:ss");
            long totalHourWork = 0;

            while (rs.next()) {
                int empId = rs.getInt("EMP_CODE");
                Date timeIn = dataFormat.parse(rs.getString("ENT_TIME_IN"));   
                Date timeOut = dataFormat.parse(rs.getString("ENT_TIME_OUT")); 
                
                long timeDifference = timeOut.getTime()-timeIn.getTime();
                totalHourWork += timeDifference;
            }
            
            double totalHours = totalHourWork / (60.0 * 60 * 1000);
            System.out.println("Total Work Houe : "+totalHours);
           

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
     */

    @Override
    public Payroll save(Payroll obj) {

        String sql = "INSERT INTO PAYROLL (EMP_CODE, PRL_STATUS, ENT_CODE, PRL_TOTAL_PAY, PLR_PRICE_HR)"
                + "VALUES(?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getEmployeeId());
            stmt.setString(2, obj.getStatus());
            stmt.setInt(3, obj.getEntryId());
            stmt.setFloat(4, obj.getTotalPay());
            stmt.setFloat(5, obj.getPriceHour());
//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public Payroll update(Payroll obj) {
        String sql = "UPDATE PAYROLL"
                + " SET EMP_CODE = ?, PRL_STATUS = ?,  ENT_CODE = ?, PRL_TOTAL_PAY = ?, PLR_PRICE_HR = ? "
                + " WHERE PRL_CODE = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getEmployeeId());
            stmt.setString(2, obj.getStatus());
            stmt.setInt(3, obj.getEntryId());
            stmt.setFloat(4, obj.getTotalPay());
            stmt.setFloat(5, obj.getPriceHour());
            stmt.setInt(6, obj.getId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(Payroll obj) {
        String sql = "DELETE FROM PAYROLL WHERE PRL_CODE=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

}
