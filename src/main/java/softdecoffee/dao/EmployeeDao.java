/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package softdecoffee.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import softdecoffee.helper.DatabaseHelper;
import softdecoffee.model.Employee;

/**
 *
 * @author werapan
 */
public class EmployeeDao implements Dao<Employee> {

    public Employee getUser(String username) {
        Employee employee = null;
        String sql = "SELECT * FROM EMPLOYEE WHERE EMP_USERNAME=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, username);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                employee = Employee.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return employee;
    }

    public static boolean getByLogin(String login, String password, String role) {
        Employee employee = null;
        String sql = "SELECT * FROM EMPLOYEE WHERE EMP_USERNAME=? AND EMP_PASS = ? AND EMP_RANK = ? ";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, login);
            stmt.setString(2, password);
            stmt.setString(3, role);
            ResultSet rs = stmt.executeQuery();

            return rs.next();  // If a result is found, authentication successful

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public Employee get(String login, String password) {
        Employee employee = null;
        String sql = "SELECT * FROM EMPLOYEE WHERE EMP_PASS = ? AND EMP_USERNAME = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, password);
            stmt.setString(2, login);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                employee = Employee.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return employee;
    }

    @Override
    public Employee get(int id) {
        Employee employee = null;
        String sql = "SELECT * FROM EMPLOYEE WHERE EMP_CODE=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                employee = Employee.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return employee;
    }

    public static Employee getByLogin(String username, String password) {
        Employee employee = null;
        String sql = "SELECT * FROM EMPLOYEE WHERE EMP_USERNAME = ? AND EMP_PASS = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, username);
            stmt.setString(2, password);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                employee = Employee.fromRS(rs);

            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return employee;
    }

    public static Employee getByUserName(String username) {
        Employee employee = null;
        String sql = "SELECT * FROM EMPLOYEE WHERE EMP_USERNAME = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, username);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                employee = Employee.fromRS(rs);

            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return employee;
    }

    public List<Employee> getAll() {
        ArrayList<Employee> list = new ArrayList();
        String sql = " SELECT * FROM EMPLOYEE ";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Employee employee = Employee.fromRS(rs);
                list.add(employee);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<Employee> getEmByPayroll() {
        ArrayList<Employee> list = new ArrayList();
        String sql = " SELECT * FROM EMPLOYEE NATURAL JOIN PAYROLL WHERE PRL_STATUS = 'NOT PAY' ";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Employee employee = Employee.fromRS(rs);
                list.add(employee);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<Employee> getAll(String where, String order) {
        ArrayList<Employee> list = new ArrayList();
        String sql = " SELECT * FROM EMPLOYEE where " + where + " ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Employee employee = Employee.fromRS(rs);
                list.add(employee);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<Employee> getAll(String order) {
        ArrayList<Employee> list = new ArrayList();
        String sql = " SELECT * FROM EMPLOYEE  ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Employee employee = Employee.fromRS(rs);
                list.add(employee);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public Employee save(Employee obj) {

        String sql = " INSERT INTO EMPLOYEE(EMP_FNAME, EMP_LNAME, EMP_EMAIL, EMP_TEL,EMP_ADDRESS,EMP_RANK,EMP_STATUS,EMP_PASS,EMP_USERNAME)"
                + "VALUES(?, ?, ?, ?, ?,?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getFname());
            stmt.setString(2, obj.getLname());
            stmt.setString(3, obj.getEmail());
            stmt.setString(4, obj.getTel());
            stmt.setString(5, obj.getAddress());
            stmt.setString(6, obj.getPosition());
            stmt.setString(7, obj.getStatus());
            stmt.setString(8, obj.getPassword());
            stmt.setString(9, obj.getUsername());
            stmt.setInt(10, obj.getId());
            //            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public Employee update(Employee obj) {
        String sql = " UPDATE EMPLOYEE"
                + " SET EMP_FNAME = ?, EMP_LNAME = ?, EMP_EMAIL = ?, EMP_TEL = ?, EMP_ADDRESS = ?, EMP_RANK = ?, EMP_STATUS = ?, EMP_PASS = ?, EMP_USERNAME = ?"
                + " WHERE EMP_CODE = ?";

        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getFname());
            stmt.setString(2, obj.getLname());
            stmt.setString(3, obj.getEmail());
            stmt.setString(4, obj.getTel());
            stmt.setString(5, obj.getAddress());
            stmt.setString(6, obj.getPosition());
            stmt.setString(7, obj.getStatus());
            stmt.setString(8, obj.getPassword());
            stmt.setString(9, obj.getUsername());
            stmt.setInt(10, obj.getId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(Employee obj) {
        String sql = "DELETE FROM EMPLOYEE WHERE EMP_CODE=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }
}
