/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package softdecoffee.ui;

import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.table.AbstractTableModel;
import softdecoffee.component.MaterialPanel;
import softdecoffee.model.Vendor;
import softdecoffee.service.VendorService;
import softdecoffee.ui.main.MainMemuOfManager;
import softdecoffee.ui.main.MainMemuOfStaff;

/**
 *
 * @author Puri
 */
public class VendorPanel extends javax.swing.JPanel {

    private List<Vendor> list;
    private final VendorService vendorService;
    private Vendor editedVendor;
    private String currentUser;
    private String rank;

    /**
     * Creates new form VendorPanel
     */
    public VendorPanel(String username,String rank) {
        initComponents();
	this.rank = rank;
        vendorService = new VendorService();
        list = vendorService.getVendors();

        tblVendor.setRowHeight(25);
        tblVendor.setModel(new AbstractTableModel() {
            String[] headers = {"Id", "Country", "Address", "Tel", "Name", "Contact", "Category", "", ""};

            @Override
            public String getColumnName(int column) {
                return headers[column];
            }

            @Override
            public int getRowCount() {
                return list.size();
            }

            @Override
            public int getColumnCount() {
                return 9;
            }

            @Override
            public Class<?> getColumnClass(int columnIndex) {
                switch (columnIndex) {
                    case 7:
                    case 8:
                        return ImageIcon.class;
                    default:
                        return String.class;
                }
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                Vendor vendor = list.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return vendor.getId();
                    case 1:
                        return vendor.getCountry();
                    case 2:
                        return vendor.getAddress();
                    case 3:
                        return vendor.getTel();
                    case 4:
                        return vendor.getName();
                    case 5:
                        return vendor.getContact();
                    case 6:
                        return vendor.getCategory();
                    case 7:
                        ImageIcon iconEdit = setImage("./image/icon/Edite.png");
                        return iconEdit;
                    case 8:
                        ImageIcon iconDelete = setImage("./image/icon/delete.png");
                        return iconDelete;
                    default:
                        return "Unknown";
                }
            }

            private ImageIcon setImage(String path) {
                ImageIcon icon = new ImageIcon(path);
                Image image = icon.getImage();
                int width = image.getWidth(null);
                int height = image.getHeight(null);
                Image newImage = image.getScaledInstance((int) (20 * ((float) width / height)), 20, Image.SCALE_SMOOTH);
                return new ImageIcon(newImage);
            }
        });
        tblVendor.getColumnModel().getColumn(7).setPreferredWidth(25);
        tblVendor.getColumnModel().getColumn(8).setPreferredWidth(25);

        
        tblVendor.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                int row = tblVendor.rowAtPoint(e.getPoint());
                int col = tblVendor.columnAtPoint(e.getPoint());
                if (col == 7) {
                    int selectedIndex = tblVendor.getSelectedRow();
                    if (selectedIndex >= 0) {
                        editedVendor = list.get(selectedIndex);
                        openDialog();
                    }
                } else if (col == 8) {
                    int selectedIndex = tblVendor.getSelectedRow();
                    if (selectedIndex >= 0) {
                        editedVendor = list.get(selectedIndex);
                        int input = JOptionPane.showConfirmDialog(tblVendor, "Do you want to proceed?", "Select an Option...",
                                JOptionPane.YES_NO_OPTION, JOptionPane.ERROR_MESSAGE);
                        if (input == 0) {
                            vendorService.delete(editedVendor);
                        }
                        refreshTable();
                    }
                }
            }
        });

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel3 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblVendor = new javax.swing.JTable();
        jPanel2 = new javax.swing.JPanel();
        btnAdd = new javax.swing.JButton();
        btnBack = new javax.swing.JButton();

        jPanel3.setBackground(new java.awt.Color(221, 206, 183));

        tblVendor.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tblVendor);

        jPanel2.setBackground(new java.awt.Color(171, 146, 131));
        jPanel2.setPreferredSize(new java.awt.Dimension(162, 44));

        btnAdd.setBackground(new java.awt.Color(242, 199, 99));
        btnAdd.setFont(new java.awt.Font("Poppins Medium", 0, 12)); // NOI18N
        btnAdd.setText("Add");
        btnAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddActionPerformed(evt);
            }
        });

        btnBack.setBackground(new java.awt.Color(242, 199, 99));
        btnBack.setFont(new java.awt.Font("Poppins Medium", 0, 12)); // NOI18N
        btnBack.setText("Back");
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnBack)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnAdd)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnBack)
                    .addComponent(btnAdd, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(9, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 918, Short.MAX_VALUE)
                .addContainerGap())
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, 930, Short.MAX_VALUE)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 544, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
        openStockBillPanel();
    }//GEN-LAST:event_btnBackActionPerformed

    private void openStockBillPanel() {
        if (rank == "Manager") {
		  MainMemuOfManager mainMemuOfManager;
		 mainMemuOfManager = (MainMemuOfManager) SwingUtilities.getWindowAncestor(this);
		 mainMemuOfManager.setScrPanel(new StockBillPanel(currentUser,rank), "Stock Bill");
		 mainMemuOfManager.revalidate();
	    } else if (rank == "Staff") {
		 MainMemuOfStaff mainMemuOfStaff;
		 mainMemuOfStaff = (MainMemuOfStaff) SwingUtilities.getWindowAncestor(this);
		 mainMemuOfStaff.setScrPanel(new StockBillPanel(currentUser,rank), "Stock Bill");
		 mainMemuOfStaff.revalidate();
	    }
    }

    private void btnAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddActionPerformed
        editedVendor = new Vendor();
        openDialog();
    }

    private void openDialog() {
        JFrame frame = (JFrame) SwingUtilities.getRoot(this);
        VendorDialog vendorDialog = new VendorDialog(frame, editedVendor);
        vendorDialog.setLocationRelativeTo(this);
        vendorDialog.setVisible(true);
        vendorDialog.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                refreshTable();
            }
        });

    }//GEN-LAST:event_btnAddActionPerformed

    private void refreshTable() {
        list = vendorService.getVendors();
        tblVendor.revalidate();
        tblVendor.repaint();
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAdd;
    private javax.swing.JButton btnBack;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tblVendor;
    // End of variables declaration//GEN-END:variables
}
