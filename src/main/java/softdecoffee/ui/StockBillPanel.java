/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package softdecoffee.ui;

//import stockbill.ui.main.MainMemuOfManager;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import static java.util.Collections.list;
import javax.swing.table.AbstractTableModel;
import softdecoffee.service.StockBillService;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import softdecoffee.component.MaterialPanel;
import softdecoffee.dao.EmployeeDao;
import softdecoffee.dao.StockBillDao;
import softdecoffee.dao.StockBillDetailDao;
import softdecoffee.dao.VendorDao;
import softdecoffee.model.Employee;
import softdecoffee.model.StockBill;
import softdecoffee.model.StockBill;
import softdecoffee.model.StockBillDetail;
import softdecoffee.model.Vendor;
import softdecoffee.service.StockBillDetailService;
import softdecoffee.service.VendorService;
import softdecoffee.ui.main.MainMemuOfManager;
import softdecoffee.ui.main.MainMemuOfStaff;

/**
 *
 * @author Puri
 */
public class StockBillPanel extends javax.swing.JPanel {

	private List<StockBill> list;
	private List<StockBillDetail> listDetail;
	private final StockBillService stockBillService = new StockBillService();
	private StockBill editedStockBill;
	private List<StockBill> vd;
	private StockBillDetail editedStockBillDetail;
	private List<StockBillDetail> bd;
	private String currentUser;
	private StockBillDetailService stockBillDetailService = new StockBillDetailService();
	private ArrayList<StockBillDetail> stockBillDetails;
	List<StockBill> stockBills = new ArrayList<>();
	private StockBill stockBill;
	private StockBill deletedStockBill;
	private StockBillDao stockBillDao = new StockBillDao();
	private StockBillDetailDao stockBillDetailDao = new StockBillDetailDao();
	private String rank;
	/**
	 * Creates new form StockBillPanel
	 */
	public StockBillPanel(String currentUser,String rank) {
		initComponents();
		this.currentUser = currentUser;
		this.rank =rank;
		list = stockBillService.getStockBills();
		listDetail = stockBillDetailService.getBillStocks();

		tblStockBill.setRowHeight(25);
		updateStockBillTable(list);
		tblStockBillDetail.setModel(new AbstractTableModel() {
			String[] columnName = {"ID", "StockBill Id", "Amount", "Price", "Total Price", "Material ID"};

			@Override
			public String getColumnName(int column) {
				return columnName[column];
			}

			@Override
			public int getRowCount() {
				return listDetail.size();
			}

			@Override
			public int getColumnCount() {
				return 6;
			}

			@Override
			public Class<?> getColumnClass(int columnIndex) {
				switch (columnIndex) {
					default:
						return String.class;
				}
			}

			@Override
			public Object getValueAt(int rowIndex, int columnIndex) {
				StockBillDetail stockBillDetail = listDetail.get(rowIndex);
				switch (columnIndex) {
					case 0:
						return stockBillDetail.getId();
					case 1:
						return stockBillDetail.getBiiId();
					case 2:
						return stockBillDetail.getAmount();
					case 3:
						return stockBillDetail.getPrice();
					case 4:
						return stockBillDetail.getTotalPrrice();
					case 5:
						return stockBillDetail.getMaterrialId();
					default:
						return "Unknown";
				}
			}
		});
		tblStockBillDetail.addMouseListener(new MouseAdapter() {

			@Override
			public void mouseClicked(MouseEvent e) {

				int row = tblStockBillDetail.rowAtPoint(e.getPoint());
				System.out.println("row = " + row);
				System.out.println("tblStockBilldetail listDetail = " + listDetail);
				System.out.println("mouseClicked listDetail size = " + listDetail.size());
				System.out.println("listDetail = " + listDetail.get(row));
				StockBillDetail editStockBillDetail = listDetail.get(row);
				System.out.println("editStockBillDetail" + editStockBillDetail);
				int id = editStockBillDetail.getId();
				System.out.println("editstockbillDetail:" + id);
				System.out.println("mouseClicked StockBillDetail end part 1");

				tblStockBillDetail.revalidate();
				tblStockBillDetail.repaint();
			}

		});
		refreshTable();

	}

	private void updateStockBillDetailTable(List<StockBillDetail> listDetail) {
		tblStockBillDetail.setModel(new AbstractTableModel() {
			String[] columnName = {"ID", "StockBill Id", "Amount", "Price", "Total Price", "Material ID"};

			@Override
			public String getColumnName(int column) {
				return columnName[column];
			}

			@Override
			public int getRowCount() {
				return listDetail.size();
			}

			@Override
			public int getColumnCount() {
				return 6;
			}

			@Override
			public Class<?> getColumnClass(int columnIndex) {
				switch (columnIndex) {
					default:
						return String.class;
				}
			}

			@Override
			public Object getValueAt(int rowIndex, int columnIndex) {
				StockBillDetail stockBillDetail = listDetail.get(rowIndex);
				switch (columnIndex) {
					case 0:
						return stockBillDetail.getId();
					case 1:
						return stockBillDetail.getBiiId();
					case 2:
						return stockBillDetail.getAmount();
					case 3:
						return stockBillDetail.getPrice();
					case 4:
						return stockBillDetail.getTotalPrrice();
					case 5:
						return stockBillDetail.getMaterrialId();
					default:
						return "Unknown";
				}
			}

		});

	}

	private void updateStockBillTable(List<StockBill> newData) {
                tblStockBillDetail.setRowHeight(25);
		tblStockBill.setModel(new AbstractTableModel() {
			String[] columnName = {"ID", "Total Price", "Total QTY", "Date", "Bill Pay", "Employee ID", "Vendor ID"};

			@Override
			public String getColumnName(int column) {
				return columnName[column];
			}

			@Override
			public int getRowCount() {
				return newData.size();
			}

			@Override
			public int getColumnCount() {
				return 7;
			}

			@Override
			public Class<?> getColumnClass(int columnIndex) {
				switch (columnIndex) {
					default:
						return String.class;
				}
			}

			@Override
			public Object getValueAt(int rowIndex, int columnIndex) {
				StockBill stockBill = newData.get(rowIndex);
				switch (columnIndex) {
					case 0:
						return stockBill.getId();
					case 1:
						return stockBill.getTotalPrice();
					case 2:
						return stockBill.getTotalQty();
					case 3:
						return stockBill.getDateTime();
					case 4:
						return stockBill.getBillPay();
					case 5:
						return stockBill.getEmployeeId();
					case 6:
						return stockBill.getVendorId();
					default:
						return "Unknown";
				}
			}

		});
		tblStockBill.addMouseListener(new MouseAdapter() {

			@Override
			public void mouseClicked(MouseEvent e) {
				int row = tblStockBill.rowAtPoint(e.getPoint());

				System.out.println("list = " + list.get(row));
				StockBill editStockBill = list.get(row);
				System.out.println("editStockBill" + editStockBill);
				int id = editStockBill.getId();
				System.out.println("editstockbill:" + id);
				System.out.println("mouseClicked StockBill end part 1");

				StockBillDetailDao stockBillDetailDao = new StockBillDetailDao();
				stockBillDetails = (ArrayList<StockBillDetail>) stockBillDetailDao.getAllByStockBillId(id);

				StockBillDao stockBillDao = new StockBillDao();
				stockBill = stockBillDao.get(id);
				stockBill.setStockBillDetails(stockBillDetails);

				listDetail = stockBill.getStockBillDetails();
				System.out.println("tblStockBill listDetail = " + listDetail);
				System.out.println("before update listDetail size = " + listDetail.size());
				//updateStockBillDetailTable(listDetail);  
				//System.out.println("after update listDetail size = "+listDetail.size());
				tblStockBillDetail.revalidate();
				tblStockBillDetail.repaint();
			}

		});

	}

	/**
	 * This method is called from within the constructor to initialize the
	 * form. WARNING: Do NOT modify this code. The content of this method is
	 * always regenerated by the Form Editor.
	 */
	@SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        cbVendor = new javax.swing.JComboBox<>();
        btnVendor = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        btnBack = new javax.swing.JButton();
        btnAdd = new javax.swing.JButton();
        btnEdit = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblStockBill = new javax.swing.JTable();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblStockBillDetail = new javax.swing.JTable();

        setBackground(new java.awt.Color(221, 206, 183));

        jPanel2.setBackground(new java.awt.Color(221, 206, 183));
        jPanel2.setPreferredSize(new java.awt.Dimension(261, 42));

        cbVendor.setBackground(new java.awt.Color(242, 199, 99));
        cbVendor.setFont(new java.awt.Font("Poppins Medium", 0, 12)); // NOI18N
        cbVendor.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Select Vendor", "Central", "BigC", "Makro", "Lotus" }));
        cbVendor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbVendorActionPerformed(evt);
            }
        });

        btnVendor.setBackground(new java.awt.Color(242, 199, 99));
        btnVendor.setFont(new java.awt.Font("Poppins Medium", 0, 14)); // NOI18N
        btnVendor.setText("Vendor");
        btnVendor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVendorActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(cbVendor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnVendor)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(13, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cbVendor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnVendor))
                .addContainerGap())
        );

        jPanel3.setBackground(new java.awt.Color(171, 146, 131));
        jPanel3.setPreferredSize(new java.awt.Dimension(261, 42));

        btnBack.setBackground(new java.awt.Color(242, 199, 99));
        btnBack.setFont(new java.awt.Font("Poppins Medium", 0, 14)); // NOI18N
        btnBack.setText("Material");
        btnBack.setAlignmentX(0.5F);
        btnBack.setMaximumSize(new java.awt.Dimension(72, 29));
        btnBack.setMinimumSize(new java.awt.Dimension(72, 29));
        btnBack.setPreferredSize(new java.awt.Dimension(72, 29));
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });

        btnAdd.setBackground(new java.awt.Color(242, 199, 99));
        btnAdd.setFont(new java.awt.Font("Poppins Medium", 0, 14)); // NOI18N
        btnAdd.setText("Add");
        btnAdd.setMaximumSize(new java.awt.Dimension(72, 29));
        btnAdd.setMinimumSize(new java.awt.Dimension(72, 29));
        btnAdd.setPreferredSize(new java.awt.Dimension(72, 29));
        btnAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddActionPerformed(evt);
            }
        });

        btnEdit.setBackground(new java.awt.Color(242, 199, 99));
        btnEdit.setFont(new java.awt.Font("Poppins Medium", 0, 14)); // NOI18N
        btnEdit.setText("Edit");
        btnEdit.setMaximumSize(new java.awt.Dimension(72, 29));
        btnEdit.setMinimumSize(new java.awt.Dimension(72, 29));
        btnEdit.setPreferredSize(new java.awt.Dimension(72, 29));
        btnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditActionPerformed(evt);
            }
        });

        btnDelete.setBackground(new java.awt.Color(242, 199, 99));
        btnDelete.setFont(new java.awt.Font("Poppins Medium", 0, 14)); // NOI18N
        btnDelete.setText("Delete");
        btnDelete.setAlignmentX(0.5F);
        btnDelete.setMaximumSize(new java.awt.Dimension(72, 29));
        btnDelete.setMinimumSize(new java.awt.Dimension(72, 29));
        btnDelete.setPreferredSize(new java.awt.Dimension(72, 29));
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnBack, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnAdd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnEdit, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(12, 12, 12)
                .addComponent(btnDelete, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnBack, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnAdd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnEdit, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnDelete, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(7, Short.MAX_VALUE))
        );

        tblStockBill.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tblStockBill);

        tblStockBillDetail.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane2.setViewportView(tblStockBillDetail);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, 930, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 324, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 588, Short.MAX_VALUE)
                .addContainerGap())
            .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 930, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 498, Short.MAX_VALUE)
                    .addComponent(jScrollPane2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddActionPerformed
	    editedStockBill = new StockBill();
	    editedStockBillDetail = new StockBillDetail();
	    openAddDialog();
    }//GEN-LAST:event_btnAddActionPerformed

	private void openAddDialog() {
		JFrame frame = (JFrame) SwingUtilities.getRoot(this);
		StockBillAddDialog stockBillAddDialog = new StockBillAddDialog(frame, editedStockBill);
		stockBillAddDialog.setLocationRelativeTo(this);
		stockBillAddDialog.setVisible(true);
		stockBillAddDialog.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosed(WindowEvent e) {
				list = stockBillService.getStockBills();
				listDetail = stockBillDetailService.getBillStocks();
				updateStockBillTable(list);
				//updateStockBillDetailTable(listDetail);

				refreshTable();
			}
		});

	}

	private void refreshTableToOrigin() {
		list = stockBillService.getStockBills();
		listDetail = stockBillDetailService.getBillStocks();
		tblStockBill.revalidate();
		tblStockBill.repaint();
		tblStockBillDetail.revalidate();
		tblStockBillDetail.repaint();
	}

	private void refreshTable() {
		tblStockBill.revalidate();
		tblStockBill.repaint();
		tblStockBillDetail.revalidate();
		tblStockBillDetail.repaint();
	}

	private void refreshTableWhenDelete() {
		list = stockBillService.getStockBills();
		listDetail = stockBillDetailService.getBillStocks();
		updateStockBillTable(list);
		refreshTable();
	}

    private void btnVendorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVendorActionPerformed
	    openVenPanel();
    }//GEN-LAST:event_btnVendorActionPerformed

	private void openVenPanel() {
		if (rank == "Manager") {
		  MainMemuOfManager mainMemuOfManager;
		 mainMemuOfManager = (MainMemuOfManager) SwingUtilities.getWindowAncestor(this);
		 mainMemuOfManager.setScrPanel(new VendorPanel(currentUser,rank), "Vendor");
		 mainMemuOfManager.revalidate();
	    } else if (rank == "Staff") {
		 MainMemuOfStaff mainMemuOfStaff;
		 mainMemuOfStaff = (MainMemuOfStaff) SwingUtilities.getWindowAncestor(this);
		 mainMemuOfStaff.setScrPanel(new VendorPanel(currentUser,rank), "Vendor");
		 mainMemuOfStaff.revalidate();
	    }

	}

    private void cbVendorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbVendorActionPerformed
	    String selectedVendor = (String) cbVendor.getSelectedItem();

	    if (selectedVendor != null) {
		    switch (selectedVendor) {
			    case "Select Vendor":
				    vd = stockBillService.getAll();
				    break;
			    case "Central":
				    vd = stockBillService.getByVendor(1);
				    break;
			    case "BigC":
				    vd = stockBillService.getByVendor(2);
				    break;
			    case "Makro":
				    vd = stockBillService.getByVendor(3);
				    break;
			    case "Lotus":
				    vd = stockBillService.getByVendor(4);
				    break;
		    }

		    updateStockBillTable(vd);
	    }

    }//GEN-LAST:event_cbVendorActionPerformed

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
	    if (rank == "Manager") {
		    MainMemuOfManager mainMemuOfManager;
		    mainMemuOfManager = (MainMemuOfManager) SwingUtilities.getWindowAncestor(this);
		    mainMemuOfManager.setScrPanel(new MaterialPanel(currentUser, rank), "Stock Management");
		    mainMemuOfManager.revalidate();
	    } else if (rank == "Staff") {
		    MainMemuOfStaff mainMemuOfStaff;
		    mainMemuOfStaff = (MainMemuOfStaff) SwingUtilities.getWindowAncestor(this);
		    mainMemuOfStaff.setScrPanel(new MaterialPanel(currentUser, rank), "Stock Management");
		    mainMemuOfStaff.revalidate();
	    }
    }//GEN-LAST:event_btnBackActionPerformed

    private void btnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditActionPerformed
	    int selectedIndex = tblStockBill.getSelectedRow();
	    if (selectedIndex >= 0 && selectedIndex < list.size()) {
		    editedStockBill = list.get(selectedIndex);

		    if (listDetail != null && listDetail.size() > selectedIndex) {
			    editedStockBillDetail = listDetail.get(selectedIndex);
		    } else {
			    // Handle the case where listDetail is null or not enough elements
			    if (listDetail == null) {
				    listDetail = new ArrayList<>(); // Initialize listDetail if it's null
			    }

			    // Ensure listDetail has enough elements up to selectedIndex
			    while (listDetail.size() <= selectedIndex) {
				    listDetail.add(new StockBillDetail()); // Add placeholders until the list is long enough
			    }

			    editedStockBillDetail = listDetail.get(selectedIndex);
		    }

		    openAddDialog();
	    }

    }//GEN-LAST:event_btnEditActionPerformed

    private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
	    int selectedIndex = tblStockBill.getSelectedRow();
	    if (selectedIndex >= 0) {
		    editedStockBill = list.get(selectedIndex);
		    int input = JOptionPane.showConfirmDialog(this, "Do you want to proceed?", "Select an Option...",
			    JOptionPane.YES_NO_OPTION, JOptionPane.ERROR_MESSAGE);
		    if (input == 0) {
			    stockBillDetailDao.deleteAllFromStockBillID(editedStockBill);
			    stockBillService.delete(editedStockBill);
		    }
		    refreshTableWhenDelete();
	    }


    }//GEN-LAST:event_btnDeleteActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAdd;
    private javax.swing.JButton btnBack;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnEdit;
    private javax.swing.JButton btnVendor;
    private javax.swing.JComboBox<String> cbVendor;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable tblStockBill;
    private javax.swing.JTable tblStockBillDetail;
    // End of variables declaration//GEN-END:variables
}
