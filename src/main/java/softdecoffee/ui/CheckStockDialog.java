/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JDialog.java to edit this template
 */
package softdecoffee.ui;

import java.awt.Font;
import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;
import softdecoffee.dao.CheckStockDao;
import softdecoffee.dao.CheckStockDetailDao;
import softdecoffee.dao.EmployeeDao;
import softdecoffee.dao.MaterialDao;
import softdecoffee.model.CheckStock;
import softdecoffee.model.Material;
import softdecoffee.model.CheckStockDetail;
import softdecoffee.model.Employee;
import softdecoffee.service.CheckStockService;
import softdecoffee.service.MaterialService;

/**
 *
 * @author Lenovo
 */
// ต้อง เพิ่ม current User และ setting ใน txfCheckStockId ด้วย เพื่อใช้ในการ assign ค่าให้กับ parameter ของ checkStockDetail
// และตั้งค่าparameter ใน method addCheckStockDetail บรรทัดที่ 213
public class CheckStockDialog extends javax.swing.JDialog {
    //material-->check stock-->check stock detail

    ArrayList<Material> materials;
    MaterialService materialService = new MaterialService();
    CheckStockService checkStockService = new CheckStockService();
    CheckStock checkStock;
    private static CheckStockDialog instance;
    private CheckStock editedCheckStock;
    ArrayList<CheckStockDetail> checkStockDetails = new ArrayList<CheckStockDetail>();
    ArrayList<Material> editedMaterials = new ArrayList<Material>();
    CheckStockDetail checkStockDetail;
    CheckStockDao checkStockDao = new CheckStockDao();
    private int checkStockId = 1;
    private String currentUser;
    private Employee EmpUser = new Employee();
    private String rank;

    /**
     * Creates new form PosPanel
     */
    public CheckStockDialog(java.awt.Frame parent, CheckStock editedCheckStock,String username,String rank) {
        super(parent, true);
        initComponents();
        currentUser = username;
	this.rank = rank;
        this.editedCheckStock = editedCheckStock;
        
        EmpUser = EmployeeDao.getByUserName(currentUser);        

        initMaterialTable();
        checkStock = new CheckStock();
        //ทำให้ id มีค่าเป็นค่าล่าสุด
        while (checkStock.getId() == -1 || checkStock.getId() <= checkStockDao.getLast().getId()) {
            checkStockId++;
            checkStock.setId(checkStockId);
        }

        lblCheckStockId.setText("" + checkStock.getId());
        //checkStock.setUser(UserService.getCurrentUser());
        
    }

    private void initMaterialTable() {
        materials = materialService.getMaterialsOrderById();        
        tblMaterial.getTableHeader().setFont(new Font("TH Saraban New", Font.PLAIN, 16));
        tblMaterial.setRowHeight(100);
        tblMaterial.setModel(new AbstractTableModel() {
            String[] headers = {"Image", "ID", "Name", "Qoh"};

            @Override
            public Class<?> getColumnClass(int columnIndex) {
                switch (columnIndex) {
                    case 0:
                        return ImageIcon.class;
                    default:
                        return String.class;
                }
            }

            @Override
            public String getColumnName(int column) {
                return headers[column];
            }

            @Override
            public int getRowCount() {
                return materials.size();
            }

            @Override
            public int getColumnCount() {
                return 4;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                Material material = materials.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        ImageIcon icon = new ImageIcon("./Material" + material.getId() + ".png");
                        Image image = icon.getImage();
                        int width = image.getWidth(null);
                        int height = image.getHeight(null);
                        Image newImage = image.getScaledInstance((int) ((float) (100 * width) / height), 100, Image.SCALE_SMOOTH);
                        icon.setImage(newImage);
                        return icon;
                    case 1:
                        return material.getId();
                    case 2:
                        return material.getName();
                    case 3:
                        return material.getQoh();
                    default:
                        return "";
                }

            }

            @Override
            public void setValueAt(Object avalue, int rowIndex, int columnIndex) {
                Material material = materials.get(rowIndex);
                if (columnIndex == 3) { // ตรวจสอบว่ามีการแก้ไขคอลัมน์ Remaining
                    int newRemaining = Integer.parseInt((String) avalue);
                    if(newRemaining<1) return;
                    material.setQoh(newRemaining);
                    editedMaterials.add(material);    
                }
            }

            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                switch (columnIndex) {
                    case 3:
                        return true;
                    default:
                        return false;
                }
            }
            
            
        });
            
    }

    private void refreshMaterial() {
        materials = materialService.getMaterialsOrderById();
        tblMaterial.revalidate();
        tblMaterial.repaint();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        lblCheckStockId = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        scrMaterial = new javax.swing.JScrollPane();
        tblMaterial = new javax.swing.JTable();
        jPanel5 = new javax.swing.JPanel();
        btnCancel = new javax.swing.JButton();
        btnClear = new javax.swing.JButton();
        btnSave = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(221, 206, 183));

        jPanel2.setBackground(new java.awt.Color(171, 146, 131));

        jLabel1.setFont(new java.awt.Font("Poppins Medium", 0, 14)); // NOI18N
        jLabel1.setText("Check stock ID :");

        lblCheckStockId.setBackground(new java.awt.Color(204, 255, 255));
        lblCheckStockId.setFont(new java.awt.Font("Poppins Medium", 0, 14)); // NOI18N

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblCheckStockId, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblCheckStockId, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(22, Short.MAX_VALUE))
        );

        tblMaterial.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        scrMaterial.setViewportView(tblMaterial);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(scrMaterial, javax.swing.GroupLayout.DEFAULT_SIZE, 766, Short.MAX_VALUE)
                .addGap(0, 0, 0))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(scrMaterial, javax.swing.GroupLayout.DEFAULT_SIZE, 327, Short.MAX_VALUE)
        );

        jPanel5.setBackground(new java.awt.Color(171, 146, 131));

        btnCancel.setBackground(new java.awt.Color(242, 199, 99));
        btnCancel.setFont(new java.awt.Font("Poppins Medium", 0, 14)); // NOI18N
        btnCancel.setText("Cancel");
        btnCancel.setMaximumSize(new java.awt.Dimension(72, 29));
        btnCancel.setMinimumSize(new java.awt.Dimension(72, 29));
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });

        btnClear.setBackground(new java.awt.Color(242, 199, 99));
        btnClear.setFont(new java.awt.Font("Poppins Medium", 0, 14)); // NOI18N
        btnClear.setText("Clear");
        btnClear.setMaximumSize(new java.awt.Dimension(72, 29));
        btnClear.setMinimumSize(new java.awt.Dimension(72, 29));
        btnClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClearActionPerformed(evt);
            }
        });

        btnSave.setBackground(new java.awt.Color(242, 199, 99));
        btnSave.setFont(new java.awt.Font("Poppins Medium", 0, 14)); // NOI18N
        btnSave.setText("Save");
        btnSave.setMaximumSize(new java.awt.Dimension(72, 29));
        btnSave.setMinimumSize(new java.awt.Dimension(72, 29));
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnClear, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnCancel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnCancel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnClear, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

        private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
            CheckStockPanel checkStockPanel = new CheckStockPanel(currentUser,rank);
            CheckStockDetailDao checkStockDetailDao = new CheckStockDetailDao();  
            //อัพเดท ข้อมุลของ material ที่แก้ไข editedMaterials
            materialService.updateArray(editedMaterials);
            
            //เพิ่ม material มาเป็น checkStockDetail
            //ตัวใหม่1
            for (Material material : materials) {


                // สร้างอ็อบเจ็ก CheckStockDetail ใหม่
                checkStockDetail = new CheckStockDetail(material.getId(), checkStock.getId(), material.getQoh(), 0, 0);

                // เพิ่ม CheckStockDetail เข้าไปใน checkStockDetails
                checkStockDetails.add(checkStockDetail);
            } 
      
            // เพิ่ม CheckStockDetail เข้ากับ CheckStock
            checkStock.setCheckStockDetails(checkStockDetails);
            //
            //save checkStockDetail ลง db
            ArrayList<CheckStockDetail> checkStockDetailslocal = checkStock.getCheckStockDetails();
            for (CheckStockDetail checkStockDetaillocal : checkStockDetailslocal) {
                checkStockDetailDao.save(checkStockDetaillocal);
            }
            //set employee id ให้กับ checkStock
            checkStock.setEmployeeId(EmpUser.getId());
            //save checkStock ลง db
            checkStockDao.save(checkStock);
            //รีเฟรชหน้าตารางแสดง checkStock
            checkStockPanel.refreshCheckStock();
            this.dispose();
        }//GEN-LAST:event_btnSaveActionPerformed

    private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnCancelActionPerformed

    private void btnClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClearActionPerformed
        refreshMaterial();
    }//GEN-LAST:event_btnClearActionPerformed

    /**
     * @param args the command line arguments
     */

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnClear;
    private javax.swing.JButton btnSave;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JLabel lblCheckStockId;
    private javax.swing.JScrollPane scrMaterial;
    private javax.swing.JTable tblMaterial;
    // End of variables declaration//GEN-END:variables
}
