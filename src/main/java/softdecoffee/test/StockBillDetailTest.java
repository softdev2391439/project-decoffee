/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package softdecoffee.test;

import java.util.List;
import softdecoffee.dao.MaterialDao;
import softdecoffee.dao.StockBillDao;
import softdecoffee.model.Material;
import softdecoffee.model.StockBill;
import softdecoffee.model.StockBillDetail;
import softdecoffee.service.StockBillDetailService;

/**
 *
 * @author Nobpharat
 */
public class StockBillDetailTest {

	public static void main(String[] args) {
//		StockBillDetailService stockBillDetailService = new StockBillDetailService();
//		for(StockBillDetail c : stockBillDetailService.getBillStocks()){
//			System.out.println(c);
//		}
////		StockBillDetail stockBillDetail1 = new StockBillDetail(1, 100, 1000, 10000, 2);
////		stockBillDetailService.addNew(stockBillDetail1);
//		System.out.println("After Delete Test");
//		stockBillDetailService.delete(stockBillDetailService.getById(56));
//		for(StockBillDetail c : stockBillDetailService.getBillStocks()){
//			System.out.println(c);
//		}
////		System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
////		for(StockBillDetail c : stockBillDetailService.getBillStocks()){
////			System.out.println(c);
////		}
//		for(StockBillDetail c : stockBillDetailService.getByBillID(1)){
//			System.out.println(c);
//		}
                MaterialDao md = new MaterialDao();
                StockBillDao sb = new StockBillDao();
                List<StockBill> stockBills = sb.getAll();
                StockBill stockbill0 = stockBills.get(0);
                Material material = md.get(4);
                StockBillDetail newStockBillDetail = new StockBillDetail(58,stockbill0.getId(), 2,
                    2.5,5,material.getId());
                System.out.println(newStockBillDetail);
	}
	
}
