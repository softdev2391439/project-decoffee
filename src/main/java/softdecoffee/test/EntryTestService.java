/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package softdecoffee.test;

import softdecoffee.model.Entry;
import softdecoffee.service.EntryService;

/**
 *
 * @author sarit
 */
public class EntryTestService {

    public static void main(String[] args) {
        EntryService cs = new EntryService();

// Print all entries
        System.out.println("All Entries:");
        for (Entry entry : cs.getEntrys()) {
            System.out.println(entry);
        }

// Create a new entry for testing
        Entry testEntry = new Entry(-1, 9, null, null, null, "Present", 6);

// Add the test entry
        cs.addNew(testEntry);

// Print all entries again, including the new test entry
        System.out.println("All Entries After Adding Test Entry:");
        for (Entry entry : cs.getEntrys()) {
            System.out.println(entry);
        }
//        Entry delCus = cs.getByTel("0911451569");
//        delCus.setTel("0868362175");
//        cs.update(delCus);
//        System.out.println("After Update");
//        for(Entry customer : cs.getEntrys()){
//            System.out.println(customer );
//        }
//        cs.delete(delCus);
//        for(Entry customer : cs.getEntrys()){
//            System.out.println(customer );
//        }
    }
}
