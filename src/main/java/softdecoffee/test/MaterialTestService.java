/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package softdecoffee.test;

import softdecoffee.model.Material;
import softdecoffee.service.MaterialService;



/**
 *
 * @author Nobpharat
 */
public class MaterialTestService {
    public static void main(String[] args) {
        MaterialService ms = new MaterialService();
        for(Material material : ms.getMaterials()){
            System.out.println(material );
        }
        System.out.println(ms.getById(1));
//        Material newmaMaterial1 = new Material("Coco", "Kilogram", 10, 20, 20);
//        Material newmaMaterial2 = new Material("Milk", "Liter", 20, 20, 20);
//        Material newmaMaterial3 = new Material("Sugar", "Kilogram", 30, 20, 20);
//        Material newmaMaterial4 = new Material("Coffee Seeds", "Kilogram", 40, 20, 20);
//        ms.addNew(newmaMaterial1);
//        ms.addNew(newmaMaterial2);
//        ms.addNew(newmaMaterial3);
//        ms.addNew(newmaMaterial4);
//        for(Material material : ms.getMaterials()){
//            System.out.println(material );
//        }
        
        Material upMaterial = ms.getById(2);
        upMaterial.setMinimum(100.50);
        ms.update(upMaterial);
        System.out.println(ms.getById(2));
        ms.delete(ms.getById(4));
        for(Material material : ms.getMaterials()){
            System.out.println(material );
        }
        
    }
}
