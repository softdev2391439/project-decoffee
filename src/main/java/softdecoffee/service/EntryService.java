/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package softdecoffee.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import softdecoffee.dao.EntryDao;
import softdecoffee.model.Entry;

/**
 *
 * @author sarit
 */
public class EntryService {

    private EntryDao entryDao = new EntryDao();

    public Entry getById(int id) {
        EntryDao entryDao = new EntryDao();
        return entryDao.get(id);
    }

    public List<Entry> getEntrys() {
        EntryDao entryDao = new EntryDao();
        return entryDao.getAll(" ENT_CODE asc");
    }

    public List<Entry> getEntryByPayroll() {
        EntryDao entryDao = new EntryDao();
        return entryDao.getEntryByPayroll();
    }

    public static Entry addNew(Entry editedEntry) {
        EntryDao entryDao = new EntryDao();
        return entryDao.save(editedEntry);
    }

    public Entry update(Entry editedEntry) {
        EntryDao entryDao = new EntryDao();
        return entryDao.update(editedEntry);
    }

    public int delete(Entry editedEntry) {
        EntryDao entryDao = new EntryDao();
        return entryDao.delete(editedEntry);
    }

    public List<Entry> getDate() {
        EntryDao entryDao = new EntryDao();
        String today = getCurrentDate();
        return entryDao.getAll(today, " ENT_CODE asc");
    }

    private String getCurrentDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        return dateFormat.format(new Date());
    }

    public Entry addNew1(Entry editedEntry) {
        EntryDao entryDao = new EntryDao();
        return entryDao.save(editedEntry);
    }

    public ArrayList<Entry> getCheckTimeForToday() {
        String today = getCurrentDate();
        return (ArrayList<Entry>) entryDao.getAll(today, " ENT_CODE asc");
    }
}
