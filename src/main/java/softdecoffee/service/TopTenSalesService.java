/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package softdecoffee.service;

import java.util.List;
import softdecoffee.dao.TopTenSalesDao;
import softdecoffee.model.TopTenSalesReport;

/**
 *
 * @author natta
 */
public class TopTenSalesService {
    public List<TopTenSalesReport> getTopTenArtistByTotalPrice(){
        TopTenSalesDao artistDao = new TopTenSalesDao();
        return artistDao.getArtistByTotalPrice(10);
    }
    
    public List<TopTenSalesReport> getTopTenArtistByTotalPrice(String begin , String end ){
        TopTenSalesDao artistDao = new TopTenSalesDao();
        return artistDao.getArtistByTotalPrice(begin,end,10);
    }
}
