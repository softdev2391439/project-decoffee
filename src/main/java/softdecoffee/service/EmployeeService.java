/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package softdecoffee.service;

import java.util.List;
import softdecoffee.dao.EmployeeDao;
import softdecoffee.model.Employee;

/**
 *
 * @author werapan
 */
public class EmployeeService {

    public Employee getUserAndPassword(String loing, String password) {
        EmployeeDao employeeDao = new EmployeeDao();
        return employeeDao.get(loing, password);
    }

    public Employee getByUser(String username) {
        EmployeeDao employeeDao = new EmployeeDao();
        return employeeDao.getUser(username);
    }

    public Employee getById(int id) {
        EmployeeDao employeeDao = new EmployeeDao();
        return employeeDao.get(id);
    }

    public List<Employee> getEmployees() {
        EmployeeDao employeeDao = new EmployeeDao();
        return employeeDao.getAll("EMP_CODE asc");
    }

    public List<Employee> getEmByPayroll() {
        EmployeeDao employeeDao = new EmployeeDao();
        return employeeDao.getEmByPayroll();
    }

    public static Employee login(String username, String password) {
        EmployeeDao employeeDao = new EmployeeDao();
        Employee employee = employeeDao.getByLogin(username, password);

        if (employee != null) {
            return employee;
        } else {
            return null;
        }
    }

    public Employee addNew(Employee editedEmployee) {
        EmployeeDao employeeDao = new EmployeeDao();
        return employeeDao.save(editedEmployee);
    }

    public Employee update(Employee editedEmployee) {
        EmployeeDao employeeDao = new EmployeeDao();
        return employeeDao.update(editedEmployee);
    }

    public int delete(Employee editedEmployee) {
        EmployeeDao employeeDao = new EmployeeDao();
        return employeeDao.delete(editedEmployee);
    }

    public static Employee getCurrentUser() {
        return new Employee();
    }
}
