/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package softdecoffee.service;


import java.util.ArrayList;
import java.util.List;
import softdecoffee.dao.PromotionDao;
import softdecoffee.dao.PromotionDao;
import softdecoffee.model.Promotion;
import softdecoffee.model.Promotion;

/**
 *
 * @author Nobpharat
 */
public class PromotionService {   
    public Promotion getById(int id){
        PromotionDao promotionDao = new PromotionDao();
        return promotionDao.get(id);
    }
    public List<Promotion> getPromotions(){
        PromotionDao promotionDao = new PromotionDao();
        
        return promotionDao.getAll(" PRO_CODE asc");
    }

    public Promotion addNew(Promotion editedPromotion) {
       PromotionDao promotionDao = new PromotionDao();
        return promotionDao.save(editedPromotion);
    }

    public Promotion update(Promotion editedPromotion) {
        PromotionDao promotionDao = new PromotionDao();
        return promotionDao.update(editedPromotion);
    }

    public int delete(Promotion editedPromotion) {
        PromotionDao promotionDao = new PromotionDao();
        return promotionDao.delete(editedPromotion);
    }
    
    private PromotionDao promotionDao = new PromotionDao();

    public ArrayList<Promotion> getPromotionsOrderByName() {
        return (ArrayList<Promotion>) promotionDao.getAll(" PRO_NAME asc");
    }
}

