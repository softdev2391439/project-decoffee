/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package softdecoffee.service;

import java.util.List;
import softdecoffee.dao.UserDao;
import softdecoffee.model.User;

/**
 *
 * @author natta
 */
public class UserService {

    public User login(String login, String password) {
        UserDao userDao = new UserDao();
        User user = userDao.getByLogin(login);
        if (user != null && user.getPassword().equals(password)) {
            userDao.updateLastLogin(user);
            return user;
        }
        return null;
    }

    public User getById(int id) {
        UserDao userDao = new UserDao();
        return userDao.get(id);
    }

    public List<User> getUsers() {
        UserDao userDao = new UserDao();
        return userDao.getAll(" U_ID asc");
    }

    public User addNew(User editedUser) {
        UserDao userDao = new UserDao();
        return userDao.save(editedUser);
    }

    public User update(User editedUser) {
        UserDao userDao = new UserDao();
        return userDao.update(editedUser);
    }

    public int delete(User editedUser) {
        UserDao userDao = new UserDao();
        return userDao.delete(editedUser);
    }

}
