/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package softdecoffee.service;


import java.util.List;
import softdecoffee.dao.ProductDao;
import softdecoffee.model.Product;

/**
 *
 * @author Nobpharat
 */
public class ProductService {   

    public static boolean getProductsByCategory;
    public Product getById(int id){
        ProductDao ProductDao = new ProductDao();
        return ProductDao.get(id);
    }
    public static List<Product> getProducts(){
        ProductDao ProductDao = new ProductDao();
        
        return ProductDao.getAll(" PROD_CODE asc");
    }
    public Product addNew(Product editedProduct) {
       ProductDao ProductDao = new ProductDao();
        return ProductDao.save(editedProduct);
    }

    public Product update(Product editedProduct) {
        ProductDao ProductDao = new ProductDao();
        return ProductDao.update(editedProduct);
    }

    public int delete(Product editedProduct) {
        ProductDao ProductDao = new ProductDao();
        return ProductDao.delete(editedProduct);
    }
}

