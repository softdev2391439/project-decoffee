/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package softdecoffee.service;

import java.util.List;
import softdecoffee.dao.VendorDao;
import softdecoffee.model.Vendor;


/**
 *
 * @author ASUS
 */
public class VendorService {
    public Vendor getByTel(String tel) {
        VendorDao vendorDao = new VendorDao();
        Vendor vendor = vendorDao.getByTel(tel);
        return vendor;
    }
    public Vendor getById(int id){
        VendorDao vendorDao = new VendorDao();
        return vendorDao.get(id);
    }
    
    public List<Vendor> getVendors(){
        VendorDao vendorDao = new VendorDao();
        return vendorDao.getAll(" V_CODE asc");
    }

    public Vendor addNew(Vendor editedVendor) {
        VendorDao vendorDao = new VendorDao();
        return vendorDao.save(editedVendor);
    }

    public Vendor update(Vendor editedVendor) {
        VendorDao vendorDao = new VendorDao();
        return vendorDao.update(editedVendor);
    }

    public int delete(Vendor editedVendor) {
        VendorDao vendorDao = new VendorDao();
        return vendorDao.delete(editedVendor);
    }


    
}
