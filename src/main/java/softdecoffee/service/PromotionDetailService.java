/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package softdecoffee.service;


import java.util.List;
import softdecoffee.dao.PromotionDetailDao;
import softdecoffee.model.PromotionDetail;

/**
 *
 * @author basba
 */
public class PromotionDetailService {   
    public PromotionDetail getById(int id){
        PromotionDetailDao promotionDetailDao = new PromotionDetailDao();
        return promotionDetailDao.get(id);
    }
    public List<PromotionDetail> getPromotionDetails(){
        PromotionDetailDao promotionDetailDao = new PromotionDetailDao();
        
        return promotionDetailDao.getAll(" PRDE_CODE asc");
    }

    public PromotionDetail addNew(PromotionDetail editedPromotionDetail) {
       PromotionDetailDao promotionDetailDao = new PromotionDetailDao();
        return promotionDetailDao.save(editedPromotionDetail);
    }

    public PromotionDetail update(PromotionDetail editedPromotionDetail) {
        PromotionDetailDao promotionDetailDao = new PromotionDetailDao();
        return promotionDetailDao.update(editedPromotionDetail);
    }

    public int delete(PromotionDetail editedPromotionDetail) {
        PromotionDetailDao promotionDetailDao = new PromotionDetailDao();
        return promotionDetailDao.delete(editedPromotionDetail);
    }

}

