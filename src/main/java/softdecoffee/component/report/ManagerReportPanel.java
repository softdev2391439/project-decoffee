/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package softdecoffee.component.report;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GradientPaint;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import javax.swing.JComboBox;
import javax.swing.JScrollBar;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import org.jdatepicker.impl.JDatePanelImpl;
import org.jdatepicker.impl.JDatePickerImpl;
import org.jdatepicker.impl.UtilDateModel;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PiePlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.LineAndShapeRenderer;
import org.jfree.chart.title.TextTitle;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.DefaultPieDataset;
import softdecoffee.component.other.DateLabelFormatter;
import softdecoffee.component.other.ScrollBarCustom;
import softdecoffee.model.ReportManager;
import softdecoffee.service.ReportManagerService;

/**
 *
 * @author Nobpharat
 */
public class ManagerReportPanel extends javax.swing.JPanel {

	private UtilDateModel model1;
	private UtilDateModel model2;
	private String date;
	private String start;
	private String end;
	private AbstractTableModel model;
	List<ReportManager> products;
	List<ReportManager> finances;
	List<ReportManager> financesDESC;
	List<ReportManager> materials;
	private DefaultPieDataset pieDataset;
	private DefaultCategoryDataset barDataset;
	private DefaultCategoryDataset lineDataset;

	/**
	 * Creates new form ManagerReportPanel
	 */
	public ManagerReportPanel() {
		initComponents();
		initCustomScrollbar();
		end = getCurrentDate();
		start = getPastDate(30);

		System.out.println(start + " " + end);
		initDatePicker();
		materials = new ReportManagerService().getBalanceMaterial();
		finances = new ReportManagerService().getMoneyInfo((start), (end));
		financesDESC = new ReportManagerService().getMoneyInfoDESC((start), (end));
		initMaterialTable();
		initPieChart();
		loadPieDataSet();
		initLineChart();
		loadLineDataSet("Income");
		initFinanceTable();
		scrBestSeller.setViewportView(new BestSellerList());

	}

	private void initCustomScrollbar() {
		ScrollBarCustom sbv = new ScrollBarCustom();
		ScrollBarCustom sbh = new ScrollBarCustom();
		sbh.setOrientation(JScrollBar.HORIZONTAL);
		
		ScrollBarCustom sbv2 = new ScrollBarCustom();
		ScrollBarCustom sbh2 = new ScrollBarCustom();
		sbh2.setOrientation(JScrollBar.HORIZONTAL);
		
		ScrollBarCustom sbv3 = new ScrollBarCustom();
		ScrollBarCustom sbh3 = new ScrollBarCustom();
		sbh3.setOrientation(JScrollBar.HORIZONTAL);
		
		scrBestSeller.setVerticalScrollBar(sbv);
		scrBestSeller.setHorizontalScrollBar(sbh);
		
		scrFinanceTable.setVerticalScrollBar(sbv2);
		scrFinanceTable.setHorizontalScrollBar(sbh2);
		
		scrMaterialTable.setVerticalScrollBar(sbv3);
		scrMaterialTable.setHorizontalScrollBar(sbh3);
	}

	private void initPieChart() {
		pieDataset = new DefaultPieDataset();
		JFreeChart chart = ChartFactory.createPieChart("Expense", // chart title
			pieDataset, // data
			true, // include legend
			true,
			false);
		//title
		TextTitle title = chart.getTitle();
		title.setFont(new Font("Arial", Font.BOLD, 20));
		title.setPaint(new Color(192, 249, 250));

		//panel and graph
		chart.setBackgroundPaint(new Color(102, 0, 102));
		PiePlot plot = (PiePlot) chart.getPlot();
		GradientPaint gp = new GradientPaint(0, 0, Color.MAGENTA, 0, 300, new Color(138, 43, 226), true);
		plot.setBackgroundPaint(gp);
		//line
		plot.setLabelPaint(Color.WHITE);
		plot.setLabelBackgroundPaint(new Color(0, 0, 0, 255));
		ChartPanel chartPanel = new ChartPanel(chart);
		chartPanel.setPreferredSize(new java.awt.Dimension(300, 300));
		pnlExpen.add(chartPanel);
	}

	private void loadPieDataSet() {
		pieDataset.clear();
		double electric = 0;
		double water = 0;
		double rent = 0;
		double salary = 0;
		double matcost = 0;
		for (ReportManager a : finances) {
			electric += a.getElectricCost();
			water += a.getWaterCost();
			salary += a.getEmpCost();
			rent += a.getRentalCost();
			matcost += a.getMaterialCost();
		}
		pieDataset.setValue("Employee Salary", salary);
		pieDataset.setValue("Electric Bill", electric);
		pieDataset.setValue("Water Bill", water);
		pieDataset.setValue("Buy Material", matcost);
		pieDataset.setValue("Rental Cost", rent);
	}

	private void loadLineDataSet(String head) {
		lineDataset.clear();
		if (head == "Income") {
			for (ReportManager a : finances) {
				String date = getDate(a);
				String month = getYearAndMonth(a);
				lineDataset.addValue(a.getTotalIncome(), month, date);
			}
		} else if (head == "Cost") {
			for (ReportManager a : finances) {
				String date = getDate(a);
				String month = getYearAndMonth(a);
				lineDataset.addValue(a.getTotalCost(), month, date);
			}
		} else if (head == "Profit") {
			for (ReportManager a : finances) {
				String date = getDate(a);
				String month = getYearAndMonth(a);
				lineDataset.addValue(a.getTotalProfit(), month, date);
			}
		}

	}

	private String getDate(ReportManager a) {
		String pattern = "dd";
		SimpleDateFormat formater = new SimpleDateFormat(pattern, Locale.ENGLISH);
		return formater.format(a.getDate());
	}

	private String getYearAndMonth(ReportManager a) {
		String pattern = "MMMM-yyyy";
		SimpleDateFormat formater = new SimpleDateFormat(pattern, Locale.ENGLISH);
		return formater.format(a.getDate());
	}

	private void initLineChart() {
		lineDataset = new DefaultCategoryDataset();
		JFreeChart lineChart = ChartFactory.createLineChart(
			"Finance",
			"Date",
			"Baht",
			lineDataset,
			PlotOrientation.VERTICAL,
			true,
			true,
			false
		);
		lineChart.setBackgroundPaint(new Color(102, 0, 102));
		CategoryPlot plot = lineChart.getCategoryPlot();
		GradientPaint gp = new GradientPaint(0, 0, Color.MAGENTA, 0, 300, new Color(138, 43, 226), true);
		plot.setBackgroundPaint(gp);

		// Set domain axis font
		CategoryAxis domainAxis = plot.getDomainAxis();
		domainAxis.setLabelFont(new Font("Arial", Font.BOLD, 14));
		domainAxis.setTickLabelFont(new Font("Arial", Font.PLAIN, 12));
		domainAxis.setLabelPaint(Color.YELLOW);
		domainAxis.setTickLabelPaint(Color.YELLOW);

		// Set range axis font
		ValueAxis rangeAxis = plot.getRangeAxis();
		rangeAxis.setLabelFont(new Font("Arial", Font.BOLD, 14));
		rangeAxis.setTickLabelFont(new Font("Arial", Font.PLAIN, 12));
		rangeAxis.setLabelPaint(Color.YELLOW);
		rangeAxis.setTickLabelPaint(Color.YELLOW);

		// Set title font
		TextTitle title = lineChart.getTitle();
		title.setFont(new Font("Arial", Font.BOLD, 20));
		title.setPaint(Color.YELLOW);

		// Set line width
		LineAndShapeRenderer renderer = (LineAndShapeRenderer) plot.getRenderer();
		renderer.setSeriesStroke(0, new BasicStroke(3f));

		ChartPanel chartPanel = new ChartPanel(lineChart);
		chartPanel.setPreferredSize(new java.awt.Dimension(560, 300));
		pnlFinace.add(chartPanel);
	}

	/**
	 * This method is called from within the constructor to initialize the
	 * form. WARNING: Do NOT modify this code. The content of this method is
	 * always regenerated by the Form Editor.
	 */
	@SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pnlMoney = new javax.swing.JPanel();
        cmbFinance = new javax.swing.JComboBox<>();
        pnlFinace = new javax.swing.JPanel();
        pnlExpen = new javax.swing.JPanel();
        scrFinanceTable = new javax.swing.JScrollPane();
        tblFinance = new javax.swing.JTable();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        pnlDatePicker1 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        pnlDatePicker2 = new javax.swing.JPanel();
        btnSetTime = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        scrMaterialTable = new javax.swing.JScrollPane();
        tblBalanceMaterial = new javax.swing.JTable();
        scrBestSeller = new javax.swing.JScrollPane();
        jLabel3 = new javax.swing.JLabel();

        setBackground(new java.awt.Color(221, 206, 183));

        pnlMoney.setBackground(new java.awt.Color(221, 206, 183));

        cmbFinance.setBackground(new java.awt.Color(242, 199, 99));
        cmbFinance.setFont(new java.awt.Font("Poppins Medium", 0, 14)); // NOI18N
        cmbFinance.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Income", "Cost", "Profit" }));
        cmbFinance.setPreferredSize(new java.awt.Dimension(82, 29));
        cmbFinance.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbFinanceActionPerformed(evt);
            }
        });

        pnlFinace.setBackground(new java.awt.Color(171, 146, 131));

        pnlExpen.setBackground(new java.awt.Color(171, 146, 131));

        scrFinanceTable.setBackground(new java.awt.Color(102, 102, 255));
        scrFinanceTable.setBorder(null);

        tblFinance.setBackground(new java.awt.Color(204, 204, 204));
        tblFinance.setFont(new java.awt.Font("Poppins", 0, 12)); // NOI18N
        tblFinance.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        scrFinanceTable.setViewportView(tblFinance);

        jPanel2.setBackground(new java.awt.Color(221, 206, 183));

        jLabel1.setFont(new java.awt.Font("Poppins Medium", 1, 24)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Finance Infomation");

        pnlDatePicker1.setBackground(new java.awt.Color(255, 255, 255));

        jLabel4.setBackground(new java.awt.Color(131, 100, 82));
        jLabel4.setFont(new java.awt.Font("Poppins Medium", 0, 14)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel4.setText("TO");
        jLabel4.setOpaque(true);

        pnlDatePicker2.setBackground(new java.awt.Color(255, 255, 255));

        btnSetTime.setBackground(new java.awt.Color(242, 199, 99));
        btnSetTime.setFont(new java.awt.Font("Poppins Medium", 0, 14)); // NOI18N
        btnSetTime.setText("SET");
        btnSetTime.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSetTimeActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(159, 159, 159)
                .addComponent(pnlDatePicker1, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(pnlDatePicker2, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnSetTime, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(191, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pnlDatePicker1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pnlDatePicker2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnSetTime, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE))
                .addContainerGap())
        );

        javax.swing.GroupLayout pnlMoneyLayout = new javax.swing.GroupLayout(pnlMoney);
        pnlMoney.setLayout(pnlMoneyLayout);
        pnlMoneyLayout.setHorizontalGroup(
            pnlMoneyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(scrFinanceTable, javax.swing.GroupLayout.Alignment.TRAILING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlMoneyLayout.createSequentialGroup()
                .addGroup(pnlMoneyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cmbFinance, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(pnlFinace, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnlExpen, javax.swing.GroupLayout.PREFERRED_SIZE, 391, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        pnlMoneyLayout.setVerticalGroup(
            pnlMoneyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlMoneyLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(cmbFinance, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlMoneyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(pnlExpen, javax.swing.GroupLayout.DEFAULT_SIZE, 278, Short.MAX_VALUE)
                    .addComponent(pnlFinace, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(scrFinanceTable, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(60, 60, 60))
        );

        jPanel1.setBackground(new java.awt.Color(171, 146, 131));

        jLabel5.setFont(new java.awt.Font("Poppins Medium", 0, 18)); // NOI18N
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel5.setText("Balance Material");

        tblBalanceMaterial.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        scrMaterialTable.setViewportView(tblBalanceMaterial);

        scrBestSeller.setBackground(new java.awt.Color(255, 204, 255));
        scrBestSeller.setBorder(null);

        jLabel3.setFont(new java.awt.Font("Poppins Medium", 0, 18)); // NOI18N
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel3.setText("Best Seller Of Month");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(scrMaterialTable, javax.swing.GroupLayout.DEFAULT_SIZE, 407, Short.MAX_VALUE)
                    .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(scrBestSeller)
                    .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(jLabel5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(scrMaterialTable, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addComponent(scrBestSeller, javax.swing.GroupLayout.DEFAULT_SIZE, 212, Short.MAX_VALUE))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pnlMoney, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnlMoney, javax.swing.GroupLayout.PREFERRED_SIZE, 628, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

        private void cmbFinanceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbFinanceActionPerformed
		JComboBox<String> comboBox = (JComboBox<String>) evt.getSource();
		String selectedOption = (String) comboBox.getSelectedItem();
		if (selectedOption != null) {
			System.out.println("Selected: " + selectedOption);

			loadLineDataSet(selectedOption);
		}
        }//GEN-LAST:event_cmbFinanceActionPerformed

        private void btnSetTimeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSetTimeActionPerformed
		String pattern = "yyyy-MM-dd";
		SimpleDateFormat formater = new SimpleDateFormat(pattern, Locale.ENGLISH);
		System.out.println("" + formater.format(model1.getValue()) + " " + formater.format(model2.getValue()));
		String beginDate = formater.format(model1.getValue());
		String endDate = formater.format(model2.getValue());
		finances = new ReportManagerService().getMoneyInfo(beginDate, endDate);
		financesDESC = new ReportManagerService().getMoneyInfoDESC((beginDate), (endDate));
		materials = new ReportManagerService().getBalanceMaterial();
		loadPieDataSet();
		tblFinance.repaint();
		loadLineDataSet(cmbFinance.getSelectedItem().toString());
        }//GEN-LAST:event_btnSetTimeActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnSetTime;
    private javax.swing.JComboBox<String> cmbFinance;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel pnlDatePicker1;
    private javax.swing.JPanel pnlDatePicker2;
    private javax.swing.JPanel pnlExpen;
    private javax.swing.JPanel pnlFinace;
    private javax.swing.JPanel pnlMoney;
    private javax.swing.JScrollPane scrBestSeller;
    private javax.swing.JScrollPane scrFinanceTable;
    private javax.swing.JScrollPane scrMaterialTable;
    private javax.swing.JTable tblBalanceMaterial;
    private javax.swing.JTable tblFinance;
    // End of variables declaration//GEN-END:variables
	private static String getFormattedDate(LocalDate date) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		return date.format(formatter);
	}

	private static String getCurrentDate() {
		LocalDate localDate = LocalDate.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		String formattedDate = localDate.format(formatter);
		System.out.println("Formatted Date: " + formattedDate);
		return formattedDate;
	}

	private static String getPastDate(int days) {
		LocalDate currentDate = LocalDate.now();
		LocalDate pastDate = currentDate.minusDays(days);
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		String formattedDate = pastDate.format(formatter);
		return formattedDate;
	}

	private void initMaterialTable() {
		model = new AbstractTableModel() {

			String[] columnNames = {"Name", "Quantity"};

			@Override
			public String getColumnName(int column) {
				return columnNames[column];
			}

			@Override
			public int getRowCount() {
				return materials.size();
			}

			@Override
			public int getColumnCount() {
				return 2;
			}

			@Override
			public Object getValueAt(int rowIndex, int columnIndex) {
				ReportManager material = materials.get(rowIndex);
				switch (columnIndex) {
					case 0:
						return material.getMaterialName();
					case 1:
						return material.getMaterialQty();

					default:
						return "";
				}
			}
		};
		tblBalanceMaterial.setModel(model);
	}

	private void initFinanceTable() {
		
		model = new AbstractTableModel() {

			String[] columnNames = {"Date", "Income", "Expense", "Profit", "Electric cost", "Water cost", "Rental cost", "Material cost", "Employee cost"};

			@Override
			public String getColumnName(int column) {
				return columnNames[column];
			}

			@Override
			public int getRowCount() {
				return financesDESC.size();
			}

			@Override
			public int getColumnCount() {
				return 9;
			}

			@Override
			public Object getValueAt(int rowIndex, int columnIndex) {
				ReportManager finance = financesDESC.get(rowIndex);
				switch (columnIndex) {
					case 0:
						return finance.getDate();
					case 1:
						return finance.getTotalIncome();
					case 2:
						return finance.getTotalCost();
					case 3:
						return finance.getTotalProfit();
					case 4:
						return finance.getElectricCost();
					case 5:
						return finance.getWaterCost();
					case 6:
						return finance.getRentalCost();
					case 7:
						return finance.getMaterialCost();
					case 8:
						return finance.getEmpCost();

					default:
						return "";
				}
			}
		};

		tblFinance.setDefaultRenderer(Object.class, new DefaultTableCellRenderer() {
			@Override
			public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
				Component c = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
				if (column == 3 && finances.get(row).getTotalProfit() < 0) {
					c.setBackground(new Color(255, 51, 51));
				} else if (isSelected) {
					c.setBackground(table.getSelectionBackground());
					c.setForeground(table.getSelectionForeground());
				} else {
					Color startColor = new Color(0, 0, 153);
					Color endColor = new Color(153, 0, 153);
					float ratio = (float) row / (float) table.getRowCount();
					int red = (int) (startColor.getRed() * (1 - ratio) + endColor.getRed() * ratio);
					int green = (int) (startColor.getGreen() * (1 - ratio) + endColor.getGreen() * ratio);
					int blue = (int) (startColor.getBlue() * (1 - ratio) + endColor.getBlue() * ratio);
					Color background = new Color(red, green, blue);
					c.setBackground(background);
					c.setForeground(Color.WHITE);
				}
				return c;
			}
		});

		tblFinance.setModel(model);
	}

	private void initDatePicker() {
		model1 = new UtilDateModel();
		Properties p1 = new Properties();
		p1.put("text.today", "Today");
		p1.put("text.month", "Month");
		p1.put("text.year", "Year");
		JDatePanelImpl datePanel1 = new JDatePanelImpl(model1, p1);
		JDatePickerImpl datePicker1 = new JDatePickerImpl(datePanel1, new DateLabelFormatter());
		datePicker1.setPreferredSize(new Dimension(150, 50));
		pnlDatePicker1.add(datePicker1);
		model1.setSelected(true);

		model2 = new UtilDateModel();
		Properties p2 = new Properties();
		p2.put("text.today", "Today");
		p2.put("text.month", "Month");
		p2.put("text.year", "Year");
		JDatePanelImpl datePanel2 = new JDatePanelImpl(model2, p2);
		JDatePickerImpl datePicker2 = new JDatePickerImpl(datePanel2, new DateLabelFormatter());
		datePicker2.setPreferredSize(new Dimension(150, 50));
		pnlDatePicker2.add(datePicker2);
		model2.setSelected(true);
	}
}
