--
-- File generated with SQLiteStudio v3.4.4 on อ. ต.ค. 24 18:34:04 2023
--
-- Text encoding used: UTF-8
--
PRAGMA foreign_keys = off;
BEGIN TRANSACTION;

-- Table: BILL_DETAIL
DROP TABLE IF EXISTS BILL_DETAIL;

CREATE TABLE IF NOT EXISTS BILL_DETAIL (
    BD_CODE           INTEGER PRIMARY KEY ASC AUTOINCREMENT
                              UNIQUE
                              NOT NULL,
    BILL_CODE         INTEGER REFERENCES BILL_STOCK (BILL_CODE) ON DELETE RESTRICT
                                                                ON UPDATE CASCADE,
    BD_AMOUNT         NUMERIC NOT NULL,
    BD_PRICE_PER_UNIT NUMERIC NOT NULL,
    BD_PRICE_TOTAL    NUMERIC NOT NULL,
    M_CODE                    REFERENCES MATERIAL (M_CODE) ON DELETE RESTRICT
                                                           ON UPDATE CASCADE
);

INSERT INTO BILL_DETAIL (
                            BD_CODE,
                            BILL_CODE,
                            BD_AMOUNT,
                            BD_PRICE_PER_UNIT,
                            BD_PRICE_TOTAL,
                            M_CODE
                        )
                        VALUES (
                            1,
                            1,
                            5,
                            50,
                            250,
                            1
                        );

INSERT INTO BILL_DETAIL (
                            BD_CODE,
                            BILL_CODE,
                            BD_AMOUNT,
                            BD_PRICE_PER_UNIT,
                            BD_PRICE_TOTAL,
                            M_CODE
                        )
                        VALUES (
                            2,
                            2,
                            3,
                            50,
                            150,
                            2
                        );

INSERT INTO BILL_DETAIL (
                            BD_CODE,
                            BILL_CODE,
                            BD_AMOUNT,
                            BD_PRICE_PER_UNIT,
                            BD_PRICE_TOTAL,
                            M_CODE
                        )
                        VALUES (
                            3,
                            3,
                            6,
                            50,
                            300,
                            3
                        );

INSERT INTO BILL_DETAIL (
                            BD_CODE,
                            BILL_CODE,
                            BD_AMOUNT,
                            BD_PRICE_PER_UNIT,
                            BD_PRICE_TOTAL,
                            M_CODE
                        )
                        VALUES (
                            4,
                            4,
                            4,
                            50,
                            200,
                            4
                        );

INSERT INTO BILL_DETAIL (
                            BD_CODE,
                            BILL_CODE,
                            BD_AMOUNT,
                            BD_PRICE_PER_UNIT,
                            BD_PRICE_TOTAL,
                            M_CODE
                        )
                        VALUES (
                            5,
                            5,
                            2,
                            50,
                            100,
                            5
                        );

INSERT INTO BILL_DETAIL (
                            BD_CODE,
                            BILL_CODE,
                            BD_AMOUNT,
                            BD_PRICE_PER_UNIT,
                            BD_PRICE_TOTAL,
                            M_CODE
                        )
                        VALUES (
                            6,
                            6,
                            5,
                            50,
                            250,
                            6
                        );

INSERT INTO BILL_DETAIL (
                            BD_CODE,
                            BILL_CODE,
                            BD_AMOUNT,
                            BD_PRICE_PER_UNIT,
                            BD_PRICE_TOTAL,
                            M_CODE
                        )
                        VALUES (
                            7,
                            7,
                            1,
                            50,
                            50,
                            7
                        );

INSERT INTO BILL_DETAIL (
                            BD_CODE,
                            BILL_CODE,
                            BD_AMOUNT,
                            BD_PRICE_PER_UNIT,
                            BD_PRICE_TOTAL,
                            M_CODE
                        )
                        VALUES (
                            8,
                            8,
                            4,
                            50,
                            200,
                            8
                        );

INSERT INTO BILL_DETAIL (
                            BD_CODE,
                            BILL_CODE,
                            BD_AMOUNT,
                            BD_PRICE_PER_UNIT,
                            BD_PRICE_TOTAL,
                            M_CODE
                        )
                        VALUES (
                            9,
                            9,
                            3,
                            50,
                            150,
                            9
                        );

INSERT INTO BILL_DETAIL (
                            BD_CODE,
                            BILL_CODE,
                            BD_AMOUNT,
                            BD_PRICE_PER_UNIT,
                            BD_PRICE_TOTAL,
                            M_CODE
                        )
                        VALUES (
                            10,
                            10,
                            2,
                            50,
                            100,
                            10
                        );

INSERT INTO BILL_DETAIL (
                            BD_CODE,
                            BILL_CODE,
                            BD_AMOUNT,
                            BD_PRICE_PER_UNIT,
                            BD_PRICE_TOTAL,
                            M_CODE
                        )
                        VALUES (
                            11,
                            11,
                            3,
                            50,
                            150,
                            11
                        );

INSERT INTO BILL_DETAIL (
                            BD_CODE,
                            BILL_CODE,
                            BD_AMOUNT,
                            BD_PRICE_PER_UNIT,
                            BD_PRICE_TOTAL,
                            M_CODE
                        )
                        VALUES (
                            12,
                            12,
                            6,
                            50,
                            300,
                            12
                        );

INSERT INTO BILL_DETAIL (
                            BD_CODE,
                            BILL_CODE,
                            BD_AMOUNT,
                            BD_PRICE_PER_UNIT,
                            BD_PRICE_TOTAL,
                            M_CODE
                        )
                        VALUES (
                            13,
                            13,
                            5,
                            50,
                            250,
                            13
                        );

INSERT INTO BILL_DETAIL (
                            BD_CODE,
                            BILL_CODE,
                            BD_AMOUNT,
                            BD_PRICE_PER_UNIT,
                            BD_PRICE_TOTAL,
                            M_CODE
                        )
                        VALUES (
                            14,
                            14,
                            4,
                            50,
                            200,
                            14
                        );

INSERT INTO BILL_DETAIL (
                            BD_CODE,
                            BILL_CODE,
                            BD_AMOUNT,
                            BD_PRICE_PER_UNIT,
                            BD_PRICE_TOTAL,
                            M_CODE
                        )
                        VALUES (
                            15,
                            15,
                            2,
                            50,
                            100,
                            15
                        );

INSERT INTO BILL_DETAIL (
                            BD_CODE,
                            BILL_CODE,
                            BD_AMOUNT,
                            BD_PRICE_PER_UNIT,
                            BD_PRICE_TOTAL,
                            M_CODE
                        )
                        VALUES (
                            16,
                            16,
                            1,
                            90,
                            90,
                            16
                        );

INSERT INTO BILL_DETAIL (
                            BD_CODE,
                            BILL_CODE,
                            BD_AMOUNT,
                            BD_PRICE_PER_UNIT,
                            BD_PRICE_TOTAL,
                            M_CODE
                        )
                        VALUES (
                            17,
                            17,
                            5,
                            90,
                            450,
                            17
                        );

INSERT INTO BILL_DETAIL (
                            BD_CODE,
                            BILL_CODE,
                            BD_AMOUNT,
                            BD_PRICE_PER_UNIT,
                            BD_PRICE_TOTAL,
                            M_CODE
                        )
                        VALUES (
                            18,
                            18,
                            6,
                            90,
                            540,
                            18
                        );

INSERT INTO BILL_DETAIL (
                            BD_CODE,
                            BILL_CODE,
                            BD_AMOUNT,
                            BD_PRICE_PER_UNIT,
                            BD_PRICE_TOTAL,
                            M_CODE
                        )
                        VALUES (
                            19,
                            19,
                            3,
                            90,
                            270,
                            19
                        );

INSERT INTO BILL_DETAIL (
                            BD_CODE,
                            BILL_CODE,
                            BD_AMOUNT,
                            BD_PRICE_PER_UNIT,
                            BD_PRICE_TOTAL,
                            M_CODE
                        )
                        VALUES (
                            20,
                            20,
                            2,
                            90,
                            180,
                            20
                        );

INSERT INTO BILL_DETAIL (
                            BD_CODE,
                            BILL_CODE,
                            BD_AMOUNT,
                            BD_PRICE_PER_UNIT,
                            BD_PRICE_TOTAL,
                            M_CODE
                        )
                        VALUES (
                            21,
                            21,
                            4,
                            90,
                            360,
                            1
                        );

INSERT INTO BILL_DETAIL (
                            BD_CODE,
                            BILL_CODE,
                            BD_AMOUNT,
                            BD_PRICE_PER_UNIT,
                            BD_PRICE_TOTAL,
                            M_CODE
                        )
                        VALUES (
                            22,
                            22,
                            5,
                            90,
                            450,
                            2
                        );

INSERT INTO BILL_DETAIL (
                            BD_CODE,
                            BILL_CODE,
                            BD_AMOUNT,
                            BD_PRICE_PER_UNIT,
                            BD_PRICE_TOTAL,
                            M_CODE
                        )
                        VALUES (
                            23,
                            23,
                            6,
                            90,
                            540,
                            3
                        );

INSERT INTO BILL_DETAIL (
                            BD_CODE,
                            BILL_CODE,
                            BD_AMOUNT,
                            BD_PRICE_PER_UNIT,
                            BD_PRICE_TOTAL,
                            M_CODE
                        )
                        VALUES (
                            24,
                            24,
                            1,
                            90,
                            90,
                            4
                        );

INSERT INTO BILL_DETAIL (
                            BD_CODE,
                            BILL_CODE,
                            BD_AMOUNT,
                            BD_PRICE_PER_UNIT,
                            BD_PRICE_TOTAL,
                            M_CODE
                        )
                        VALUES (
                            25,
                            25,
                            4,
                            90,
                            360,
                            5
                        );

INSERT INTO BILL_DETAIL (
                            BD_CODE,
                            BILL_CODE,
                            BD_AMOUNT,
                            BD_PRICE_PER_UNIT,
                            BD_PRICE_TOTAL,
                            M_CODE
                        )
                        VALUES (
                            26,
                            26,
                            3,
                            90,
                            270,
                            6
                        );

INSERT INTO BILL_DETAIL (
                            BD_CODE,
                            BILL_CODE,
                            BD_AMOUNT,
                            BD_PRICE_PER_UNIT,
                            BD_PRICE_TOTAL,
                            M_CODE
                        )
                        VALUES (
                            27,
                            27,
                            2,
                            90,
                            180,
                            7
                        );

INSERT INTO BILL_DETAIL (
                            BD_CODE,
                            BILL_CODE,
                            BD_AMOUNT,
                            BD_PRICE_PER_UNIT,
                            BD_PRICE_TOTAL,
                            M_CODE
                        )
                        VALUES (
                            28,
                            28,
                            5,
                            90,
                            450,
                            8
                        );

INSERT INTO BILL_DETAIL (
                            BD_CODE,
                            BILL_CODE,
                            BD_AMOUNT,
                            BD_PRICE_PER_UNIT,
                            BD_PRICE_TOTAL,
                            M_CODE
                        )
                        VALUES (
                            29,
                            29,
                            6,
                            90,
                            540,
                            9
                        );

INSERT INTO BILL_DETAIL (
                            BD_CODE,
                            BILL_CODE,
                            BD_AMOUNT,
                            BD_PRICE_PER_UNIT,
                            BD_PRICE_TOTAL,
                            M_CODE
                        )
                        VALUES (
                            30,
                            30,
                            3,
                            90,
                            270,
                            10
                        );

INSERT INTO BILL_DETAIL (
                            BD_CODE,
                            BILL_CODE,
                            BD_AMOUNT,
                            BD_PRICE_PER_UNIT,
                            BD_PRICE_TOTAL,
                            M_CODE
                        )
                        VALUES (
                            31,
                            31,
                            2,
                            150,
                            300,
                            11
                        );

INSERT INTO BILL_DETAIL (
                            BD_CODE,
                            BILL_CODE,
                            BD_AMOUNT,
                            BD_PRICE_PER_UNIT,
                            BD_PRICE_TOTAL,
                            M_CODE
                        )
                        VALUES (
                            32,
                            32,
                            1,
                            150,
                            150,
                            12
                        );

INSERT INTO BILL_DETAIL (
                            BD_CODE,
                            BILL_CODE,
                            BD_AMOUNT,
                            BD_PRICE_PER_UNIT,
                            BD_PRICE_TOTAL,
                            M_CODE
                        )
                        VALUES (
                            33,
                            33,
                            6,
                            150,
                            90,
                            13
                        );

INSERT INTO BILL_DETAIL (
                            BD_CODE,
                            BILL_CODE,
                            BD_AMOUNT,
                            BD_PRICE_PER_UNIT,
                            BD_PRICE_TOTAL,
                            M_CODE
                        )
                        VALUES (
                            34,
                            34,
                            5,
                            150,
                            750,
                            14
                        );

INSERT INTO BILL_DETAIL (
                            BD_CODE,
                            BILL_CODE,
                            BD_AMOUNT,
                            BD_PRICE_PER_UNIT,
                            BD_PRICE_TOTAL,
                            M_CODE
                        )
                        VALUES (
                            35,
                            35,
                            4,
                            150,
                            600,
                            15
                        );

INSERT INTO BILL_DETAIL (
                            BD_CODE,
                            BILL_CODE,
                            BD_AMOUNT,
                            BD_PRICE_PER_UNIT,
                            BD_PRICE_TOTAL,
                            M_CODE
                        )
                        VALUES (
                            36,
                            1,
                            5,
                            150,
                            750,
                            16
                        );

INSERT INTO BILL_DETAIL (
                            BD_CODE,
                            BILL_CODE,
                            BD_AMOUNT,
                            BD_PRICE_PER_UNIT,
                            BD_PRICE_TOTAL,
                            M_CODE
                        )
                        VALUES (
                            37,
                            2,
                            3,
                            150,
                            450,
                            17
                        );

INSERT INTO BILL_DETAIL (
                            BD_CODE,
                            BILL_CODE,
                            BD_AMOUNT,
                            BD_PRICE_PER_UNIT,
                            BD_PRICE_TOTAL,
                            M_CODE
                        )
                        VALUES (
                            38,
                            3,
                            6,
                            150,
                            900,
                            18
                        );

INSERT INTO BILL_DETAIL (
                            BD_CODE,
                            BILL_CODE,
                            BD_AMOUNT,
                            BD_PRICE_PER_UNIT,
                            BD_PRICE_TOTAL,
                            M_CODE
                        )
                        VALUES (
                            39,
                            4,
                            4,
                            150,
                            600,
                            19
                        );

INSERT INTO BILL_DETAIL (
                            BD_CODE,
                            BILL_CODE,
                            BD_AMOUNT,
                            BD_PRICE_PER_UNIT,
                            BD_PRICE_TOTAL,
                            M_CODE
                        )
                        VALUES (
                            40,
                            5,
                            2,
                            150,
                            300,
                            20
                        );

INSERT INTO BILL_DETAIL (
                            BD_CODE,
                            BILL_CODE,
                            BD_AMOUNT,
                            BD_PRICE_PER_UNIT,
                            BD_PRICE_TOTAL,
                            M_CODE
                        )
                        VALUES (
                            41,
                            6,
                            5,
                            150,
                            750,
                            1
                        );

INSERT INTO BILL_DETAIL (
                            BD_CODE,
                            BILL_CODE,
                            BD_AMOUNT,
                            BD_PRICE_PER_UNIT,
                            BD_PRICE_TOTAL,
                            M_CODE
                        )
                        VALUES (
                            42,
                            7,
                            1,
                            150,
                            150,
                            2
                        );

INSERT INTO BILL_DETAIL (
                            BD_CODE,
                            BILL_CODE,
                            BD_AMOUNT,
                            BD_PRICE_PER_UNIT,
                            BD_PRICE_TOTAL,
                            M_CODE
                        )
                        VALUES (
                            43,
                            8,
                            4,
                            150,
                            600,
                            3
                        );

INSERT INTO BILL_DETAIL (
                            BD_CODE,
                            BILL_CODE,
                            BD_AMOUNT,
                            BD_PRICE_PER_UNIT,
                            BD_PRICE_TOTAL,
                            M_CODE
                        )
                        VALUES (
                            44,
                            9,
                            3,
                            150,
                            450,
                            4
                        );

INSERT INTO BILL_DETAIL (
                            BD_CODE,
                            BILL_CODE,
                            BD_AMOUNT,
                            BD_PRICE_PER_UNIT,
                            BD_PRICE_TOTAL,
                            M_CODE
                        )
                        VALUES (
                            45,
                            10,
                            2,
                            150,
                            300,
                            5
                        );

INSERT INTO BILL_DETAIL (
                            BD_CODE,
                            BILL_CODE,
                            BD_AMOUNT,
                            BD_PRICE_PER_UNIT,
                            BD_PRICE_TOTAL,
                            M_CODE
                        )
                        VALUES (
                            46,
                            11,
                            3,
                            150,
                            450,
                            6
                        );

INSERT INTO BILL_DETAIL (
                            BD_CODE,
                            BILL_CODE,
                            BD_AMOUNT,
                            BD_PRICE_PER_UNIT,
                            BD_PRICE_TOTAL,
                            M_CODE
                        )
                        VALUES (
                            47,
                            12,
                            6,
                            150,
                            900,
                            7
                        );

INSERT INTO BILL_DETAIL (
                            BD_CODE,
                            BILL_CODE,
                            BD_AMOUNT,
                            BD_PRICE_PER_UNIT,
                            BD_PRICE_TOTAL,
                            M_CODE
                        )
                        VALUES (
                            48,
                            13,
                            5,
                            150,
                            750,
                            8
                        );

INSERT INTO BILL_DETAIL (
                            BD_CODE,
                            BILL_CODE,
                            BD_AMOUNT,
                            BD_PRICE_PER_UNIT,
                            BD_PRICE_TOTAL,
                            M_CODE
                        )
                        VALUES (
                            49,
                            14,
                            4,
                            150,
                            600,
                            9
                        );

INSERT INTO BILL_DETAIL (
                            BD_CODE,
                            BILL_CODE,
                            BD_AMOUNT,
                            BD_PRICE_PER_UNIT,
                            BD_PRICE_TOTAL,
                            M_CODE
                        )
                        VALUES (
                            50,
                            15,
                            2,
                            150,
                            300,
                            10
                        );

INSERT INTO BILL_DETAIL (
                            BD_CODE,
                            BILL_CODE,
                            BD_AMOUNT,
                            BD_PRICE_PER_UNIT,
                            BD_PRICE_TOTAL,
                            M_CODE
                        )
                        VALUES (
                            51,
                            16,
                            1,
                            150,
                            150,
                            11
                        );

INSERT INTO BILL_DETAIL (
                            BD_CODE,
                            BILL_CODE,
                            BD_AMOUNT,
                            BD_PRICE_PER_UNIT,
                            BD_PRICE_TOTAL,
                            M_CODE
                        )
                        VALUES (
                            52,
                            17,
                            5,
                            150,
                            750,
                            12
                        );

INSERT INTO BILL_DETAIL (
                            BD_CODE,
                            BILL_CODE,
                            BD_AMOUNT,
                            BD_PRICE_PER_UNIT,
                            BD_PRICE_TOTAL,
                            M_CODE
                        )
                        VALUES (
                            53,
                            18,
                            6,
                            150,
                            900,
                            13
                        );

INSERT INTO BILL_DETAIL (
                            BD_CODE,
                            BILL_CODE,
                            BD_AMOUNT,
                            BD_PRICE_PER_UNIT,
                            BD_PRICE_TOTAL,
                            M_CODE
                        )
                        VALUES (
                            54,
                            19,
                            3,
                            150,
                            450,
                            14
                        );

INSERT INTO BILL_DETAIL (
                            BD_CODE,
                            BILL_CODE,
                            BD_AMOUNT,
                            BD_PRICE_PER_UNIT,
                            BD_PRICE_TOTAL,
                            M_CODE
                        )
                        VALUES (
                            55,
                            20,
                            2,
                            150,
                            300,
                            15
                        );


-- Table: BILL_STOCK
DROP TABLE IF EXISTS BILL_STOCK;

CREATE TABLE IF NOT EXISTS BILL_STOCK (
    BILL_CODE        INTEGER  UNIQUE
                              NOT NULL
                              PRIMARY KEY ASC AUTOINCREMENT,
    BILL_TOTAL_PRICE INTEGER  NOT NULL,
    BILL_TOTAL_QTY   INTEGER  NOT NULL,
    BILL_DATETIME    DATETIME DEFAULT ( (datetime('now', 'localtime') ) ) 
                              NOT NULL,
    EMP_CODE         INTEGER  REFERENCES EMPLOYEE (EMP_CODE) ON DELETE CASCADE
                                                             ON UPDATE NO ACTION,
    BILL_PAY         NUMERIC  NOT NULL,
    V_CODE           INTEGER  REFERENCES VENDOR (V_CODE) ON DELETE RESTRICT
                                                         ON UPDATE CASCADE
);

INSERT INTO BILL_STOCK (
                           BILL_CODE,
                           BILL_TOTAL_PRICE,
                           BILL_TOTAL_QTY,
                           BILL_DATETIME,
                           EMP_CODE,
                           BILL_PAY,
                           V_CODE
                       )
                       VALUES (
                           1,
                           500,
                           5,
                           '2023-10-09 08:00:00',
                           1,
                           500,
                           2
                       );

INSERT INTO BILL_STOCK (
                           BILL_CODE,
                           BILL_TOTAL_PRICE,
                           BILL_TOTAL_QTY,
                           BILL_DATETIME,
                           EMP_CODE,
                           BILL_PAY,
                           V_CODE
                       )
                       VALUES (
                           2,
                           500,
                           4,
                           '2023-10-16 08:00:00',
                           2,
                           500,
                           4
                       );

INSERT INTO BILL_STOCK (
                           BILL_CODE,
                           BILL_TOTAL_PRICE,
                           BILL_TOTAL_QTY,
                           BILL_DATETIME,
                           EMP_CODE,
                           BILL_PAY,
                           V_CODE
                       )
                       VALUES (
                           3,
                           500,
                           6,
                           '2023-10-23 08:00:00',
                           3,
                           500,
                           1
                       );

INSERT INTO BILL_STOCK (
                           BILL_CODE,
                           BILL_TOTAL_PRICE,
                           BILL_TOTAL_QTY,
                           BILL_DATETIME,
                           EMP_CODE,
                           BILL_PAY,
                           V_CODE
                       )
                       VALUES (
                           4,
                           500,
                           3,
                           '2023-10-30 08:00:00',
                           4,
                           500,
                           2
                       );

INSERT INTO BILL_STOCK (
                           BILL_CODE,
                           BILL_TOTAL_PRICE,
                           BILL_TOTAL_QTY,
                           BILL_DATETIME,
                           EMP_CODE,
                           BILL_PAY,
                           V_CODE
                       )
                       VALUES (
                           5,
                           500,
                           5,
                           '2023-11-06 08:00:00',
                           5,
                           500,
                           1
                       );

INSERT INTO BILL_STOCK (
                           BILL_CODE,
                           BILL_TOTAL_PRICE,
                           BILL_TOTAL_QTY,
                           BILL_DATETIME,
                           EMP_CODE,
                           BILL_PAY,
                           V_CODE
                       )
                       VALUES (
                           6,
                           500,
                           4,
                           '2023-11-13 08:00:00',
                           1,
                           500,
                           3
                       );

INSERT INTO BILL_STOCK (
                           BILL_CODE,
                           BILL_TOTAL_PRICE,
                           BILL_TOTAL_QTY,
                           BILL_DATETIME,
                           EMP_CODE,
                           BILL_PAY,
                           V_CODE
                       )
                       VALUES (
                           7,
                           500,
                           6,
                           '2023-11-20 08:00:00',
                           2,
                           500,
                           3
                       );

INSERT INTO BILL_STOCK (
                           BILL_CODE,
                           BILL_TOTAL_PRICE,
                           BILL_TOTAL_QTY,
                           BILL_DATETIME,
                           EMP_CODE,
                           BILL_PAY,
                           V_CODE
                       )
                       VALUES (
                           8,
                           500,
                           3,
                           '2023-11-27 08:00:00',
                           3,
                           500,
                           1
                       );

INSERT INTO BILL_STOCK (
                           BILL_CODE,
                           BILL_TOTAL_PRICE,
                           BILL_TOTAL_QTY,
                           BILL_DATETIME,
                           EMP_CODE,
                           BILL_PAY,
                           V_CODE
                       )
                       VALUES (
                           9,
                           500,
                           5,
                           '2023-12-04 08:00:00',
                           4,
                           500,
                           1
                       );

INSERT INTO BILL_STOCK (
                           BILL_CODE,
                           BILL_TOTAL_PRICE,
                           BILL_TOTAL_QTY,
                           BILL_DATETIME,
                           EMP_CODE,
                           BILL_PAY,
                           V_CODE
                       )
                       VALUES (
                           10,
                           500,
                           4,
                           '2023-12-11 08:00:00',
                           5,
                           500,
                           1
                       );

INSERT INTO BILL_STOCK (
                           BILL_CODE,
                           BILL_TOTAL_PRICE,
                           BILL_TOTAL_QTY,
                           BILL_DATETIME,
                           EMP_CODE,
                           BILL_PAY,
                           V_CODE
                       )
                       VALUES (
                           11,
                           500,
                           6,
                           '2023-12-18 08:00:00',
                           1,
                           500,
                           4
                       );

INSERT INTO BILL_STOCK (
                           BILL_CODE,
                           BILL_TOTAL_PRICE,
                           BILL_TOTAL_QTY,
                           BILL_DATETIME,
                           EMP_CODE,
                           BILL_PAY,
                           V_CODE
                       )
                       VALUES (
                           12,
                           500,
                           3,
                           '2023-12-25 08:00:00',
                           2,
                           500,
                           3
                       );

INSERT INTO BILL_STOCK (
                           BILL_CODE,
                           BILL_TOTAL_PRICE,
                           BILL_TOTAL_QTY,
                           BILL_DATETIME,
                           EMP_CODE,
                           BILL_PAY,
                           V_CODE
                       )
                       VALUES (
                           13,
                           500,
                           5,
                           '2024-01-01 08:00:00',
                           3,
                           500,
                           2
                       );

INSERT INTO BILL_STOCK (
                           BILL_CODE,
                           BILL_TOTAL_PRICE,
                           BILL_TOTAL_QTY,
                           BILL_DATETIME,
                           EMP_CODE,
                           BILL_PAY,
                           V_CODE
                       )
                       VALUES (
                           14,
                           500,
                           4,
                           '2024-01-08 08:00:00',
                           4,
                           500,
                           4
                       );

INSERT INTO BILL_STOCK (
                           BILL_CODE,
                           BILL_TOTAL_PRICE,
                           BILL_TOTAL_QTY,
                           BILL_DATETIME,
                           EMP_CODE,
                           BILL_PAY,
                           V_CODE
                       )
                       VALUES (
                           15,
                           500,
                           3,
                           '2024-01-15 08:00:00',
                           5,
                           500,
                           3
                       );

INSERT INTO BILL_STOCK (
                           BILL_CODE,
                           BILL_TOTAL_PRICE,
                           BILL_TOTAL_QTY,
                           BILL_DATETIME,
                           EMP_CODE,
                           BILL_PAY,
                           V_CODE
                       )
                       VALUES (
                           16,
                           500,
                           5,
                           '2024-01-22 08:00:00',
                           1,
                           500,
                           2
                       );

INSERT INTO BILL_STOCK (
                           BILL_CODE,
                           BILL_TOTAL_PRICE,
                           BILL_TOTAL_QTY,
                           BILL_DATETIME,
                           EMP_CODE,
                           BILL_PAY,
                           V_CODE
                       )
                       VALUES (
                           17,
                           500,
                           6,
                           '2024-01-29 08:00:00',
                           2,
                           500,
                           3
                       );

INSERT INTO BILL_STOCK (
                           BILL_CODE,
                           BILL_TOTAL_PRICE,
                           BILL_TOTAL_QTY,
                           BILL_DATETIME,
                           EMP_CODE,
                           BILL_PAY,
                           V_CODE
                       )
                       VALUES (
                           18,
                           500,
                           4,
                           '2024-02-05 08:00:00',
                           3,
                           500,
                           2
                       );

INSERT INTO BILL_STOCK (
                           BILL_CODE,
                           BILL_TOTAL_PRICE,
                           BILL_TOTAL_QTY,
                           BILL_DATETIME,
                           EMP_CODE,
                           BILL_PAY,
                           V_CODE
                       )
                       VALUES (
                           19,
                           500,
                           5,
                           '2024-02-12 08:00:00',
                           4,
                           500,
                           1
                       );

INSERT INTO BILL_STOCK (
                           BILL_CODE,
                           BILL_TOTAL_PRICE,
                           BILL_TOTAL_QTY,
                           BILL_DATETIME,
                           EMP_CODE,
                           BILL_PAY,
                           V_CODE
                       )
                       VALUES (
                           20,
                           500,
                           3,
                           '2024-02-19 08:00:00',
                           5,
                           500,
                           4
                       );

INSERT INTO BILL_STOCK (
                           BILL_CODE,
                           BILL_TOTAL_PRICE,
                           BILL_TOTAL_QTY,
                           BILL_DATETIME,
                           EMP_CODE,
                           BILL_PAY,
                           V_CODE
                       )
                       VALUES (
                           21,
                           500,
                           4,
                           '2024-02-26 08:00:00',
                           1,
                           500,
                           2
                       );

INSERT INTO BILL_STOCK (
                           BILL_CODE,
                           BILL_TOTAL_PRICE,
                           BILL_TOTAL_QTY,
                           BILL_DATETIME,
                           EMP_CODE,
                           BILL_PAY,
                           V_CODE
                       )
                       VALUES (
                           22,
                           500,
                           6,
                           '2024-03-04 08:00:00',
                           2,
                           500,
                           3
                       );

INSERT INTO BILL_STOCK (
                           BILL_CODE,
                           BILL_TOTAL_PRICE,
                           BILL_TOTAL_QTY,
                           BILL_DATETIME,
                           EMP_CODE,
                           BILL_PAY,
                           V_CODE
                       )
                       VALUES (
                           23,
                           500,
                           3,
                           '2024-03-11 08:00:00',
                           3,
                           500,
                           1
                       );

INSERT INTO BILL_STOCK (
                           BILL_CODE,
                           BILL_TOTAL_PRICE,
                           BILL_TOTAL_QTY,
                           BILL_DATETIME,
                           EMP_CODE,
                           BILL_PAY,
                           V_CODE
                       )
                       VALUES (
                           24,
                           500,
                           5,
                           '2024-03-18 08:00:00',
                           4,
                           500,
                           2
                       );

INSERT INTO BILL_STOCK (
                           BILL_CODE,
                           BILL_TOTAL_PRICE,
                           BILL_TOTAL_QTY,
                           BILL_DATETIME,
                           EMP_CODE,
                           BILL_PAY,
                           V_CODE
                       )
                       VALUES (
                           25,
                           500,
                           4,
                           '2024-03-25 08:00:00',
                           5,
                           500,
                           1
                       );

INSERT INTO BILL_STOCK (
                           BILL_CODE,
                           BILL_TOTAL_PRICE,
                           BILL_TOTAL_QTY,
                           BILL_DATETIME,
                           EMP_CODE,
                           BILL_PAY,
                           V_CODE
                       )
                       VALUES (
                           26,
                           500,
                           6,
                           '2024-04-01 08:00:00',
                           1,
                           500,
                           2
                       );

INSERT INTO BILL_STOCK (
                           BILL_CODE,
                           BILL_TOTAL_PRICE,
                           BILL_TOTAL_QTY,
                           BILL_DATETIME,
                           EMP_CODE,
                           BILL_PAY,
                           V_CODE
                       )
                       VALUES (
                           27,
                           500,
                           4,
                           '2024-04-08 08:00:00',
                           2,
                           500,
                           4
                       );

INSERT INTO BILL_STOCK (
                           BILL_CODE,
                           BILL_TOTAL_PRICE,
                           BILL_TOTAL_QTY,
                           BILL_DATETIME,
                           EMP_CODE,
                           BILL_PAY,
                           V_CODE
                       )
                       VALUES (
                           28,
                           500,
                           5,
                           '2024-04-15 08:00:00',
                           3,
                           500,
                           1
                       );

INSERT INTO BILL_STOCK (
                           BILL_CODE,
                           BILL_TOTAL_PRICE,
                           BILL_TOTAL_QTY,
                           BILL_DATETIME,
                           EMP_CODE,
                           BILL_PAY,
                           V_CODE
                       )
                       VALUES (
                           29,
                           500,
                           6,
                           '2024-04-22 08:00:00',
                           4,
                           500,
                           2
                       );

INSERT INTO BILL_STOCK (
                           BILL_CODE,
                           BILL_TOTAL_PRICE,
                           BILL_TOTAL_QTY,
                           BILL_DATETIME,
                           EMP_CODE,
                           BILL_PAY,
                           V_CODE
                       )
                       VALUES (
                           30,
                           500,
                           3,
                           '2024-04-29 08:00:00',
                           5,
                           500,
                           3
                       );

INSERT INTO BILL_STOCK (
                           BILL_CODE,
                           BILL_TOTAL_PRICE,
                           BILL_TOTAL_QTY,
                           BILL_DATETIME,
                           EMP_CODE,
                           BILL_PAY,
                           V_CODE
                       )
                       VALUES (
                           31,
                           500,
                           5,
                           '2024-05-06 08:00:00',
                           1,
                           500,
                           1
                       );

INSERT INTO BILL_STOCK (
                           BILL_CODE,
                           BILL_TOTAL_PRICE,
                           BILL_TOTAL_QTY,
                           BILL_DATETIME,
                           EMP_CODE,
                           BILL_PAY,
                           V_CODE
                       )
                       VALUES (
                           32,
                           500,
                           4,
                           '2024-05-13 08:00:00',
                           2,
                           500,
                           3
                       );

INSERT INTO BILL_STOCK (
                           BILL_CODE,
                           BILL_TOTAL_PRICE,
                           BILL_TOTAL_QTY,
                           BILL_DATETIME,
                           EMP_CODE,
                           BILL_PAY,
                           V_CODE
                       )
                       VALUES (
                           33,
                           500,
                           3,
                           '2024-05-20 08:00:00',
                           3,
                           500,
                           1
                       );

INSERT INTO BILL_STOCK (
                           BILL_CODE,
                           BILL_TOTAL_PRICE,
                           BILL_TOTAL_QTY,
                           BILL_DATETIME,
                           EMP_CODE,
                           BILL_PAY,
                           V_CODE
                       )
                       VALUES (
                           34,
                           500,
                           6,
                           '2024-05-27 08:00:00',
                           4,
                           500,
                           1
                       );

INSERT INTO BILL_STOCK (
                           BILL_CODE,
                           BILL_TOTAL_PRICE,
                           BILL_TOTAL_QTY,
                           BILL_DATETIME,
                           EMP_CODE,
                           BILL_PAY,
                           V_CODE
                       )
                       VALUES (
                           35,
                           500,
                           4,
                           '2024-06-03 08:00:00',
                           5,
                           500,
                           2
                       );


-- Table: CATEGORY
DROP TABLE IF EXISTS CATEGORY;

CREATE TABLE IF NOT EXISTS CATEGORY (
    CAT_CODE INTEGER   PRIMARY KEY ASC AUTOINCREMENT,
    CAT_NAME TEXT (50) NOT NULL
);

INSERT INTO CATEGORY (
                         CAT_CODE,
                         CAT_NAME
                     )
                     VALUES (
                         1,
                         'Coffee'
                     );

INSERT INTO CATEGORY (
                         CAT_CODE,
                         CAT_NAME
                     )
                     VALUES (
                         2,
                         'Bakery'
                     );

INSERT INTO CATEGORY (
                         CAT_CODE,
                         CAT_NAME
                     )
                     VALUES (
                         3,
                         'Food'
                     );


-- Table: CHECK_STOCK
DROP TABLE IF EXISTS CHECK_STOCK;

CREATE TABLE IF NOT EXISTS CHECK_STOCK (
    CHECK_CODE     INTEGER  PRIMARY KEY ASC AUTOINCREMENT
                            UNIQUE,
    EMP_CODE       INTEGER  NOT NULL
                            REFERENCES EMPLOYEE (EMP_CODE) ON DELETE CASCADE
                                                           ON UPDATE CASCADE,
    CHECK_DATETIME DATETIME NOT NULL
                            DEFAULT ( (datetime('now', 'localtime') ) ),
    FOREIGN KEY (
        EMP_CODE
    )
    REFERENCES EMPLOYEE (EMP_CODE) 
);

INSERT INTO CHECK_STOCK (
                            CHECK_CODE,
                            EMP_CODE,
                            CHECK_DATETIME
                        )
                        VALUES (
                            1,
                            2,
                            '2023-01-01 08:00:00'
                        );

INSERT INTO CHECK_STOCK (
                            CHECK_CODE,
                            EMP_CODE,
                            CHECK_DATETIME
                        )
                        VALUES (
                            2,
                            3,
                            '2023-01-02 09:15:00'
                        );

INSERT INTO CHECK_STOCK (
                            CHECK_CODE,
                            EMP_CODE,
                            CHECK_DATETIME
                        )
                        VALUES (
                            3,
                            4,
                            '2023-01-03 10:30:00'
                        );

INSERT INTO CHECK_STOCK (
                            CHECK_CODE,
                            EMP_CODE,
                            CHECK_DATETIME
                        )
                        VALUES (
                            4,
                            5,
                            '2023-01-04 11:45:00'
                        );

INSERT INTO CHECK_STOCK (
                            CHECK_CODE,
                            EMP_CODE,
                            CHECK_DATETIME
                        )
                        VALUES (
                            5,
                            6,
                            '2023-01-05 12:00:00'
                        );

INSERT INTO CHECK_STOCK (
                            CHECK_CODE,
                            EMP_CODE,
                            CHECK_DATETIME
                        )
                        VALUES (
                            6,
                            7,
                            '2023-02-01 08:00:00'
                        );

INSERT INTO CHECK_STOCK (
                            CHECK_CODE,
                            EMP_CODE,
                            CHECK_DATETIME
                        )
                        VALUES (
                            7,
                            8,
                            '2023-02-02 09:15:00'
                        );

INSERT INTO CHECK_STOCK (
                            CHECK_CODE,
                            EMP_CODE,
                            CHECK_DATETIME
                        )
                        VALUES (
                            8,
                            9,
                            '2023-02-03 10:30:00'
                        );

INSERT INTO CHECK_STOCK (
                            CHECK_CODE,
                            EMP_CODE,
                            CHECK_DATETIME
                        )
                        VALUES (
                            9,
                            2,
                            '2023-02-04 11:45:00'
                        );

INSERT INTO CHECK_STOCK (
                            CHECK_CODE,
                            EMP_CODE,
                            CHECK_DATETIME
                        )
                        VALUES (
                            10,
                            3,
                            '2023-02-05 12:00:00'
                        );

INSERT INTO CHECK_STOCK (
                            CHECK_CODE,
                            EMP_CODE,
                            CHECK_DATETIME
                        )
                        VALUES (
                            11,
                            4,
                            '2023-03-01 08:00:00'
                        );

INSERT INTO CHECK_STOCK (
                            CHECK_CODE,
                            EMP_CODE,
                            CHECK_DATETIME
                        )
                        VALUES (
                            12,
                            5,
                            '2023-03-02 09:15:00'
                        );

INSERT INTO CHECK_STOCK (
                            CHECK_CODE,
                            EMP_CODE,
                            CHECK_DATETIME
                        )
                        VALUES (
                            13,
                            6,
                            '2023-03-03 10:30:00'
                        );

INSERT INTO CHECK_STOCK (
                            CHECK_CODE,
                            EMP_CODE,
                            CHECK_DATETIME
                        )
                        VALUES (
                            14,
                            7,
                            '2023-03-04 11:45:00'
                        );

INSERT INTO CHECK_STOCK (
                            CHECK_CODE,
                            EMP_CODE,
                            CHECK_DATETIME
                        )
                        VALUES (
                            15,
                            8,
                            '2023-03-05 12:00:00'
                        );

INSERT INTO CHECK_STOCK (
                            CHECK_CODE,
                            EMP_CODE,
                            CHECK_DATETIME
                        )
                        VALUES (
                            16,
                            9,
                            '2023-04-01 08:00:00'
                        );

INSERT INTO CHECK_STOCK (
                            CHECK_CODE,
                            EMP_CODE,
                            CHECK_DATETIME
                        )
                        VALUES (
                            17,
                            2,
                            '2023-04-02 09:15:00'
                        );

INSERT INTO CHECK_STOCK (
                            CHECK_CODE,
                            EMP_CODE,
                            CHECK_DATETIME
                        )
                        VALUES (
                            18,
                            3,
                            '2023-04-03 10:30:00'
                        );

INSERT INTO CHECK_STOCK (
                            CHECK_CODE,
                            EMP_CODE,
                            CHECK_DATETIME
                        )
                        VALUES (
                            19,
                            4,
                            '2023-04-04 11:45:00'
                        );

INSERT INTO CHECK_STOCK (
                            CHECK_CODE,
                            EMP_CODE,
                            CHECK_DATETIME
                        )
                        VALUES (
                            20,
                            5,
                            '2023-04-05 12:00:00'
                        );


-- Table: CUSTOMER
DROP TABLE IF EXISTS CUSTOMER;

CREATE TABLE IF NOT EXISTS CUSTOMER (
    CUS_CODE    INTEGER        PRIMARY KEY AUTOINCREMENT
                               UNIQUE,
    CUS_FNAME   VARCHAR (20)   NOT NULL,
    CUS_LNAME   VARCHAR (20)   NOT NULL,
    CUS_EMAIL   VARCHAR (30)   NOT NULL,
    CUS_TEL     CHAR (10)      NOT NULL,
    CUS_ADD     VARCHAR (100)  NOT NULL,
    CUS_GEN     VARCHAR (6)    NOT NULL,
    CUS_INDATE  DATE           NOT NULL
                               DEFAULT (date(datetime('now', 'localtime') ) ),
    CUS_P_VALUE NUMERIC (5, 2) CHECK (CUS_P_VALUE >= 0.0 AND 
                                      CUS_P_VALUE <= 999.99) 
                               NOT NULL,
    CUS_P_RATE  NUMERIC (5, 2) CHECK (CUS_P_RATE >= 0.0 AND 
                                      CUS_P_RATE <= 999.99) 
                               NOT NULL
);

INSERT INTO CUSTOMER (
                         CUS_CODE,
                         CUS_FNAME,
                         CUS_LNAME,
                         CUS_EMAIL,
                         CUS_TEL,
                         CUS_ADD,
                         CUS_GEN,
                         CUS_INDATE,
                         CUS_P_VALUE,
                         CUS_P_RATE
                     )
                     VALUES (
                         1,
                         'John',
                         'Doe',
                         'john.doe@example.com',
                         '123-456-7890',
                         '123 Main St',
                         'Male',
                         '2023-10-09 08:00:00',
                         150,
                         10
                     );

INSERT INTO CUSTOMER (
                         CUS_CODE,
                         CUS_FNAME,
                         CUS_LNAME,
                         CUS_EMAIL,
                         CUS_TEL,
                         CUS_ADD,
                         CUS_GEN,
                         CUS_INDATE,
                         CUS_P_VALUE,
                         CUS_P_RATE
                     )
                     VALUES (
                         2,
                         'Jane',
                         'Smith',
                         'jane.smith@example.com',
                         '987-654-3210',
                         '456 Elm St',
                         'Female',
                         '2023-10-09 09:15:00',
                         175,
                         10
                     );

INSERT INTO CUSTOMER (
                         CUS_CODE,
                         CUS_FNAME,
                         CUS_LNAME,
                         CUS_EMAIL,
                         CUS_TEL,
                         CUS_ADD,
                         CUS_GEN,
                         CUS_INDATE,
                         CUS_P_VALUE,
                         CUS_P_RATE
                     )
                     VALUES (
                         3,
                         'Alice',
                         'Johnson',
                         'alice.johnson@example.com',
                         '555-123-4567',
                         '789 Oak St',
                         'Female',
                         '2023-10-09 10:30:00',
                         180,
                         10
                     );

INSERT INTO CUSTOMER (
                         CUS_CODE,
                         CUS_FNAME,
                         CUS_LNAME,
                         CUS_EMAIL,
                         CUS_TEL,
                         CUS_ADD,
                         CUS_GEN,
                         CUS_INDATE,
                         CUS_P_VALUE,
                         CUS_P_RATE
                     )
                     VALUES (
                         4,
                         'Bob',
                         'Williams',
                         'bob.williams@example.com',
                         '444-777-8888',
                         '321 Pine St',
                         'Male',
                         '2023-10-09 11:45:00',
                         160,
                         10
                     );

INSERT INTO CUSTOMER (
                         CUS_CODE,
                         CUS_FNAME,
                         CUS_LNAME,
                         CUS_EMAIL,
                         CUS_TEL,
                         CUS_ADD,
                         CUS_GEN,
                         CUS_INDATE,
                         CUS_P_VALUE,
                         CUS_P_RATE
                     )
                     VALUES (
                         5,
                         'Charlie',
                         'Brown',
                         'charlie.brown@example.com',
                         '999-555-4444',
                         '654 Cedar St',
                         'Male',
                         '2023-10-09 12:00:00',
                         190,
                         10
                     );

INSERT INTO CUSTOMER (
                         CUS_CODE,
                         CUS_FNAME,
                         CUS_LNAME,
                         CUS_EMAIL,
                         CUS_TEL,
                         CUS_ADD,
                         CUS_GEN,
                         CUS_INDATE,
                         CUS_P_VALUE,
                         CUS_P_RATE
                     )
                     VALUES (
                         6,
                         'Eva',
                         'Harris',
                         'eva.harris@example.com',
                         '111-222-3333',
                         '987 Maple St',
                         'Female',
                         '2023-10-10 08:00:00',
                         140,
                         10
                     );

INSERT INTO CUSTOMER (
                         CUS_CODE,
                         CUS_FNAME,
                         CUS_LNAME,
                         CUS_EMAIL,
                         CUS_TEL,
                         CUS_ADD,
                         CUS_GEN,
                         CUS_INDATE,
                         CUS_P_VALUE,
                         CUS_P_RATE
                     )
                     VALUES (
                         7,
                         'Grace',
                         'Lee',
                         'grace.lee@example.com',
                         '666-999-8888',
                         '456 Birch St',
                         'Female',
                         '2023-10-10 09:15:00',
                         130,
                         10
                     );

INSERT INTO CUSTOMER (
                         CUS_CODE,
                         CUS_FNAME,
                         CUS_LNAME,
                         CUS_EMAIL,
                         CUS_TEL,
                         CUS_ADD,
                         CUS_GEN,
                         CUS_INDATE,
                         CUS_P_VALUE,
                         CUS_P_RATE
                     )
                     VALUES (
                         8,
                         'David',
                         'Johnson',
                         'david.johnson@example.com',
                         '333-444-5555',
                         '789 Walnut St',
                         'Male',
                         '2023-10-10 10:30:00',
                         155,
                         10
                     );

INSERT INTO CUSTOMER (
                         CUS_CODE,
                         CUS_FNAME,
                         CUS_LNAME,
                         CUS_EMAIL,
                         CUS_TEL,
                         CUS_ADD,
                         CUS_GEN,
                         CUS_INDATE,
                         CUS_P_VALUE,
                         CUS_P_RATE
                     )
                     VALUES (
                         9,
                         'Frank',
                         'White',
                         'frank.white@example.com',
                         '777-888-1111',
                         '222 Oak St',
                         'Male',
                         '2023-10-10 11:45:00',
                         145,
                         10
                     );

INSERT INTO CUSTOMER (
                         CUS_CODE,
                         CUS_FNAME,
                         CUS_LNAME,
                         CUS_EMAIL,
                         CUS_TEL,
                         CUS_ADD,
                         CUS_GEN,
                         CUS_INDATE,
                         CUS_P_VALUE,
                         CUS_P_RATE
                     )
                     VALUES (
                         10,
                         'Helen',
                         'Davis',
                         'helen.davis@example.com',
                         '222-555-6666',
                         '111 Cedar St',
                         'Female',
                         '2023-10-10 12:00:00',
                         170,
                         10
                     );


-- Table: EMPLOYEE
DROP TABLE IF EXISTS EMPLOYEE;

CREATE TABLE IF NOT EXISTS EMPLOYEE (
    EMP_CODE     INTEGER       PRIMARY KEY ASC AUTOINCREMENT
                               UNIQUE,
    EMP_FNAME    VARCHAR (20)  NOT NULL,
    EMP_LNAME    VARCHAR (20)  NOT NULL,
    EMP_EMAIL    VARCHAR (30)  NOT NULL,
    EMP_TEL      CHAR (10)     NOT NULL,
    EMP_INDATE   DATE          NOT NULL
                               DEFAULT ( (datetime('now', 'localtime') ) ),
    EMP_ADDRESS  VARCHAR (100) NOT NULL,
    EMP_RANK     VARCHAR (10)  NOT NULL,
    EMP_STATUS   VARCHAR (10)  NOT NULL,
    EMP_PASS     TEXT (20)     NOT NULL
                               UNIQUE,
    EMP_USERNAME TEXT (20)     UNIQUE
                               NOT NULL
);

INSERT INTO EMPLOYEE (
                         EMP_CODE,
                         EMP_FNAME,
                         EMP_LNAME,
                         EMP_EMAIL,
                         EMP_TEL,
                         EMP_INDATE,
                         EMP_ADDRESS,
                         EMP_RANK,
                         EMP_STATUS,
                         EMP_PASS,
                         EMP_USERNAME
                     )
                     VALUES (
                         1,
                         'John',
                         'Doe',
                         'john.doe@example.com',
                         '123-456-7890',
                         '2023-10-09',
                         '123 Main St',
                         'Manager',
                         'Active',
                         'password1',
                         'user1'
                     );

INSERT INTO EMPLOYEE (
                         EMP_CODE,
                         EMP_FNAME,
                         EMP_LNAME,
                         EMP_EMAIL,
                         EMP_TEL,
                         EMP_INDATE,
                         EMP_ADDRESS,
                         EMP_RANK,
                         EMP_STATUS,
                         EMP_PASS,
                         EMP_USERNAME
                     )
                     VALUES (
                         2,
                         'Jane',
                         'Smith',
                         'jane.smith@example.com',
                         '987-654-3210',
                         '2023-10-09',
                         '456 Elm St',
                         'Employee',
                         'Active',
                         'password2',
                         'user2'
                     );

INSERT INTO EMPLOYEE (
                         EMP_CODE,
                         EMP_FNAME,
                         EMP_LNAME,
                         EMP_EMAIL,
                         EMP_TEL,
                         EMP_INDATE,
                         EMP_ADDRESS,
                         EMP_RANK,
                         EMP_STATUS,
                         EMP_PASS,
                         EMP_USERNAME
                     )
                     VALUES (
                         3,
                         'Mock',
                         'Employee1',
                         'mock1@example.com',
                         '111-222-3333',
                         '2023-10-09',
                         '789 Oak St',
                         'Employee',
                         'Active',
                         'password3',
                         'user3'
                     );

INSERT INTO EMPLOYEE (
                         EMP_CODE,
                         EMP_FNAME,
                         EMP_LNAME,
                         EMP_EMAIL,
                         EMP_TEL,
                         EMP_INDATE,
                         EMP_ADDRESS,
                         EMP_RANK,
                         EMP_STATUS,
                         EMP_PASS,
                         EMP_USERNAME
                     )
                     VALUES (
                         4,
                         'Mock',
                         'Employee2',
                         'mock2@example.com',
                         '444-555-6666',
                         '2023-10-09',
                         '987 Pine St',
                         'Employee',
                         'Active',
                         'password4',
                         'user4'
                     );

INSERT INTO EMPLOYEE (
                         EMP_CODE,
                         EMP_FNAME,
                         EMP_LNAME,
                         EMP_EMAIL,
                         EMP_TEL,
                         EMP_INDATE,
                         EMP_ADDRESS,
                         EMP_RANK,
                         EMP_STATUS,
                         EMP_PASS,
                         EMP_USERNAME
                     )
                     VALUES (
                         5,
                         'Mock',
                         'Employee3',
                         'mock3@example.com',
                         '777-888-9999',
                         '2023-10-09',
                         '654 Cedar St',
                         'Employee',
                         'Active',
                         'password5',
                         'user5'
                     );

INSERT INTO EMPLOYEE (
                         EMP_CODE,
                         EMP_FNAME,
                         EMP_LNAME,
                         EMP_EMAIL,
                         EMP_TEL,
                         EMP_INDATE,
                         EMP_ADDRESS,
                         EMP_RANK,
                         EMP_STATUS,
                         EMP_PASS,
                         EMP_USERNAME
                     )
                     VALUES (
                         6,
                         'Mock',
                         'Employee4',
                         'mock4@example.com',
                         '333-444-5555',
                         '2023-10-09',
                         '321 Birch St',
                         'Employee',
                         'Active',
                         'password6',
                         'user6'
                     );

INSERT INTO EMPLOYEE (
                         EMP_CODE,
                         EMP_FNAME,
                         EMP_LNAME,
                         EMP_EMAIL,
                         EMP_TEL,
                         EMP_INDATE,
                         EMP_ADDRESS,
                         EMP_RANK,
                         EMP_STATUS,
                         EMP_PASS,
                         EMP_USERNAME
                     )
                     VALUES (
                         7,
                         'Mock',
                         'Employee5',
                         'mock5@example.com',
                         '666-777-8888',
                         '2023-10-09',
                         '222 Maple St',
                         'Employee',
                         'Active',
                         'password7',
                         'user7'
                     );

INSERT INTO EMPLOYEE (
                         EMP_CODE,
                         EMP_FNAME,
                         EMP_LNAME,
                         EMP_EMAIL,
                         EMP_TEL,
                         EMP_INDATE,
                         EMP_ADDRESS,
                         EMP_RANK,
                         EMP_STATUS,
                         EMP_PASS,
                         EMP_USERNAME
                     )
                     VALUES (
                         8,
                         'Mock',
                         'Employee6',
                         'mock6@example.com',
                         '999-111-2222',
                         '2023-10-09',
                         '888 Walnut St',
                         'Employee',
                         'Active',
                         'password8',
                         'user8'
                     );

INSERT INTO EMPLOYEE (
                         EMP_CODE,
                         EMP_FNAME,
                         EMP_LNAME,
                         EMP_EMAIL,
                         EMP_TEL,
                         EMP_INDATE,
                         EMP_ADDRESS,
                         EMP_RANK,
                         EMP_STATUS,
                         EMP_PASS,
                         EMP_USERNAME
                     )
                     VALUES (
                         9,
                         'Mock',
                         'Employee7',
                         'mock7@example.com',
                         '222-333-4444',
                         '2023-10-09',
                         '555 Oak St',
                         'Employee',
                         'Active',
                         'password9',
                         'user9'
                     );

INSERT INTO EMPLOYEE (
                         EMP_CODE,
                         EMP_FNAME,
                         EMP_LNAME,
                         EMP_EMAIL,
                         EMP_TEL,
                         EMP_INDATE,
                         EMP_ADDRESS,
                         EMP_RANK,
                         EMP_STATUS,
                         EMP_PASS,
                         EMP_USERNAME
                     )
                     VALUES (
                         10,
                         'Mock',
                         'Employee8',
                         'mock8@example.com',
                         '555-666-7777',
                         '2023-10-09',
                         '111 Pine St',
                         'Employee',
                         'Active',
                         'password10',
                         'user10'
                     );


-- Table: ENTRY
DROP TABLE IF EXISTS ENTRY;

CREATE TABLE IF NOT EXISTS ENTRY (
    ENT_CODE     INTEGER        PRIMARY KEY ASC AUTOINCREMENT
                                UNIQUE,
    EMP_CODE     INTEGER        NOT NULL
                                REFERENCES EMPLOYEE (EMP_CODE) ON DELETE CASCADE
                                                               ON UPDATE CASCADE,
    ENT_DATE     DATETIME       NOT NULL
                                DEFAULT (date(datetime('now', 'localtime') ) ),
    ENT_TIME_IN  DATETIME       NOT NULL
                                DEFAULT (time(datetime('now', 'localtime') ) ),
    ENT_TIME_OUT DATETIME       NOT NULL
                                DEFAULT (time(datetime('now', 'localtime') ) ),
    ENT_STATUS   VARCHAR (10)   NOT NULL
                                DEFAULT [present,absent],
    ENT_TOTAL_HR NUMERIC (6, 2) CHECK (ENT_TOTAL_HR >= 0.0 AND 
                                       ENT_TOTAL_HR <= 999.99) 
                                NOT NULL,
    FOREIGN KEY (
        EMP_CODE
    )
    REFERENCES EMPLOYEE (EMP_CODE) 
);

INSERT INTO ENTRY (
                      ENT_CODE,
                      EMP_CODE,
                      ENT_DATE,
                      ENT_TIME_IN,
                      ENT_TIME_OUT,
                      ENT_STATUS,
                      ENT_TOTAL_HR
                  )
                  VALUES (
                      1,
                      1,
                      '2023-10-01 13:42:52',
                      '2023-10-01 08:42:52',
                      '2023-10-01 16:42:52',
                      'Present',
                      15
                  );

INSERT INTO ENTRY (
                      ENT_CODE,
                      EMP_CODE,
                      ENT_DATE,
                      ENT_TIME_IN,
                      ENT_TIME_OUT,
                      ENT_STATUS,
                      ENT_TOTAL_HR
                  )
                  VALUES (
                      2,
                      1,
                      '2023-10-01 13:42:52',
                      '2023-10-01 08:42:52',
                      '2023-10-01 16:42:52',
                      'Present',
                      20
                  );

INSERT INTO ENTRY (
                      ENT_CODE,
                      EMP_CODE,
                      ENT_DATE,
                      ENT_TIME_IN,
                      ENT_TIME_OUT,
                      ENT_STATUS,
                      ENT_TOTAL_HR
                  )
                  VALUES (
                      3,
                      2,
                      '2023-10-01 13:42:52',
                      '2023-10-01 08:42:52',
                      '2023-10-01 16:42:52',
                      'Present',
                      15
                  );

INSERT INTO ENTRY (
                      ENT_CODE,
                      EMP_CODE,
                      ENT_DATE,
                      ENT_TIME_IN,
                      ENT_TIME_OUT,
                      ENT_STATUS,
                      ENT_TOTAL_HR
                  )
                  VALUES (
                      4,
                      3,
                      '2023-10-01 13:42:52',
                      '2023-10-01 08:42:52',
                      '2023-10-01 16:42:52',
                      'Present',
                      2
                  );

INSERT INTO ENTRY (
                      ENT_CODE,
                      EMP_CODE,
                      ENT_DATE,
                      ENT_TIME_IN,
                      ENT_TIME_OUT,
                      ENT_STATUS,
                      ENT_TOTAL_HR
                  )
                  VALUES (
                      5,
                      4,
                      '2023-10-01 13:42:52',
                      '2023-10-01 08:42:52',
                      '2023-10-01 16:42:52',
                      'Present',
                      6
                  );

INSERT INTO ENTRY (
                      ENT_CODE,
                      EMP_CODE,
                      ENT_DATE,
                      ENT_TIME_IN,
                      ENT_TIME_OUT,
                      ENT_STATUS,
                      ENT_TOTAL_HR
                  )
                  VALUES (
                      6,
                      5,
                      '2023-10-01 13:42:52',
                      '2023-10-01 08:42:52',
                      '2023-10-01 16:42:52',
                      'Present',
                      9
                  );

INSERT INTO ENTRY (
                      ENT_CODE,
                      EMP_CODE,
                      ENT_DATE,
                      ENT_TIME_IN,
                      ENT_TIME_OUT,
                      ENT_STATUS,
                      ENT_TOTAL_HR
                  )
                  VALUES (
                      7,
                      6,
                      '2023-10-01 13:42:52',
                      '2023-10-01 08:42:52',
                      '2023-10-01 16:42:52',
                      'Present',
                      2
                  );

INSERT INTO ENTRY (
                      ENT_CODE,
                      EMP_CODE,
                      ENT_DATE,
                      ENT_TIME_IN,
                      ENT_TIME_OUT,
                      ENT_STATUS,
                      ENT_TOTAL_HR
                  )
                  VALUES (
                      8,
                      7,
                      '2023-10-01 13:42:52',
                      '2023-10-01 08:42:52',
                      '2023-10-01 16:42:52',
                      'Present',
                      12
                  );

INSERT INTO ENTRY (
                      ENT_CODE,
                      EMP_CODE,
                      ENT_DATE,
                      ENT_TIME_IN,
                      ENT_TIME_OUT,
                      ENT_STATUS,
                      ENT_TOTAL_HR
                  )
                  VALUES (
                      9,
                      8,
                      '2023-10-01 13:42:52',
                      '2023-10-01 08:42:52',
                      '2023-10-01 16:42:52',
                      'Present',
                      45
                  );

INSERT INTO ENTRY (
                      ENT_CODE,
                      EMP_CODE,
                      ENT_DATE,
                      ENT_TIME_IN,
                      ENT_TIME_OUT,
                      ENT_STATUS,
                      ENT_TOTAL_HR
                  )
                  VALUES (
                      10,
                      9,
                      '2023-10-01 13:42:52',
                      '2023-10-01 08:42:52',
                      '2023-10-01 16:42:52',
                      'Present',
                      20
                  );


-- Table: MATERIAL
DROP TABLE IF EXISTS MATERIAL;

CREATE TABLE IF NOT EXISTS MATERIAL (
    M_CODE           INTEGER        PRIMARY KEY ASC AUTOINCREMENT
                                    UNIQUE
                                    NOT NULL,
    M_NAME           VARCHAR (20)   NOT NULL,
    M_QOH            NUMERIC (6, 2) CHECK (M_QOH >= 0.0 AND 
                                           M_QOH <= 9999.0) 
                                    NOT NULL,
    M_PRICE_PER_UNIT NUMERIC (6, 2) CHECK (M_PRICE_PER_UNIT >= 0.0 AND 
                                           M_PRICE_PER_UNIT <= 9999.0) 
                                    NOT NULL,
    M_UNIT           VARCHAR (20)   NOT NULL,
    M_MIN            NUMERIC (6, 2) CHECK (M_MIN >= 0.0 AND 
                                           M_MIN <= 9999.0) 
                                    NOT NULL
);

INSERT INTO MATERIAL (
                         M_CODE,
                         M_NAME,
                         M_QOH,
                         M_PRICE_PER_UNIT,
                         M_UNIT,
                         M_MIN
                     )
                     VALUES (
                         1,
                         'Coco',
                         10,
                         20,
                         'Kilogram',
                         20
                     );

INSERT INTO MATERIAL (
                         M_CODE,
                         M_NAME,
                         M_QOH,
                         M_PRICE_PER_UNIT,
                         M_UNIT,
                         M_MIN
                     )
                     VALUES (
                         2,
                         'Milk',
                         20,
                         20,
                         'Liter',
                         100.5
                     );

INSERT INTO MATERIAL (
                         M_CODE,
                         M_NAME,
                         M_QOH,
                         M_PRICE_PER_UNIT,
                         M_UNIT,
                         M_MIN
                     )
                     VALUES (
                         3,
                         'Sugar',
                         30,
                         20,
                         'Kilogram',
                         20
                     );

INSERT INTO MATERIAL (
                         M_CODE,
                         M_NAME,
                         M_QOH,
                         M_PRICE_PER_UNIT,
                         M_UNIT,
                         M_MIN
                     )
                     VALUES (
                         4,
                         'Coffee Beans',
                         45,
                         50,
                         'Kilogram',
                         10
                     );

INSERT INTO MATERIAL (
                         M_CODE,
                         M_NAME,
                         M_QOH,
                         M_PRICE_PER_UNIT,
                         M_UNIT,
                         M_MIN
                     )
                     VALUES (
                         5,
                         'Tea Leaves',
                         40,
                         50,
                         'Kilogram',
                         10
                     );

INSERT INTO MATERIAL (
                         M_CODE,
                         M_NAME,
                         M_QOH,
                         M_PRICE_PER_UNIT,
                         M_UNIT,
                         M_MIN
                     )
                     VALUES (
                         6,
                         'Sugar',
                         30,
                         50,
                         'Kilogram',
                         10
                     );

INSERT INTO MATERIAL (
                         M_CODE,
                         M_NAME,
                         M_QOH,
                         M_PRICE_PER_UNIT,
                         M_UNIT,
                         M_MIN
                     )
                     VALUES (
                         7,
                         'Milk',
                         25,
                         50,
                         'Liter',
                         10
                     );

INSERT INTO MATERIAL (
                         M_CODE,
                         M_NAME,
                         M_QOH,
                         M_PRICE_PER_UNIT,
                         M_UNIT,
                         M_MIN
                     )
                     VALUES (
                         8,
                         'Espresso Machine',
                         5,
                         50,
                         'Unit',
                         1
                     );

INSERT INTO MATERIAL (
                         M_CODE,
                         M_NAME,
                         M_QOH,
                         M_PRICE_PER_UNIT,
                         M_UNIT,
                         M_MIN
                     )
                     VALUES (
                         9,
                         'Tea Bags',
                         50,
                         50,
                         'Pack',
                         10
                     );

INSERT INTO MATERIAL (
                         M_CODE,
                         M_NAME,
                         M_QOH,
                         M_PRICE_PER_UNIT,
                         M_UNIT,
                         M_MIN
                     )
                     VALUES (
                         10,
                         'Cups',
                         40,
                         50,
                         'Unit',
                         10
                     );

INSERT INTO MATERIAL (
                         M_CODE,
                         M_NAME,
                         M_QOH,
                         M_PRICE_PER_UNIT,
                         M_UNIT,
                         M_MIN
                     )
                     VALUES (
                         11,
                         'Napkins',
                         20,
                         50,
                         'Unit',
                         10
                     );

INSERT INTO MATERIAL (
                         M_CODE,
                         M_NAME,
                         M_QOH,
                         M_PRICE_PER_UNIT,
                         M_UNIT,
                         M_MIN
                     )
                     VALUES (
                         12,
                         'Syrup',
                         15,
                         50,
                         'Liter',
                         10
                     );

INSERT INTO MATERIAL (
                         M_CODE,
                         M_NAME,
                         M_QOH,
                         M_PRICE_PER_UNIT,
                         M_UNIT,
                         M_MIN
                     )
                     VALUES (
                         13,
                         'Coffee Filters',
                         30,
                         50,
                         'Pack',
                         10
                     );

INSERT INTO MATERIAL (
                         M_CODE,
                         M_NAME,
                         M_QOH,
                         M_PRICE_PER_UNIT,
                         M_UNIT,
                         M_MIN
                     )
                     VALUES (
                         14,
                         'Cream',
                         10,
                         80,
                         'Liter',
                         10
                     );

INSERT INTO MATERIAL (
                         M_CODE,
                         M_NAME,
                         M_QOH,
                         M_PRICE_PER_UNIT,
                         M_UNIT,
                         M_MIN
                     )
                     VALUES (
                         15,
                         'Chocolate Powder',
                         20,
                         80,
                         'Kilogram',
                         10
                     );

INSERT INTO MATERIAL (
                         M_CODE,
                         M_NAME,
                         M_QOH,
                         M_PRICE_PER_UNIT,
                         M_UNIT,
                         M_MIN
                     )
                     VALUES (
                         16,
                         'Cinnamon',
                         10,
                         80,
                         'Kilogram',
                         10
                     );

INSERT INTO MATERIAL (
                         M_CODE,
                         M_NAME,
                         M_QOH,
                         M_PRICE_PER_UNIT,
                         M_UNIT,
                         M_MIN
                     )
                     VALUES (
                         17,
                         'Vanilla Extract',
                         10,
                         80,
                         'Liter',
                         10
                     );

INSERT INTO MATERIAL (
                         M_CODE,
                         M_NAME,
                         M_QOH,
                         M_PRICE_PER_UNIT,
                         M_UNIT,
                         M_MIN
                     )
                     VALUES (
                         18,
                         'Tea Infuser',
                         25,
                         80,
                         'Unit',
                         10
                     );

INSERT INTO MATERIAL (
                         M_CODE,
                         M_NAME,
                         M_QOH,
                         M_PRICE_PER_UNIT,
                         M_UNIT,
                         M_MIN
                     )
                     VALUES (
                         19,
                         'Lemon',
                         15,
                         80,
                         'Unit',
                         10
                     );

INSERT INTO MATERIAL (
                         M_CODE,
                         M_NAME,
                         M_QOH,
                         M_PRICE_PER_UNIT,
                         M_UNIT,
                         M_MIN
                     )
                     VALUES (
                         20,
                         'Honey',
                         20,
                         80,
                         'Liter',
                         10
                     );

INSERT INTO MATERIAL (
                         M_CODE,
                         M_NAME,
                         M_QOH,
                         M_PRICE_PER_UNIT,
                         M_UNIT,
                         M_MIN
                     )
                     VALUES (
                         21,
                         'Mocha Syrup',
                         10,
                         80,
                         'Liter',
                         10
                     );

INSERT INTO MATERIAL (
                         M_CODE,
                         M_NAME,
                         M_QOH,
                         M_PRICE_PER_UNIT,
                         M_UNIT,
                         M_MIN
                     )
                     VALUES (
                         22,
                         'Iced Coffee',
                         20,
                         80,
                         'Liter',
                         10
                     );

INSERT INTO MATERIAL (
                         M_CODE,
                         M_NAME,
                         M_QOH,
                         M_PRICE_PER_UNIT,
                         M_UNIT,
                         M_MIN
                     )
                     VALUES (
                         23,
                         'Whipped Cream',
                         10,
                         80,
                         'Liter',
                         10
                     );


-- Table: PAYROLL
DROP TABLE IF EXISTS PAYROLL;

CREATE TABLE IF NOT EXISTS PAYROLL (
    PRL_CODE      INTEGER        PRIMARY KEY ASC AUTOINCREMENT
                                 UNIQUE,
    EMP_CODE      INTEGER        NOT NULL
                                 REFERENCES EMPLOYEE (EMP_CODE) ON DELETE CASCADE
                                                                ON UPDATE CASCADE,
    PRL_STATUS    CHAR (8)       NOT NULL
                                 DEFAULT [PAY,NOTPAY],
    PRL_DATE      DATE           NOT NULL
                                 DEFAULT (datetime('now', 'localtime') ),
    ENT_CODE      CHAR (8)       NOT NULL
                                 REFERENCES ENTRY (ENT_CODE) ON DELETE CASCADE
                                                             ON UPDATE CASCADE,
    PRL_TOTAL_PAY NUMERIC (8, 2) NOT NULL
                                 CHECK (PRL_TOTAL_PAY >= 0.0 AND 
                                        PRL_TOTAL_PAY <= 99999.99),
    PRL_PRICE_HR  NUMERIC (5, 2) CHECK (PRL_PRICE_HR >= 0.0 AND 
                                        PRL_PRICE_HR <= 99.99) 
                                 DEFAULT [25.0]
                                 NOT NULL,
    FOREIGN KEY (
        EMP_CODE
    )
    REFERENCES EMPLOYEE (EMP_CODE),
    FOREIGN KEY (
        ENT_CODE
    )
    REFERENCES ENTRY (ENT_CODE) 
);

INSERT INTO PAYROLL (
                        PRL_CODE,
                        EMP_CODE,
                        PRL_STATUS,
                        PRL_DATE,
                        ENT_CODE,
                        PRL_TOTAL_PAY,
                        PRL_PRICE_HR
                    )
                    VALUES (
                        1,
                        1,
                        'PAY',
                        '2023-10-09 08:00:00',
                        '1',
                        5000,
                        25
                    );

INSERT INTO PAYROLL (
                        PRL_CODE,
                        EMP_CODE,
                        PRL_STATUS,
                        PRL_DATE,
                        ENT_CODE,
                        PRL_TOTAL_PAY,
                        PRL_PRICE_HR
                    )
                    VALUES (
                        2,
                        2,
                        'PAY',
                        '2023-10-09 09:15:00',
                        '2',
                        6000,
                        25
                    );

INSERT INTO PAYROLL (
                        PRL_CODE,
                        EMP_CODE,
                        PRL_STATUS,
                        PRL_DATE,
                        ENT_CODE,
                        PRL_TOTAL_PAY,
                        PRL_PRICE_HR
                    )
                    VALUES (
                        3,
                        3,
                        'PAY',
                        '2023-10-09 10:30:00',
                        '3',
                        7000,
                        25
                    );

INSERT INTO PAYROLL (
                        PRL_CODE,
                        EMP_CODE,
                        PRL_STATUS,
                        PRL_DATE,
                        ENT_CODE,
                        PRL_TOTAL_PAY,
                        PRL_PRICE_HR
                    )
                    VALUES (
                        4,
                        4,
                        'PAY',
                        '2023-10-09 11:45:00',
                        '4',
                        8000,
                        25
                    );

INSERT INTO PAYROLL (
                        PRL_CODE,
                        EMP_CODE,
                        PRL_STATUS,
                        PRL_DATE,
                        ENT_CODE,
                        PRL_TOTAL_PAY,
                        PRL_PRICE_HR
                    )
                    VALUES (
                        5,
                        5,
                        'PAY',
                        '2023-10-09 12:00:00',
                        '5',
                        9000,
                        25
                    );

INSERT INTO PAYROLL (
                        PRL_CODE,
                        EMP_CODE,
                        PRL_STATUS,
                        PRL_DATE,
                        ENT_CODE,
                        PRL_TOTAL_PAY,
                        PRL_PRICE_HR
                    )
                    VALUES (
                        6,
                        6,
                        'PAY',
                        '2023-10-10 08:00:00',
                        '6',
                        10000,
                        25
                    );

INSERT INTO PAYROLL (
                        PRL_CODE,
                        EMP_CODE,
                        PRL_STATUS,
                        PRL_DATE,
                        ENT_CODE,
                        PRL_TOTAL_PAY,
                        PRL_PRICE_HR
                    )
                    VALUES (
                        7,
                        7,
                        'PAY',
                        '2023-10-10 09:15:00',
                        '7',
                        11000,
                        25
                    );

INSERT INTO PAYROLL (
                        PRL_CODE,
                        EMP_CODE,
                        PRL_STATUS,
                        PRL_DATE,
                        ENT_CODE,
                        PRL_TOTAL_PAY,
                        PRL_PRICE_HR
                    )
                    VALUES (
                        8,
                        8,
                        'PAY',
                        '2023-10-10 10:30:00',
                        '8',
                        12000,
                        25
                    );

INSERT INTO PAYROLL (
                        PRL_CODE,
                        EMP_CODE,
                        PRL_STATUS,
                        PRL_DATE,
                        ENT_CODE,
                        PRL_TOTAL_PAY,
                        PRL_PRICE_HR
                    )
                    VALUES (
                        9,
                        9,
                        'PAY',
                        '2023-10-10 11:45:00',
                        '9',
                        13000,
                        25
                    );

INSERT INTO PAYROLL (
                        PRL_CODE,
                        EMP_CODE,
                        PRL_STATUS,
                        PRL_DATE,
                        ENT_CODE,
                        PRL_TOTAL_PAY,
                        PRL_PRICE_HR
                    )
                    VALUES (
                        10,
                        10,
                        'PAY',
                        '2023-10-10 12:00:00',
                        '10',
                        14000,
                        25
                    );


-- Table: PRODUCT
DROP TABLE IF EXISTS PRODUCT;

CREATE TABLE IF NOT EXISTS PRODUCT (
    PROD_CODE         INTEGER        PRIMARY KEY ASC AUTOINCREMENT
                                     UNIQUE,
    PROD_NAME         VARCHAR (20)   NOT NULL,
    PROD_PRICE        NUMERIC (5, 2) CHECK (PROD_PRICE >= 0.0 AND 
                                            PROD_PRICE <= 999.99) 
                                     NOT NULL,
    PROD_CATEGORY     VARCHAR (10)   NOT NULL
                                     REFERENCES CATEGORY (CAT_CODE) ON DELETE CASCADE
                                                                    ON UPDATE CASCADE
                                     DEFAULT [DRINK,DRESSERT,FOOD],
    PROD_SUB_CATEGORY VARCHAR (10)   NOT NULL
                                     DEFAULT [HOT,COLD,FRAPPE,-],
    PROD_SIZE         VARCHAR (6)    NOT NULL,
    PROD_SWEET_LEVEL  TEXT           DEFAULT [0,25,50,75,100]
                                     NOT NULL
);

INSERT INTO PRODUCT (
                        PROD_CODE,
                        PROD_NAME,
                        PROD_PRICE,
                        PROD_CATEGORY,
                        PROD_SUB_CATEGORY,
                        PROD_SIZE,
                        PROD_SWEET_LEVEL
                    )
                    VALUES (
                        1,
                        'Cappuccino',
                        30,
                        '1',
                        'HOT,COLD,FRAPPE',
                        'SML',
                        '0,25,50,75,100'
                    );

INSERT INTO PRODUCT (
                        PROD_CODE,
                        PROD_NAME,
                        PROD_PRICE,
                        PROD_CATEGORY,
                        PROD_SUB_CATEGORY,
                        PROD_SIZE,
                        PROD_SWEET_LEVEL
                    )
                    VALUES (
                        2,
                        'Latte',
                        30,
                        '1',
                        'HOT,COLD,FRAPPE',
                        'SML',
                        '0,25,50,75,100'
                    );

INSERT INTO PRODUCT (
                        PROD_CODE,
                        PROD_NAME,
                        PROD_PRICE,
                        PROD_CATEGORY,
                        PROD_SUB_CATEGORY,
                        PROD_SIZE,
                        PROD_SWEET_LEVEL
                    )
                    VALUES (
                        3,
                        'Espresso',
                        30,
                        '1',
                        'HOT,COLD,FRAPPE',
                        'SML',
                        '0,25,50,75,100'
                    );

INSERT INTO PRODUCT (
                        PROD_CODE,
                        PROD_NAME,
                        PROD_PRICE,
                        PROD_CATEGORY,
                        PROD_SUB_CATEGORY,
                        PROD_SIZE,
                        PROD_SWEET_LEVEL
                    )
                    VALUES (
                        4,
                        'Mocha',
                        30,
                        '1',
                        'HOT,COLD,FRAPPE',
                        'SML',
                        '0,25,50,75,100'
                    );

INSERT INTO PRODUCT (
                        PROD_CODE,
                        PROD_NAME,
                        PROD_PRICE,
                        PROD_CATEGORY,
                        PROD_SUB_CATEGORY,
                        PROD_SIZE,
                        PROD_SWEET_LEVEL
                    )
                    VALUES (
                        5,
                        'Americano',
                        30,
                        '1',
                        'HOT,COLD,FRAPPE',
                        'SML',
                        '0,25,50,75,100'
                    );

INSERT INTO PRODUCT (
                        PROD_CODE,
                        PROD_NAME,
                        PROD_PRICE,
                        PROD_CATEGORY,
                        PROD_SUB_CATEGORY,
                        PROD_SIZE,
                        PROD_SWEET_LEVEL
                    )
                    VALUES (
                        6,
                        'Bagel',
                        40,
                        '2',
                        '-',
                        '-',
                        '-'
                    );

INSERT INTO PRODUCT (
                        PROD_CODE,
                        PROD_NAME,
                        PROD_PRICE,
                        PROD_CATEGORY,
                        PROD_SUB_CATEGORY,
                        PROD_SIZE,
                        PROD_SWEET_LEVEL
                    )
                    VALUES (
                        7,
                        'Croissant',
                        50,
                        '2',
                        '-',
                        '-',
                        '-'
                    );

INSERT INTO PRODUCT (
                        PROD_CODE,
                        PROD_NAME,
                        PROD_PRICE,
                        PROD_CATEGORY,
                        PROD_SUB_CATEGORY,
                        PROD_SIZE,
                        PROD_SWEET_LEVEL
                    )
                    VALUES (
                        8,
                        'Chocolate Chip Cookie',
                        25,
                        '2',
                        '-',
                        '-',
                        '-'
                    );

INSERT INTO PRODUCT (
                        PROD_CODE,
                        PROD_NAME,
                        PROD_PRICE,
                        PROD_CATEGORY,
                        PROD_SUB_CATEGORY,
                        PROD_SIZE,
                        PROD_SWEET_LEVEL
                    )
                    VALUES (
                        9,
                        'Blueberry Muffin',
                        30,
                        '2',
                        '-',
                        '-',
                        '-'
                    );

INSERT INTO PRODUCT (
                        PROD_CODE,
                        PROD_NAME,
                        PROD_PRICE,
                        PROD_CATEGORY,
                        PROD_SUB_CATEGORY,
                        PROD_SIZE,
                        PROD_SWEET_LEVEL
                    )
                    VALUES (
                        10,
                        'Egg Sandwich',
                        30,
                        '3',
                        '-',
                        '-',
                        '-'
                    );

INSERT INTO PRODUCT (
                        PROD_CODE,
                        PROD_NAME,
                        PROD_PRICE,
                        PROD_CATEGORY,
                        PROD_SUB_CATEGORY,
                        PROD_SIZE,
                        PROD_SWEET_LEVEL
                    )
                    VALUES (
                        11,
                        'Club Sandwich',
                        30,
                        '3',
                        '-',
                        '-',
                        '-'
                    );

INSERT INTO PRODUCT (
                        PROD_CODE,
                        PROD_NAME,
                        PROD_PRICE,
                        PROD_CATEGORY,
                        PROD_SUB_CATEGORY,
                        PROD_SIZE,
                        PROD_SWEET_LEVEL
                    )
                    VALUES (
                        12,
                        'Chicken Wrap',
                        30,
                        '3',
                        '-',
                        '-',
                        '-'
                    );

INSERT INTO PRODUCT (
                        PROD_CODE,
                        PROD_NAME,
                        PROD_PRICE,
                        PROD_CATEGORY,
                        PROD_SUB_CATEGORY,
                        PROD_SIZE,
                        PROD_SWEET_LEVEL
                    )
                    VALUES (
                        13,
                        'Caesar Salad',
                        40,
                        '3',
                        '-',
                        '-',
                        '-'
                    );

INSERT INTO PRODUCT (
                        PROD_CODE,
                        PROD_NAME,
                        PROD_PRICE,
                        PROD_CATEGORY,
                        PROD_SUB_CATEGORY,
                        PROD_SIZE,
                        PROD_SWEET_LEVEL
                    )
                    VALUES (
                        14,
                        'Cheeseburger',
                        50,
                        '3',
                        '-',
                        '-',
                        '-'
                    );

INSERT INTO PRODUCT (
                        PROD_CODE,
                        PROD_NAME,
                        PROD_PRICE,
                        PROD_CATEGORY,
                        PROD_SUB_CATEGORY,
                        PROD_SIZE,
                        PROD_SWEET_LEVEL
                    )
                    VALUES (
                        15,
                        'Cold Brew',
                        30,
                        '1',
                        'HOT,COLD,FRAPPE',
                        'SML',
                        '0,25,50,75,100'
                    );

INSERT INTO PRODUCT (
                        PROD_CODE,
                        PROD_NAME,
                        PROD_PRICE,
                        PROD_CATEGORY,
                        PROD_SUB_CATEGORY,
                        PROD_SIZE,
                        PROD_SWEET_LEVEL
                    )
                    VALUES (
                        16,
                        'Chai Latte',
                        30,
                        '1',
                        'HOT,COLD,FRAPPE',
                        'SML',
                        '0,25,50,75,100'
                    );

INSERT INTO PRODUCT (
                        PROD_CODE,
                        PROD_NAME,
                        PROD_PRICE,
                        PROD_CATEGORY,
                        PROD_SUB_CATEGORY,
                        PROD_SIZE,
                        PROD_SWEET_LEVEL
                    )
                    VALUES (
                        17,
                        'Green Tea',
                        30,
                        '1',
                        'HOT,COLD,FRAPPE',
                        'SML',
                        '0,25,50,75,100'
                    );

INSERT INTO PRODUCT (
                        PROD_CODE,
                        PROD_NAME,
                        PROD_PRICE,
                        PROD_CATEGORY,
                        PROD_SUB_CATEGORY,
                        PROD_SIZE,
                        PROD_SWEET_LEVEL
                    )
                    VALUES (
                        18,
                        'Baguette',
                        50,
                        '2',
                        '-',
                        '-',
                        '-'
                    );

INSERT INTO PRODUCT (
                        PROD_CODE,
                        PROD_NAME,
                        PROD_PRICE,
                        PROD_CATEGORY,
                        PROD_SUB_CATEGORY,
                        PROD_SIZE,
                        PROD_SWEET_LEVEL
                    )
                    VALUES (
                        19,
                        'Scone',
                        30,
                        '2',
                        '-',
                        '-',
                        '-'
                    );

INSERT INTO PRODUCT (
                        PROD_CODE,
                        PROD_NAME,
                        PROD_PRICE,
                        PROD_CATEGORY,
                        PROD_SUB_CATEGORY,
                        PROD_SIZE,
                        PROD_SWEET_LEVEL
                    )
                    VALUES (
                        20,
                        'Chocolate Brownie',
                        30,
                        '2',
                        '-',
                        '-',
                        '-'
                    );

INSERT INTO PRODUCT (
                        PROD_CODE,
                        PROD_NAME,
                        PROD_PRICE,
                        PROD_CATEGORY,
                        PROD_SUB_CATEGORY,
                        PROD_SIZE,
                        PROD_SWEET_LEVEL
                    )
                    VALUES (
                        21,
                        'Cheese Danish',
                        30,
                        '2',
                        '-',
                        '-',
                        '-'
                    );

INSERT INTO PRODUCT (
                        PROD_CODE,
                        PROD_NAME,
                        PROD_PRICE,
                        PROD_CATEGORY,
                        PROD_SUB_CATEGORY,
                        PROD_SIZE,
                        PROD_SWEET_LEVEL
                    )
                    VALUES (
                        22,
                        'Ham and Cheese Croissant',
                        30,
                        '2',
                        '-',
                        '-',
                        '-'
                    );

INSERT INTO PRODUCT (
                        PROD_CODE,
                        PROD_NAME,
                        PROD_PRICE,
                        PROD_CATEGORY,
                        PROD_SUB_CATEGORY,
                        PROD_SIZE,
                        PROD_SWEET_LEVEL
                    )
                    VALUES (
                        23,
                        'Caprese Panini',
                        30,
                        '3',
                        '-',
                        '-',
                        '-'
                    );

INSERT INTO PRODUCT (
                        PROD_CODE,
                        PROD_NAME,
                        PROD_PRICE,
                        PROD_CATEGORY,
                        PROD_SUB_CATEGORY,
                        PROD_SIZE,
                        PROD_SWEET_LEVEL
                    )
                    VALUES (
                        24,
                        'Spinach and Feta Wrap',
                        30,
                        '3',
                        '-',
                        '-',
                        '-'
                    );

INSERT INTO PRODUCT (
                        PROD_CODE,
                        PROD_NAME,
                        PROD_PRICE,
                        PROD_CATEGORY,
                        PROD_SUB_CATEGORY,
                        PROD_SIZE,
                        PROD_SWEET_LEVEL
                    )
                    VALUES (
                        25,
                        'Greek Salad',
                        30,
                        '3',
                        '-',
                        '-',
                        '-'
                    );

INSERT INTO PRODUCT (
                        PROD_CODE,
                        PROD_NAME,
                        PROD_PRICE,
                        PROD_CATEGORY,
                        PROD_SUB_CATEGORY,
                        PROD_SIZE,
                        PROD_SWEET_LEVEL
                    )
                    VALUES (
                        26,
                        'Veggie Burger',
                        30,
                        '3',
                        '-',
                        '-',
                        '-'
                    );

INSERT INTO PRODUCT (
                        PROD_CODE,
                        PROD_NAME,
                        PROD_PRICE,
                        PROD_CATEGORY,
                        PROD_SUB_CATEGORY,
                        PROD_SIZE,
                        PROD_SWEET_LEVEL
                    )
                    VALUES (
                        27,
                        'Iced Tea',
                        30,
                        '1',
                        'HOT,COLD,FRAPPE',
                        'SML',
                        '0,25,50,75,100'
                    );

INSERT INTO PRODUCT (
                        PROD_CODE,
                        PROD_NAME,
                        PROD_PRICE,
                        PROD_CATEGORY,
                        PROD_SUB_CATEGORY,
                        PROD_SIZE,
                        PROD_SWEET_LEVEL
                    )
                    VALUES (
                        28,
                        'Flat White',
                        30,
                        '1',
                        'HOT,COLD,FRAPPE',
                        'SML',
                        '0,25,50,75,100'
                    );

INSERT INTO PRODUCT (
                        PROD_CODE,
                        PROD_NAME,
                        PROD_PRICE,
                        PROD_CATEGORY,
                        PROD_SUB_CATEGORY,
                        PROD_SIZE,
                        PROD_SWEET_LEVEL
                    )
                    VALUES (
                        29,
                        'Macchiato',
                        30,
                        '1',
                        'HOT,COLD,FRAPPE',
                        'SML',
                        '0,25,50,75,100'
                    );

INSERT INTO PRODUCT (
                        PROD_CODE,
                        PROD_NAME,
                        PROD_PRICE,
                        PROD_CATEGORY,
                        PROD_SUB_CATEGORY,
                        PROD_SIZE,
                        PROD_SWEET_LEVEL
                    )
                    VALUES (
                        30,
                        'Cinnamon Roll',
                        30,
                        '2',
                        '-',
                        '-',
                        '-'
                    );

INSERT INTO PRODUCT (
                        PROD_CODE,
                        PROD_NAME,
                        PROD_PRICE,
                        PROD_CATEGORY,
                        PROD_SUB_CATEGORY,
                        PROD_SIZE,
                        PROD_SWEET_LEVEL
                    )
                    VALUES (
                        31,
                        'Oatmeal Cookie',
                        30,
                        '2',
                        '-',
                        '-',
                        '-'
                    );

INSERT INTO PRODUCT (
                        PROD_CODE,
                        PROD_NAME,
                        PROD_PRICE,
                        PROD_CATEGORY,
                        PROD_SUB_CATEGORY,
                        PROD_SIZE,
                        PROD_SWEET_LEVEL
                    )
                    VALUES (
                        32,
                        'Raspberry Muffin',
                        30,
                        '2',
                        '-',
                        '-',
                        '-'
                    );

INSERT INTO PRODUCT (
                        PROD_CODE,
                        PROD_NAME,
                        PROD_PRICE,
                        PROD_CATEGORY,
                        PROD_SUB_CATEGORY,
                        PROD_SIZE,
                        PROD_SWEET_LEVEL
                    )
                    VALUES (
                        33,
                        'Turkey Sandwich',
                        30,
                        '3',
                        '-',
                        '-',
                        '-'
                    );

INSERT INTO PRODUCT (
                        PROD_CODE,
                        PROD_NAME,
                        PROD_PRICE,
                        PROD_CATEGORY,
                        PROD_SUB_CATEGORY,
                        PROD_SIZE,
                        PROD_SWEET_LEVEL
                    )
                    VALUES (
                        34,
                        'BLT Sandwich',
                        30,
                        '3',
                        '-',
                        '-',
                        '-'
                    );

INSERT INTO PRODUCT (
                        PROD_CODE,
                        PROD_NAME,
                        PROD_PRICE,
                        PROD_CATEGORY,
                        PROD_SUB_CATEGORY,
                        PROD_SIZE,
                        PROD_SWEET_LEVEL
                    )
                    VALUES (
                        35,
                        'Chicken Caesar Wrap',
                        30,
                        '3',
                        '-',
                        '-',
                        '-'
                    );

INSERT INTO PRODUCT (
                        PROD_CODE,
                        PROD_NAME,
                        PROD_PRICE,
                        PROD_CATEGORY,
                        PROD_SUB_CATEGORY,
                        PROD_SIZE,
                        PROD_SWEET_LEVEL
                    )
                    VALUES (
                        36,
                        'Cobb Salad',
                        30,
                        '3',
                        '-',
                        '-',
                        '-'
                    );

INSERT INTO PRODUCT (
                        PROD_CODE,
                        PROD_NAME,
                        PROD_PRICE,
                        PROD_CATEGORY,
                        PROD_SUB_CATEGORY,
                        PROD_SIZE,
                        PROD_SWEET_LEVEL
                    )
                    VALUES (
                        37,
                        'Veggie Wrap',
                        30,
                        '3',
                        '-',
                        '-',
                        '-'
                    );

INSERT INTO PRODUCT (
                        PROD_CODE,
                        PROD_NAME,
                        PROD_PRICE,
                        PROD_CATEGORY,
                        PROD_SUB_CATEGORY,
                        PROD_SIZE,
                        PROD_SWEET_LEVEL
                    )
                    VALUES (
                        38,
                        'Iced Latte',
                        30,
                        '1',
                        'HOT,COLD,FRAPPE',
                        'SML',
                        '0,25,50,75,100'
                    );

INSERT INTO PRODUCT (
                        PROD_CODE,
                        PROD_NAME,
                        PROD_PRICE,
                        PROD_CATEGORY,
                        PROD_SUB_CATEGORY,
                        PROD_SIZE,
                        PROD_SWEET_LEVEL
                    )
                    VALUES (
                        39,
                        'Turmeric Latte',
                        30,
                        '1',
                        'HOT,COLD,FRAPPE',
                        'SML',
                        '0,25,50,75,100'
                    );

INSERT INTO PRODUCT (
                        PROD_CODE,
                        PROD_NAME,
                        PROD_PRICE,
                        PROD_CATEGORY,
                        PROD_SUB_CATEGORY,
                        PROD_SIZE,
                        PROD_SWEET_LEVEL
                    )
                    VALUES (
                        40,
                        'Matcha Latte',
                        30,
                        '1',
                        'HOT,COLD,FRAPPE',
                        'SML',
                        '0,25,50,75,100'
                    );

INSERT INTO PRODUCT (
                        PROD_CODE,
                        PROD_NAME,
                        PROD_PRICE,
                        PROD_CATEGORY,
                        PROD_SUB_CATEGORY,
                        PROD_SIZE,
                        PROD_SWEET_LEVEL
                    )
                    VALUES (
                        41,
                        'Pumpkin Spice Latte',
                        30,
                        '1',
                        'HOT,COLD,FRAPPE',
                        'SML',
                        '0,25,50,75,100'
                    );

INSERT INTO PRODUCT (
                        PROD_CODE,
                        PROD_NAME,
                        PROD_PRICE,
                        PROD_CATEGORY,
                        PROD_SUB_CATEGORY,
                        PROD_SIZE,
                        PROD_SWEET_LEVEL
                    )
                    VALUES (
                        42,
                        'Chocolate Milk',
                        30,
                        '1',
                        'HOT,COLD,FRAPPE',
                        'SML',
                        '0,25,50,75,100'
                    );

INSERT INTO PRODUCT (
                        PROD_CODE,
                        PROD_NAME,
                        PROD_PRICE,
                        PROD_CATEGORY,
                        PROD_SUB_CATEGORY,
                        PROD_SIZE,
                        PROD_SWEET_LEVEL
                    )
                    VALUES (
                        43,
                        'Baguette Sandwich',
                        30,
                        '3',
                        '-',
                        '-',
                        '-'
                    );

INSERT INTO PRODUCT (
                        PROD_CODE,
                        PROD_NAME,
                        PROD_PRICE,
                        PROD_CATEGORY,
                        PROD_SUB_CATEGORY,
                        PROD_SIZE,
                        PROD_SWEET_LEVEL
                    )
                    VALUES (
                        44,
                        'Turkey Club Sandwich',
                        30,
                        '3',
                        '-',
                        '-',
                        '-'
                    );

INSERT INTO PRODUCT (
                        PROD_CODE,
                        PROD_NAME,
                        PROD_PRICE,
                        PROD_CATEGORY,
                        PROD_SUB_CATEGORY,
                        PROD_SIZE,
                        PROD_SWEET_LEVEL
                    )
                    VALUES (
                        45,
                        'Roast Beef Wrap',
                        30,
                        '3',
                        '-',
                        '-',
                        '-'
                    );

INSERT INTO PRODUCT (
                        PROD_CODE,
                        PROD_NAME,
                        PROD_PRICE,
                        PROD_CATEGORY,
                        PROD_SUB_CATEGORY,
                        PROD_SIZE,
                        PROD_SWEET_LEVEL
                    )
                    VALUES (
                        46,
                        'Cranberry Walnut Salad',
                        30,
                        '3',
                        '-',
                        '-',
                        '-'
                    );

INSERT INTO PRODUCT (
                        PROD_CODE,
                        PROD_NAME,
                        PROD_PRICE,
                        PROD_CATEGORY,
                        PROD_SUB_CATEGORY,
                        PROD_SIZE,
                        PROD_SWEET_LEVEL
                    )
                    VALUES (
                        47,
                        'Bacon Cheeseburger',
                        30,
                        '3',
                        '-',
                        '-',
                        '-'
                    );

INSERT INTO PRODUCT (
                        PROD_CODE,
                        PROD_NAME,
                        PROD_PRICE,
                        PROD_CATEGORY,
                        PROD_SUB_CATEGORY,
                        PROD_SIZE,
                        PROD_SWEET_LEVEL
                    )
                    VALUES (
                        48,
                        'Hot Chocolate',
                        30,
                        '1',
                        'HOT,COLD,FRAPPE',
                        'SML',
                        '0,25,50,75,100'
                    );

INSERT INTO PRODUCT (
                        PROD_CODE,
                        PROD_NAME,
                        PROD_PRICE,
                        PROD_CATEGORY,
                        PROD_SUB_CATEGORY,
                        PROD_SIZE,
                        PROD_SWEET_LEVEL
                    )
                    VALUES (
                        49,
                        'Chamomile Tea',
                        30,
                        '1',
                        'HOT,COLD,FRAPPE',
                        'SML',
                        '0,25,50,75,100'
                    );


-- Table: PROMOTION
DROP TABLE IF EXISTS PROMOTION;

CREATE TABLE IF NOT EXISTS PROMOTION (
    PRO_CODE      INTEGER       PRIMARY KEY ASC AUTOINCREMENT
                                UNIQUE,
    PRO_NAME      VARCHAR (30)  NOT NULL,
    PRO_STARTDATE DATE          NOT NULL
                                DEFAULT (DATETIME() ),
    PRO_ENDDATE   DATE          NOT NULL
                                DEFAULT (DATETIME() ),
    PRO_DETAIL    VARCHAR (100) NOT NULL,
    PRO_STATUS    VARCHAR (10)  NOT NULL
                                DEFAULT [ACTIVE,INACTIVE]
);

INSERT INTO PROMOTION (
                          PRO_CODE,
                          PRO_NAME,
                          PRO_STARTDATE,
                          PRO_ENDDATE,
                          PRO_DETAIL,
                          PRO_STATUS
                      )
                      VALUES (
                          1,
                          'POINT',
                          '2023-01-01',
                          '2023-12-31',
                          '100POINT DISCOUNT 10 BATH',
                          'ACTIVE'
                      );

INSERT INTO PROMOTION (
                          PRO_CODE,
                          PRO_NAME,
                          PRO_STARTDATE,
                          PRO_ENDDATE,
                          PRO_DETAIL,
                          PRO_STATUS
                      )
                      VALUES (
                          2,
                          'awdwadw',
                          '2023-09-10',
                          '2023-10-30',
                          'weewwaaw',
                          'active'
                      );


-- Table: PROMOTION_DETAIL
DROP TABLE IF EXISTS PROMOTION_DETAIL;

CREATE TABLE IF NOT EXISTS PROMOTION_DETAIL (
    PRDE_CODE     INTEGER        PRIMARY KEY ASC AUTOINCREMENT
                                 UNIQUE,
    RC_CODE       INTEGER        NOT NULL
                                 REFERENCES RECEIPT (RE_CODE) ON DELETE CASCADE
                                                              ON UPDATE CASCADE,
    PRO_CODE      INTEGER        NOT NULL
                                 REFERENCES PROMOTION (PRO_CODE) ON DELETE CASCADE
                                                                 ON UPDATE CASCADE,
    PRDE_DISCOUNT NUMERIC (6, 2) CHECK (PRDE_DISCOUNT >= 0.0 AND 
                                        PRDE_DISCOUNT <= 9999.99) 
                                 NOT NULL
);

INSERT INTO PROMOTION_DETAIL (
                                 PRDE_CODE,
                                 RC_CODE,
                                 PRO_CODE,
                                 PRDE_DISCOUNT
                             )
                             VALUES (
                                 1,
                                 1,
                                 8,
                                 150
                             );

INSERT INTO PROMOTION_DETAIL (
                                 PRDE_CODE,
                                 RC_CODE,
                                 PRO_CODE,
                                 PRDE_DISCOUNT
                             )
                             VALUES (
                                 2,
                                 2,
                                 11,
                                 75
                             );

INSERT INTO PROMOTION_DETAIL (
                                 PRDE_CODE,
                                 RC_CODE,
                                 PRO_CODE,
                                 PRDE_DISCOUNT
                             )
                             VALUES (
                                 3,
                                 3,
                                 3,
                                 50
                             );

INSERT INTO PROMOTION_DETAIL (
                                 PRDE_CODE,
                                 RC_CODE,
                                 PRO_CODE,
                                 PRDE_DISCOUNT
                             )
                             VALUES (
                                 4,
                                 4,
                                 14,
                                 125
                             );


-- Table: RECEIPT
DROP TABLE IF EXISTS RECEIPT;

CREATE TABLE IF NOT EXISTS RECEIPT (
    RE_CODE      INTEGER        PRIMARY KEY ASC AUTOINCREMENT
                                UNIQUE,
    EMP_CODE     INTEGER        NOT NULL
                                REFERENCES EMPLOYEE (EMP_CODE) ON DELETE CASCADE
                                                               ON UPDATE CASCADE,
    CUS_CODE     INTEGER        REFERENCES CUSTOMER (CUS_CODE) ON DELETE CASCADE
                                                               ON UPDATE CASCADE,
    RE_TOTAL     NUMERIC (5, 2) CHECK (RE_TOTAL >= 0.0 AND 
                                       RE_TOTAL <= 999.99) 
                                NOT NULL,
    RE_DATE      DATE           NOT NULL
                                DEFAULT (datetime('now', 'localtime') ),
    RE_PAYMENT   NUMERIC (5, 2) CHECK (RE_PAYMENT >= 0.0 AND 
                                       RE_PAYMENT <= 999.99) 
                                NOT NULL,
    RE_CHANGE    VARCHAR (20),
    RE_NET_TOTAL NUMERIC (5, 2) CHECK (RE_NET_TOTAL >= 0.0 AND 
                                       RE_NET_TOTAL <= 999.99),
    RE_DISCOUNT  NUMERIC (6, 2) CHECK (RE_DISCOUNT >= 0.0 AND 
                                       RE_DISCOUNT <= 9999.99),
    RE_TOTAL_QTY INTEGER        NOT NULL,
    FOREIGN KEY (
        CUS_CODE
    )
    REFERENCES CUSTOMER (CUST_CODE) 
);

INSERT INTO RECEIPT (
                        RE_CODE,
                        EMP_CODE,
                        CUS_CODE,
                        RE_TOTAL,
                        RE_DATE,
                        RE_PAYMENT,
                        RE_CHANGE,
                        RE_NET_TOTAL,
                        RE_DISCOUNT,
                        RE_TOTAL_QTY
                    )
                    VALUES (
                        1,
                        1,
                        1,
                        50,
                        '2023-10-09 08:15:00',
                        50,
                        '0.0',
                        50,
                        0,
                        10
                    );

INSERT INTO RECEIPT (
                        RE_CODE,
                        EMP_CODE,
                        CUS_CODE,
                        RE_TOTAL,
                        RE_DATE,
                        RE_PAYMENT,
                        RE_CHANGE,
                        RE_NET_TOTAL,
                        RE_DISCOUNT,
                        RE_TOTAL_QTY
                    )
                    VALUES (
                        2,
                        2,
                        2,
                        75,
                        '2023-10-09 09:30:00',
                        75,
                        '0.0',
                        75,
                        0,
                        10
                    );

INSERT INTO RECEIPT (
                        RE_CODE,
                        EMP_CODE,
                        CUS_CODE,
                        RE_TOTAL,
                        RE_DATE,
                        RE_PAYMENT,
                        RE_CHANGE,
                        RE_NET_TOTAL,
                        RE_DISCOUNT,
                        RE_TOTAL_QTY
                    )
                    VALUES (
                        3,
                        3,
                        3,
                        60,
                        '2023-10-09 10:45:00',
                        60,
                        '0.0',
                        60,
                        0,
                        10
                    );

INSERT INTO RECEIPT (
                        RE_CODE,
                        EMP_CODE,
                        CUS_CODE,
                        RE_TOTAL,
                        RE_DATE,
                        RE_PAYMENT,
                        RE_CHANGE,
                        RE_NET_TOTAL,
                        RE_DISCOUNT,
                        RE_TOTAL_QTY
                    )
                    VALUES (
                        4,
                        4,
                        4,
                        45,
                        '2023-10-09 11:00:00',
                        45,
                        '0.0',
                        45,
                        0,
                        10
                    );

INSERT INTO RECEIPT (
                        RE_CODE,
                        EMP_CODE,
                        CUS_CODE,
                        RE_TOTAL,
                        RE_DATE,
                        RE_PAYMENT,
                        RE_CHANGE,
                        RE_NET_TOTAL,
                        RE_DISCOUNT,
                        RE_TOTAL_QTY
                    )
                    VALUES (
                        5,
                        5,
                        5,
                        90,
                        '2023-10-09 12:15:00',
                        90,
                        '0.0',
                        90,
                        0,
                        10
                    );

INSERT INTO RECEIPT (
                        RE_CODE,
                        EMP_CODE,
                        CUS_CODE,
                        RE_TOTAL,
                        RE_DATE,
                        RE_PAYMENT,
                        RE_CHANGE,
                        RE_NET_TOTAL,
                        RE_DISCOUNT,
                        RE_TOTAL_QTY
                    )
                    VALUES (
                        6,
                        1,
                        2,
                        100,
                        '2023-10-14 19:48:35',
                        50,
                        NULL,
                        NULL,
                        NULL,
                        5
                    );

INSERT INTO RECEIPT (
                        RE_CODE,
                        EMP_CODE,
                        CUS_CODE,
                        RE_TOTAL,
                        RE_DATE,
                        RE_PAYMENT,
                        RE_CHANGE,
                        RE_NET_TOTAL,
                        RE_DISCOUNT,
                        RE_TOTAL_QTY
                    )
                    VALUES (
                        7,
                        1,
                        2,
                        100,
                        '2023-10-14 19:49:16',
                        50,
                        NULL,
                        NULL,
                        NULL,
                        5
                    );

INSERT INTO RECEIPT (
                        RE_CODE,
                        EMP_CODE,
                        CUS_CODE,
                        RE_TOTAL,
                        RE_DATE,
                        RE_PAYMENT,
                        RE_CHANGE,
                        RE_NET_TOTAL,
                        RE_DISCOUNT,
                        RE_TOTAL_QTY
                    )
                    VALUES (
                        8,
                        1,
                        2,
                        100,
                        '2023-10-14 19:49:56',
                        50,
                        NULL,
                        NULL,
                        NULL,
                        5
                    );


-- Table: RECEIPT_DETAIL
DROP TABLE IF EXISTS RECEIPT_DETAIL;

CREATE TABLE IF NOT EXISTS RECEIPT_DETAIL (
    RCD_CODE       INTEGER        PRIMARY KEY ASC AUTOINCREMENT
                                  UNIQUE,
    RC_CODE        INTEGER        NOT NULL
                                  REFERENCES RECEIPT (RE_CODE) ON DELETE CASCADE
                                                               ON UPDATE CASCADE,
    PROD_CODE      INTEGER        NOT NULL
                                  REFERENCES PRODUCT (PROD_CODE) ON DELETE CASCADE
                                                                 ON UPDATE CASCADE,
    RCD_QTY        INTEGER        NOT NULL,
    RCD_UNIT_PRICE NUMERIC (6, 2) CHECK (RCD_UNIT_PRICE >= 0.0 AND 
                                         RCD_UNIT_PRICE <= 9999.99) 
                                  NOT NULL,
    RCD_TOTAL      NUMERIC (6, 2) CHECK (RCD_TOTAL >= 0.0 AND 
                                         RCD_TOTAL <= 9999.99) 
                                  NOT NULL,
    RCD_DISCOUNT   NUMERIC (6, 2) CHECK (RCD_DISCOUNT >= 0.0 AND 
                                         RCD_DISCOUNT <= 9999.99),
    PRDE_CODE      INTEGER        REFERENCES PROMOTION_DETAIL (PRDE_CODE) ON DELETE CASCADE
                                                                          ON UPDATE CASCADE,
    FOREIGN KEY (
        PRDE_CODE
    )
    REFERENCES PROMOTION_DETAIL (PRDE_CODE) 
);

INSERT INTO RECEIPT_DETAIL (
                               RCD_CODE,
                               RC_CODE,
                               PROD_CODE,
                               RCD_QTY,
                               RCD_UNIT_PRICE,
                               RCD_TOTAL,
                               RCD_DISCOUNT,
                               PRDE_CODE
                           )
                           VALUES (
                               1,
                               1,
                               1,
                               2,
                               5,
                               10,
                               0,
                               NULL
                           );

INSERT INTO RECEIPT_DETAIL (
                               RCD_CODE,
                               RC_CODE,
                               PROD_CODE,
                               RCD_QTY,
                               RCD_UNIT_PRICE,
                               RCD_TOTAL,
                               RCD_DISCOUNT,
                               PRDE_CODE
                           )
                           VALUES (
                               2,
                               1,
                               3,
                               1,
                               3.99,
                               3.99,
                               0,
                               NULL
                           );

INSERT INTO RECEIPT_DETAIL (
                               RCD_CODE,
                               RC_CODE,
                               PROD_CODE,
                               RCD_QTY,
                               RCD_UNIT_PRICE,
                               RCD_TOTAL,
                               RCD_DISCOUNT,
                               PRDE_CODE
                           )
                           VALUES (
                               3,
                               2,
                               5,
                               4,
                               2.5,
                               10,
                               0,
                               NULL
                           );

INSERT INTO RECEIPT_DETAIL (
                               RCD_CODE,
                               RC_CODE,
                               PROD_CODE,
                               RCD_QTY,
                               RCD_UNIT_PRICE,
                               RCD_TOTAL,
                               RCD_DISCOUNT,
                               PRDE_CODE
                           )
                           VALUES (
                               4,
                               2,
                               7,
                               2,
                               1.25,
                               2.5,
                               0,
                               NULL
                           );

INSERT INTO RECEIPT_DETAIL (
                               RCD_CODE,
                               RC_CODE,
                               PROD_CODE,
                               RCD_QTY,
                               RCD_UNIT_PRICE,
                               RCD_TOTAL,
                               RCD_DISCOUNT,
                               PRDE_CODE
                           )
                           VALUES (
                               5,
                               3,
                               2,
                               3,
                               6.99,
                               20.97,
                               0,
                               NULL
                           );

INSERT INTO RECEIPT_DETAIL (
                               RCD_CODE,
                               RC_CODE,
                               PROD_CODE,
                               RCD_QTY,
                               RCD_UNIT_PRICE,
                               RCD_TOTAL,
                               RCD_DISCOUNT,
                               PRDE_CODE
                           )
                           VALUES (
                               6,
                               3,
                               4,
                               1,
                               4.49,
                               4.49,
                               0,
                               NULL
                           );

INSERT INTO RECEIPT_DETAIL (
                               RCD_CODE,
                               RC_CODE,
                               PROD_CODE,
                               RCD_QTY,
                               RCD_UNIT_PRICE,
                               RCD_TOTAL,
                               RCD_DISCOUNT,
                               PRDE_CODE
                           )
                           VALUES (
                               7,
                               4,
                               6,
                               2,
                               3,
                               6,
                               0,
                               NULL
                           );

INSERT INTO RECEIPT_DETAIL (
                               RCD_CODE,
                               RC_CODE,
                               PROD_CODE,
                               RCD_QTY,
                               RCD_UNIT_PRICE,
                               RCD_TOTAL,
                               RCD_DISCOUNT,
                               PRDE_CODE
                           )
                           VALUES (
                               8,
                               4,
                               8,
                               1,
                               2.99,
                               2.99,
                               0,
                               NULL
                           );

INSERT INTO RECEIPT_DETAIL (
                               RCD_CODE,
                               RC_CODE,
                               PROD_CODE,
                               RCD_QTY,
                               RCD_UNIT_PRICE,
                               RCD_TOTAL,
                               RCD_DISCOUNT,
                               PRDE_CODE
                           )
                           VALUES (
                               9,
                               5,
                               10,
                               1,
                               1.99,
                               1.99,
                               0,
                               NULL
                           );

INSERT INTO RECEIPT_DETAIL (
                               RCD_CODE,
                               RC_CODE,
                               PROD_CODE,
                               RCD_QTY,
                               RCD_UNIT_PRICE,
                               RCD_TOTAL,
                               RCD_DISCOUNT,
                               PRDE_CODE
                           )
                           VALUES (
                               10,
                               5,
                               12,
                               3,
                               0.75,
                               2.25,
                               0,
                               NULL
                           );

INSERT INTO RECEIPT_DETAIL (
                               RCD_CODE,
                               RC_CODE,
                               PROD_CODE,
                               RCD_QTY,
                               RCD_UNIT_PRICE,
                               RCD_TOTAL,
                               RCD_DISCOUNT,
                               PRDE_CODE
                           )
                           VALUES (
                               11,
                               1,
                               14,
                               2,
                               4.99,
                               9.98,
                               0,
                               NULL
                           );

INSERT INTO RECEIPT_DETAIL (
                               RCD_CODE,
                               RC_CODE,
                               PROD_CODE,
                               RCD_QTY,
                               RCD_UNIT_PRICE,
                               RCD_TOTAL,
                               RCD_DISCOUNT,
                               PRDE_CODE
                           )
                           VALUES (
                               12,
                               1,
                               16,
                               1,
                               3.99,
                               3.99,
                               0,
                               NULL
                           );

INSERT INTO RECEIPT_DETAIL (
                               RCD_CODE,
                               RC_CODE,
                               PROD_CODE,
                               RCD_QTY,
                               RCD_UNIT_PRICE,
                               RCD_TOTAL,
                               RCD_DISCOUNT,
                               PRDE_CODE
                           )
                           VALUES (
                               13,
                               2,
                               18,
                               4,
                               2.5,
                               10,
                               0,
                               NULL
                           );

INSERT INTO RECEIPT_DETAIL (
                               RCD_CODE,
                               RC_CODE,
                               PROD_CODE,
                               RCD_QTY,
                               RCD_UNIT_PRICE,
                               RCD_TOTAL,
                               RCD_DISCOUNT,
                               PRDE_CODE
                           )
                           VALUES (
                               14,
                               2,
                               20,
                               2,
                               1.25,
                               2.5,
                               0,
                               NULL
                           );

INSERT INTO RECEIPT_DETAIL (
                               RCD_CODE,
                               RC_CODE,
                               PROD_CODE,
                               RCD_QTY,
                               RCD_UNIT_PRICE,
                               RCD_TOTAL,
                               RCD_DISCOUNT,
                               PRDE_CODE
                           )
                           VALUES (
                               15,
                               3,
                               11,
                               3,
                               6.99,
                               20.97,
                               0,
                               NULL
                           );

INSERT INTO RECEIPT_DETAIL (
                               RCD_CODE,
                               RC_CODE,
                               PROD_CODE,
                               RCD_QTY,
                               RCD_UNIT_PRICE,
                               RCD_TOTAL,
                               RCD_DISCOUNT,
                               PRDE_CODE
                           )
                           VALUES (
                               16,
                               3,
                               13,
                               1,
                               4.49,
                               4.49,
                               0,
                               NULL
                           );

INSERT INTO RECEIPT_DETAIL (
                               RCD_CODE,
                               RC_CODE,
                               PROD_CODE,
                               RCD_QTY,
                               RCD_UNIT_PRICE,
                               RCD_TOTAL,
                               RCD_DISCOUNT,
                               PRDE_CODE
                           )
                           VALUES (
                               17,
                               4,
                               15,
                               2,
                               3,
                               6,
                               0,
                               NULL
                           );

INSERT INTO RECEIPT_DETAIL (
                               RCD_CODE,
                               RC_CODE,
                               PROD_CODE,
                               RCD_QTY,
                               RCD_UNIT_PRICE,
                               RCD_TOTAL,
                               RCD_DISCOUNT,
                               PRDE_CODE
                           )
                           VALUES (
                               18,
                               4,
                               17,
                               1,
                               2.99,
                               2.99,
                               0,
                               NULL
                           );

INSERT INTO RECEIPT_DETAIL (
                               RCD_CODE,
                               RC_CODE,
                               PROD_CODE,
                               RCD_QTY,
                               RCD_UNIT_PRICE,
                               RCD_TOTAL,
                               RCD_DISCOUNT,
                               PRDE_CODE
                           )
                           VALUES (
                               19,
                               5,
                               19,
                               1,
                               1.99,
                               1.99,
                               0,
                               NULL
                           );

INSERT INTO RECEIPT_DETAIL (
                               RCD_CODE,
                               RC_CODE,
                               PROD_CODE,
                               RCD_QTY,
                               RCD_UNIT_PRICE,
                               RCD_TOTAL,
                               RCD_DISCOUNT,
                               PRDE_CODE
                           )
                           VALUES (
                               20,
                               5,
                               1,
                               3,
                               0.75,
                               2.25,
                               0,
                               NULL
                           );


-- Table: STOCK_CHECK_DETAIL
DROP TABLE IF EXISTS STOCK_CHECK_DETAIL;

CREATE TABLE IF NOT EXISTS STOCK_CHECK_DETAIL (
    SCD_CODE             INTEGER        PRIMARY KEY ASC AUTOINCREMENT
                                        UNIQUE,
    M_CODE               INTEGER        NOT NULL
                                        REFERENCES MATERIAL (M_CODE) ON DELETE RESTRICT
                                                                     ON UPDATE CASCADE,
    CHECK_CODE           INTEGER        NOT NULL
                                        REFERENCES CHECK_STOCK (CHECK_CODE) ON DELETE RESTRICT
                                                                            ON UPDATE CASCADE,
    SCD_REMAINING_AMOUNT NUMERIC (7, 2) CHECK (SCD_REMAINING_AMOUNT >= 0.0 AND 
                                               SCD_REMAINING_AMOUNT <= 99999.0) 
                                        NOT NULL,
    SCD_EXPIRED_AMOUNT   NUMERIC (7, 2) CHECK (SCD_EXPIRED_AMOUNT >= 0.0 AND 
                                               SCD_EXPIRED_AMOUNT <= 99999.0),
    SCD_TOTAL_LOST       NUMERIC (7, 2) CHECK (SCD_TOTAL_LOST >= 0.0 AND 
                                               SCD_TOTAL_LOST <= 99999.0),
    FOREIGN KEY (
        M_CODE
    )
    REFERENCES MATERIAL (M_CODE),
    FOREIGN KEY (
        CHECK_CODE
    )
    REFERENCES CHECK_STOCK (CHECK_CODE) 
);

INSERT INTO STOCK_CHECK_DETAIL (
                                   SCD_CODE,
                                   M_CODE,
                                   CHECK_CODE,
                                   SCD_REMAINING_AMOUNT,
                                   SCD_EXPIRED_AMOUNT,
                                   SCD_TOTAL_LOST
                               )
                               VALUES (
                                   1,
                                   1,
                                   1,
                                   10,
                                   NULL,
                                   NULL
                               );

INSERT INTO STOCK_CHECK_DETAIL (
                                   SCD_CODE,
                                   M_CODE,
                                   CHECK_CODE,
                                   SCD_REMAINING_AMOUNT,
                                   SCD_EXPIRED_AMOUNT,
                                   SCD_TOTAL_LOST
                               )
                               VALUES (
                                   2,
                                   2,
                                   1,
                                   8,
                                   NULL,
                                   NULL
                               );

INSERT INTO STOCK_CHECK_DETAIL (
                                   SCD_CODE,
                                   M_CODE,
                                   CHECK_CODE,
                                   SCD_REMAINING_AMOUNT,
                                   SCD_EXPIRED_AMOUNT,
                                   SCD_TOTAL_LOST
                               )
                               VALUES (
                                   3,
                                   3,
                                   1,
                                   15,
                                   NULL,
                                   NULL
                               );

INSERT INTO STOCK_CHECK_DETAIL (
                                   SCD_CODE,
                                   M_CODE,
                                   CHECK_CODE,
                                   SCD_REMAINING_AMOUNT,
                                   SCD_EXPIRED_AMOUNT,
                                   SCD_TOTAL_LOST
                               )
                               VALUES (
                                   4,
                                   4,
                                   2,
                                   5,
                                   NULL,
                                   NULL
                               );

INSERT INTO STOCK_CHECK_DETAIL (
                                   SCD_CODE,
                                   M_CODE,
                                   CHECK_CODE,
                                   SCD_REMAINING_AMOUNT,
                                   SCD_EXPIRED_AMOUNT,
                                   SCD_TOTAL_LOST
                               )
                               VALUES (
                                   5,
                                   5,
                                   2,
                                   12,
                                   NULL,
                                   NULL
                               );

INSERT INTO STOCK_CHECK_DETAIL (
                                   SCD_CODE,
                                   M_CODE,
                                   CHECK_CODE,
                                   SCD_REMAINING_AMOUNT,
                                   SCD_EXPIRED_AMOUNT,
                                   SCD_TOTAL_LOST
                               )
                               VALUES (
                                   6,
                                   6,
                                   2,
                                   7,
                                   NULL,
                                   NULL
                               );

INSERT INTO STOCK_CHECK_DETAIL (
                                   SCD_CODE,
                                   M_CODE,
                                   CHECK_CODE,
                                   SCD_REMAINING_AMOUNT,
                                   SCD_EXPIRED_AMOUNT,
                                   SCD_TOTAL_LOST
                               )
                               VALUES (
                                   7,
                                   7,
                                   3,
                                   3,
                                   NULL,
                                   NULL
                               );

INSERT INTO STOCK_CHECK_DETAIL (
                                   SCD_CODE,
                                   M_CODE,
                                   CHECK_CODE,
                                   SCD_REMAINING_AMOUNT,
                                   SCD_EXPIRED_AMOUNT,
                                   SCD_TOTAL_LOST
                               )
                               VALUES (
                                   8,
                                   8,
                                   3,
                                   9,
                                   NULL,
                                   NULL
                               );

INSERT INTO STOCK_CHECK_DETAIL (
                                   SCD_CODE,
                                   M_CODE,
                                   CHECK_CODE,
                                   SCD_REMAINING_AMOUNT,
                                   SCD_EXPIRED_AMOUNT,
                                   SCD_TOTAL_LOST
                               )
                               VALUES (
                                   9,
                                   9,
                                   3,
                                   6,
                                   NULL,
                                   NULL
                               );

INSERT INTO STOCK_CHECK_DETAIL (
                                   SCD_CODE,
                                   M_CODE,
                                   CHECK_CODE,
                                   SCD_REMAINING_AMOUNT,
                                   SCD_EXPIRED_AMOUNT,
                                   SCD_TOTAL_LOST
                               )
                               VALUES (
                                   10,
                                   10,
                                   1,
                                   20,
                                   NULL,
                                   NULL
                               );

INSERT INTO STOCK_CHECK_DETAIL (
                                   SCD_CODE,
                                   M_CODE,
                                   CHECK_CODE,
                                   SCD_REMAINING_AMOUNT,
                                   SCD_EXPIRED_AMOUNT,
                                   SCD_TOTAL_LOST
                               )
                               VALUES (
                                   11,
                                   11,
                                   1,
                                   5,
                                   NULL,
                                   NULL
                               );

INSERT INTO STOCK_CHECK_DETAIL (
                                   SCD_CODE,
                                   M_CODE,
                                   CHECK_CODE,
                                   SCD_REMAINING_AMOUNT,
                                   SCD_EXPIRED_AMOUNT,
                                   SCD_TOTAL_LOST
                               )
                               VALUES (
                                   12,
                                   12,
                                   1,
                                   18,
                                   NULL,
                                   NULL
                               );

INSERT INTO STOCK_CHECK_DETAIL (
                                   SCD_CODE,
                                   M_CODE,
                                   CHECK_CODE,
                                   SCD_REMAINING_AMOUNT,
                                   SCD_EXPIRED_AMOUNT,
                                   SCD_TOTAL_LOST
                               )
                               VALUES (
                                   13,
                                   13,
                                   2,
                                   10,
                                   NULL,
                                   NULL
                               );

INSERT INTO STOCK_CHECK_DETAIL (
                                   SCD_CODE,
                                   M_CODE,
                                   CHECK_CODE,
                                   SCD_REMAINING_AMOUNT,
                                   SCD_EXPIRED_AMOUNT,
                                   SCD_TOTAL_LOST
                               )
                               VALUES (
                                   14,
                                   14,
                                   2,
                                   8,
                                   NULL,
                                   NULL
                               );

INSERT INTO STOCK_CHECK_DETAIL (
                                   SCD_CODE,
                                   M_CODE,
                                   CHECK_CODE,
                                   SCD_REMAINING_AMOUNT,
                                   SCD_EXPIRED_AMOUNT,
                                   SCD_TOTAL_LOST
                               )
                               VALUES (
                                   15,
                                   15,
                                   2,
                                   4,
                                   NULL,
                                   NULL
                               );

INSERT INTO STOCK_CHECK_DETAIL (
                                   SCD_CODE,
                                   M_CODE,
                                   CHECK_CODE,
                                   SCD_REMAINING_AMOUNT,
                                   SCD_EXPIRED_AMOUNT,
                                   SCD_TOTAL_LOST
                               )
                               VALUES (
                                   16,
                                   16,
                                   3,
                                   15,
                                   NULL,
                                   NULL
                               );

INSERT INTO STOCK_CHECK_DETAIL (
                                   SCD_CODE,
                                   M_CODE,
                                   CHECK_CODE,
                                   SCD_REMAINING_AMOUNT,
                                   SCD_EXPIRED_AMOUNT,
                                   SCD_TOTAL_LOST
                               )
                               VALUES (
                                   17,
                                   17,
                                   3,
                                   12,
                                   NULL,
                                   NULL
                               );

INSERT INTO STOCK_CHECK_DETAIL (
                                   SCD_CODE,
                                   M_CODE,
                                   CHECK_CODE,
                                   SCD_REMAINING_AMOUNT,
                                   SCD_EXPIRED_AMOUNT,
                                   SCD_TOTAL_LOST
                               )
                               VALUES (
                                   18,
                                   18,
                                   3,
                                   6,
                                   NULL,
                                   NULL
                               );

INSERT INTO STOCK_CHECK_DETAIL (
                                   SCD_CODE,
                                   M_CODE,
                                   CHECK_CODE,
                                   SCD_REMAINING_AMOUNT,
                                   SCD_EXPIRED_AMOUNT,
                                   SCD_TOTAL_LOST
                               )
                               VALUES (
                                   19,
                                   19,
                                   1,
                                   25,
                                   NULL,
                                   NULL
                               );

INSERT INTO STOCK_CHECK_DETAIL (
                                   SCD_CODE,
                                   M_CODE,
                                   CHECK_CODE,
                                   SCD_REMAINING_AMOUNT,
                                   SCD_EXPIRED_AMOUNT,
                                   SCD_TOTAL_LOST
                               )
                               VALUES (
                                   20,
                                   20,
                                   1,
                                   9,
                                   NULL,
                                   NULL
                               );

INSERT INTO STOCK_CHECK_DETAIL (
                                   SCD_CODE,
                                   M_CODE,
                                   CHECK_CODE,
                                   SCD_REMAINING_AMOUNT,
                                   SCD_EXPIRED_AMOUNT,
                                   SCD_TOTAL_LOST
                               )
                               VALUES (
                                   21,
                                   1,
                                   2,
                                   14,
                                   NULL,
                                   NULL
                               );

INSERT INTO STOCK_CHECK_DETAIL (
                                   SCD_CODE,
                                   M_CODE,
                                   CHECK_CODE,
                                   SCD_REMAINING_AMOUNT,
                                   SCD_EXPIRED_AMOUNT,
                                   SCD_TOTAL_LOST
                               )
                               VALUES (
                                   22,
                                   2,
                                   2,
                                   11,
                                   NULL,
                                   NULL
                               );

INSERT INTO STOCK_CHECK_DETAIL (
                                   SCD_CODE,
                                   M_CODE,
                                   CHECK_CODE,
                                   SCD_REMAINING_AMOUNT,
                                   SCD_EXPIRED_AMOUNT,
                                   SCD_TOTAL_LOST
                               )
                               VALUES (
                                   23,
                                   3,
                                   2,
                                   7,
                                   NULL,
                                   NULL
                               );

INSERT INTO STOCK_CHECK_DETAIL (
                                   SCD_CODE,
                                   M_CODE,
                                   CHECK_CODE,
                                   SCD_REMAINING_AMOUNT,
                                   SCD_EXPIRED_AMOUNT,
                                   SCD_TOTAL_LOST
                               )
                               VALUES (
                                   24,
                                   4,
                                   3,
                                   8,
                                   NULL,
                                   NULL
                               );

INSERT INTO STOCK_CHECK_DETAIL (
                                   SCD_CODE,
                                   M_CODE,
                                   CHECK_CODE,
                                   SCD_REMAINING_AMOUNT,
                                   SCD_EXPIRED_AMOUNT,
                                   SCD_TOTAL_LOST
                               )
                               VALUES (
                                   25,
                                   5,
                                   3,
                                   10,
                                   NULL,
                                   NULL
                               );

INSERT INTO STOCK_CHECK_DETAIL (
                                   SCD_CODE,
                                   M_CODE,
                                   CHECK_CODE,
                                   SCD_REMAINING_AMOUNT,
                                   SCD_EXPIRED_AMOUNT,
                                   SCD_TOTAL_LOST
                               )
                               VALUES (
                                   26,
                                   6,
                                   3,
                                   5,
                                   NULL,
                                   NULL
                               );

INSERT INTO STOCK_CHECK_DETAIL (
                                   SCD_CODE,
                                   M_CODE,
                                   CHECK_CODE,
                                   SCD_REMAINING_AMOUNT,
                                   SCD_EXPIRED_AMOUNT,
                                   SCD_TOTAL_LOST
                               )
                               VALUES (
                                   27,
                                   7,
                                   1,
                                   10,
                                   NULL,
                                   NULL
                               );

INSERT INTO STOCK_CHECK_DETAIL (
                                   SCD_CODE,
                                   M_CODE,
                                   CHECK_CODE,
                                   SCD_REMAINING_AMOUNT,
                                   SCD_EXPIRED_AMOUNT,
                                   SCD_TOTAL_LOST
                               )
                               VALUES (
                                   28,
                                   8,
                                   1,
                                   8,
                                   NULL,
                                   NULL
                               );

INSERT INTO STOCK_CHECK_DETAIL (
                                   SCD_CODE,
                                   M_CODE,
                                   CHECK_CODE,
                                   SCD_REMAINING_AMOUNT,
                                   SCD_EXPIRED_AMOUNT,
                                   SCD_TOTAL_LOST
                               )
                               VALUES (
                                   29,
                                   9,
                                   1,
                                   4,
                                   NULL,
                                   NULL
                               );

INSERT INTO STOCK_CHECK_DETAIL (
                                   SCD_CODE,
                                   M_CODE,
                                   CHECK_CODE,
                                   SCD_REMAINING_AMOUNT,
                                   SCD_EXPIRED_AMOUNT,
                                   SCD_TOTAL_LOST
                               )
                               VALUES (
                                   30,
                                   10,
                                   2,
                                   15,
                                   NULL,
                                   NULL
                               );

INSERT INTO STOCK_CHECK_DETAIL (
                                   SCD_CODE,
                                   M_CODE,
                                   CHECK_CODE,
                                   SCD_REMAINING_AMOUNT,
                                   SCD_EXPIRED_AMOUNT,
                                   SCD_TOTAL_LOST
                               )
                               VALUES (
                                   31,
                                   11,
                                   2,
                                   9,
                                   NULL,
                                   NULL
                               );

INSERT INTO STOCK_CHECK_DETAIL (
                                   SCD_CODE,
                                   M_CODE,
                                   CHECK_CODE,
                                   SCD_REMAINING_AMOUNT,
                                   SCD_EXPIRED_AMOUNT,
                                   SCD_TOTAL_LOST
                               )
                               VALUES (
                                   32,
                                   12,
                                   2,
                                   7,
                                   NULL,
                                   NULL
                               );

INSERT INTO STOCK_CHECK_DETAIL (
                                   SCD_CODE,
                                   M_CODE,
                                   CHECK_CODE,
                                   SCD_REMAINING_AMOUNT,
                                   SCD_EXPIRED_AMOUNT,
                                   SCD_TOTAL_LOST
                               )
                               VALUES (
                                   33,
                                   13,
                                   3,
                                   12,
                                   NULL,
                                   NULL
                               );

INSERT INTO STOCK_CHECK_DETAIL (
                                   SCD_CODE,
                                   M_CODE,
                                   CHECK_CODE,
                                   SCD_REMAINING_AMOUNT,
                                   SCD_EXPIRED_AMOUNT,
                                   SCD_TOTAL_LOST
                               )
                               VALUES (
                                   34,
                                   14,
                                   3,
                                   14,
                                   NULL,
                                   NULL
                               );

INSERT INTO STOCK_CHECK_DETAIL (
                                   SCD_CODE,
                                   M_CODE,
                                   CHECK_CODE,
                                   SCD_REMAINING_AMOUNT,
                                   SCD_EXPIRED_AMOUNT,
                                   SCD_TOTAL_LOST
                               )
                               VALUES (
                                   35,
                                   15,
                                   3,
                                   6,
                                   NULL,
                                   NULL
                               );

INSERT INTO STOCK_CHECK_DETAIL (
                                   SCD_CODE,
                                   M_CODE,
                                   CHECK_CODE,
                                   SCD_REMAINING_AMOUNT,
                                   SCD_EXPIRED_AMOUNT,
                                   SCD_TOTAL_LOST
                               )
                               VALUES (
                                   36,
                                   16,
                                   1,
                                   18,
                                   NULL,
                                   NULL
                               );

INSERT INTO STOCK_CHECK_DETAIL (
                                   SCD_CODE,
                                   M_CODE,
                                   CHECK_CODE,
                                   SCD_REMAINING_AMOUNT,
                                   SCD_EXPIRED_AMOUNT,
                                   SCD_TOTAL_LOST
                               )
                               VALUES (
                                   37,
                                   17,
                                   1,
                                   12,
                                   NULL,
                                   NULL
                               );

INSERT INTO STOCK_CHECK_DETAIL (
                                   SCD_CODE,
                                   M_CODE,
                                   CHECK_CODE,
                                   SCD_REMAINING_AMOUNT,
                                   SCD_EXPIRED_AMOUNT,
                                   SCD_TOTAL_LOST
                               )
                               VALUES (
                                   38,
                                   18,
                                   1,
                                   9,
                                   NULL,
                                   NULL
                               );

INSERT INTO STOCK_CHECK_DETAIL (
                                   SCD_CODE,
                                   M_CODE,
                                   CHECK_CODE,
                                   SCD_REMAINING_AMOUNT,
                                   SCD_EXPIRED_AMOUNT,
                                   SCD_TOTAL_LOST
                               )
                               VALUES (
                                   39,
                                   19,
                                   2,
                                   20,
                                   NULL,
                                   NULL
                               );

INSERT INTO STOCK_CHECK_DETAIL (
                                   SCD_CODE,
                                   M_CODE,
                                   CHECK_CODE,
                                   SCD_REMAINING_AMOUNT,
                                   SCD_EXPIRED_AMOUNT,
                                   SCD_TOTAL_LOST
                               )
                               VALUES (
                                   40,
                                   20,
                                   2,
                                   15,
                                   NULL,
                                   NULL
                               );


-- Table: UTILITY_COST
DROP TABLE IF EXISTS UTILITY_COST;

CREATE TABLE IF NOT EXISTS UTILITY_COST (
    UC_CODE     INTEGER PRIMARY KEY ASC AUTOINCREMENT
                        NOT NULL
                        UNIQUE,
    UC_ELECTRIC NUMERIC NOT NULL,
    UC_WATER    NUMERIC NOT NULL,
    UC_RENTAL   NUMERIC NOT NULL,
    UC_TOTAL    NUMERIC NOT NULL,
    UC_DATE     DATE    DEFAULT ( (datetime('now', 'localtime') ) ) 
                        NOT NULL
);

INSERT INTO UTILITY_COST (
                             UC_CODE,
                             UC_ELECTRIC,
                             UC_WATER,
                             UC_RENTAL,
                             UC_TOTAL,
                             UC_DATE
                         )
                         VALUES (
                             1,
                             70,
                             80,
                             90,
                             90,
                             '2023-10-23 16:58:15'
                         );

INSERT INTO UTILITY_COST (
                             UC_CODE,
                             UC_ELECTRIC,
                             UC_WATER,
                             UC_RENTAL,
                             UC_TOTAL,
                             UC_DATE
                         )
                         VALUES (
                             2,
                             70,
                             80,
                             90,
                             90,
                             '2023-10-23 14:01:40'
                         );


-- Table: VENDOR
DROP TABLE IF EXISTS VENDOR;

CREATE TABLE IF NOT EXISTS VENDOR (
    V_CODE     INTEGER       PRIMARY KEY ASC AUTOINCREMENT
                             UNIQUE,
    V_COUNTRY  VARCHAR (35)  NOT NULL,
    V_ADDRESS  VARCHAR (100) NOT NULL,
    V_PHONE    CHAR (12)     CHECK (V_PHONE LIKE '___-___-____') 
                             NOT NULL,
    V_NAME     VARCHAR (35)  NOT NULL,
    V_CONTACT  VARCHAR (30)  NOT NULL,
    V_CATEGORY VARCHAR (25)  NOT NULL
);

INSERT INTO VENDOR (
                       V_CODE,
                       V_COUNTRY,
                       V_ADDRESS,
                       V_PHONE,
                       V_NAME,
                       V_CONTACT,
                       V_CATEGORY
                   )
                   VALUES (
                       1,
                       'TH',
                       'kiri road',
                       '088-888-8888',
                       'Makro',
                       'Makro@gmail.com',
                       'cash and carry store'
                   );

INSERT INTO VENDOR (
                       V_CODE,
                       V_COUNTRY,
                       V_ADDRESS,
                       V_PHONE,
                       V_NAME,
                       V_CONTACT,
                       V_CATEGORY
                   )
                   VALUES (
                       2,
                       'TH',
                       'chonburi town',
                       '089-999-9999',
                       'Central',
                       'Central@gmail.com',
                       'Mall'
                   );

INSERT INTO VENDOR (
                       V_CODE,
                       V_COUNTRY,
                       V_ADDRESS,
                       V_PHONE,
                       V_NAME,
                       V_CONTACT,
                       V_CATEGORY
                   )
                   VALUES (
                       3,
                       'TH',
                       'kiri road',
                       '087-777-7777',
                       'BigC',
                       'BigC@gmail.com',
                       'department store'
                   );

INSERT INTO VENDOR (
                       V_CODE,
                       V_COUNTRY,
                       V_ADDRESS,
                       V_PHONE,
                       V_NAME,
                       V_CONTACT,
                       V_CATEGORY
                   )
                   VALUES (
                       4,
                       'TH',
                       'chonburi town',
                       '081-111-1111',
                       'Lotus',
                       'Lotus@gmail.com',
                       'department store'
                   );


COMMIT TRANSACTION;
PRAGMA foreign_keys = on;
